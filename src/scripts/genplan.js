var loadImage = function(src, callback) {
    var img = new Image();
    img.onload = img.onerror = function(evt) {
        callback(evt.type == "error", img);
        img.onload = img.onerror = img = null;
    };
    img.src = src;
};

let $numbers = $(`.genplan__number`);

$(".genplan__object").hover(
    function(e) {
        var pos = $(".genplan__content").offset();
        var elem_left = pos.left;
        var elem_top = pos.top;
        var Xinner = e.pageX - elem_left;
        var Yinner = e.pageY - elem_top;

        $(`.genplan__render[data-id=${this.dataset.id}]`).addClass("genplan__render--active");
        $numbers.not(`[data-id=${this.dataset.id}]`).addClass("genplan__number--pale");
        // $(`.genplan__number[data-id=${this.dataset.id}]`).addClass("genplan__number--active");
        $(`.genplan__label[data-id=${this.dataset.id}]`)
            .css({ top: Yinner + 10, left: Xinner })
            .addClass("genplan__label--active");
    },
    function() {
        $(`.genplan__render[data-id=${this.dataset.id}]`).removeClass("genplan__render--active");
        $numbers.removeClass("genplan__number--pale");
        $(`.genplan__label[data-id=${this.dataset.id}]`).removeClass("genplan__label--active");
    }
);

$(".genplan__object").click((event) => {
    window.location = $(event.currentTarget).data("bld-url");
});

$numbers
    .hover(
        function(event) {
            event.preventDefault();
            $(`.genplan__object[data-id='${$(this).data("id")}']`).trigger("mouseenter");
            return false;
        },
        function(event) {
            event.preventDefault();
            $(`.genplan__object[data-id='${$(this).data("id")}']`).trigger("mouseleave");
            return false;
        }
    )
    .on("click", function(event) {
        event.preventDefault();
        $(`.genplan__object[data-id='${$(this).data("id")}']`).trigger("click");
    });

loadImage($(".genplan__img-common").attr("src"), function(err, img) {
    if (err) {
        alert("image not load");
    } else {
        var ratio = img.width / img.height;
        var windowWidth = $(window).width();

        updateGenplan(ratio, windowWidth);

        $(window).resize(() => {
            windowWidth = $(window).width();
            updateGenplan(ratio, windowWidth);
        });
    }
});

function updateGenplan(ratio, windowWidth) {
    resizeImg(ratio);
    let $genplan = $(".genplan");
    let $genplanContent = $genplan.find(".genplan__content");
    if (windowWidth >= 1280) {
        genplanPosition();
        $genplan.css({ "overflow-x": "" });
    } else {
        $genplanContent.css({ top: "", left: "" });
        $genplan.css({ "overflow-x": "auto" });

        $genplan.scrollLeft(($genplanContent.width() - $(window).width()) / 2);
    }
    $genplan.css("visibility", "visible");
}

function resizeImg(ratio) {
    var genWidth = $(".genplan").outerWidth();
    var genHeight = $(".genplan").outerHeight();
    var getRatio = genWidth / genHeight;

    if (getRatio < ratio) {
        $(".genplan__content").css({ width: `${genHeight * ratio}px`, height: "100%" });
        $(".genplan__img-common").css({ width: `${genHeight * ratio}px`, height: "100%" });
    } else {
        $(".genplan__content").css({ width: "", height: "" });
        $(".genplan__img-common").css({ width: "", height: "" });
    }
}

function genplanPosition() {
    let $genplan = $(".genplan");
    let $genplanContent = $genplan.find(".genplan__content");

    var genWidth = $genplan.outerWidth();
    var genHeight = $genplan.outerHeight();

    var genContentWidth = $genplanContent.outerWidth();
    var genContentHeight = $genplanContent.outerHeight();

    $genplanContent.css({
        top: `${-(genContentHeight - genHeight) / 2}px`,
        left: `${-(genContentWidth - genWidth) / 2}px`,
    });
}
