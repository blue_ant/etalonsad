const FLATS_PER_PAGE = 24;

let cardTplStr = `
/*=require ./includes/chunks/FlatType.tpl */
`;

let flatsListBuilder = new FlatsList({
    tpl: cardTplStr,
    $mountEl: $("#FlatTypeResult"),
});


let $progress = $(".PickupFlatLayout_progress");
let $resultsCont = $(".PickupFlatLayout_resultsCont");

let filter = new FilterForm("#filterForm", {
    submitHandler: ($filterForm) => {
        // console.log(" --- submitHandler fired---", "filter.offset: ", filter.offset);

        $resultsCont.addClass("PickupFlatLayout_resultsCont-loading");
        $progress.show().animate({ width: "33%", opacity: 1 }, 600);

        $.ajax({
            url: $filterForm.attr("target"),
            dataType: "json",
            method: $filterForm.attr("method"),
            async: true,
            data: $.extend(true, $filterForm.serializeObject(), {
                action: "get_flats",
                limit: FLATS_PER_PAGE,
                offset: filter.offset,
            }),
            xhr: () => {
                let xhr = new window.XMLHttpRequest();
                xhr.addEventListener("progress", (event) => {
                    if (event.lengthComputable) {
                        let percent = Math.ceil((100 * event.loaded) / event.total);
                        $progress.stop(true, false).animate({ width: percent + "%" }, 400);
                        if (percent === 100) {
                            $progress.animate({ opacity: 0 }, 350, () => {
                                $progress.removeAttr("style");
                            });
                        }
                    }

                    else {
                        $progress.stop(true, false).animate({ width: '100' + "%" }, 400);
                        $progress.animate({ opacity: 0 }, 350, () => {
                            $progress.removeAttr("style");
                        });
                    }
                });

                return xhr;
            },
        })
            .done((jsonResponse) => {
                // fix pics urls in development enviroment
                if (window.location.href.indexOf("/filters.html") != -1) {
                    console.warn("rewrite json response for dev enviroment usage...");
                    jsonResponse.flats.forEach((flt) => {
                        flt.pic = "https://etalonsad.ru/" + flt.pic;
                        flt.href = "/flat.html";
                    });
                    console.log('jsonResponse', jsonResponse)
                }

                {
                    let promos = jsonResponse.promos;
                    if (promos && promos.length) {
                        promos.forEach((promo)=>{
                            jsonResponse.flats.splice(promo.order-1, 0, promo);
                        });
                    }
                }
                // set favorites
                jsonResponse.flatsType.forEach((flt) => {
                    flt.favorite = _.includes(favListFlats.favorites, flt.id) ? 1 : 0;

                });

                // set comparison
                jsonResponse.flatsType.forEach((pln) => {
                    pln.compare = _.includes(flatsToCompare.favorites, pln.id) ? 1 : 0;
                });

                // render plans
                $("#filteredFlatsCounter").html(jsonResponse.total);

                flatsListBuilder.render({
                    data: jsonResponse,
                });



                $resultsCont.removeClass("PickupFlatLayout_resultsCont-loading");
            })
            .fail(() => {
                alert("Не удалось получить данные с сервера!\nПопробуйте позже.");
            });
    },
});

let sorter = new Sorter(".Sorter");

$("#FlatTypeResult").tooltip({
    items: ".actionIcon",
    classes: {
        "ui-tooltip": "actionIconTooltip",
    },
});


toolTip();
// START: favorites toggling behaviour

$("#FlatTypeResult").on('click', ".FavoriteIcon", (event)=> {
    event.preventDefault();
    let $favLink = $(event.currentTarget);

    if ($favLink.hasClass('FavoriteIcon-active')) {
        favListFlats.remove($favLink.data('flat-type-id'));
    }else{
        favListFlats.add($favLink.data('flat-type-id'));
    }

    $favLink.toggleClass('FavoriteIcon-active');
    displayHeaderFavIcon();
    toolTip();
});
// END: compare toggling behaviour

$("#FlatTypeResult").on('click', ".ComparisonIcon", (event)=> {
    event.preventDefault();
    let $compareLink = $(event.currentTarget);

    if ($compareLink.hasClass('ComparisonIcon-active')) {
        flatsToCompare.remove($compareLink.data('flat-type-id'));
    }else{
        flatsToCompare.add($compareLink.data('flat-type-id'));
    }

    $compareLink.toggleClass('ComparisonIcon-active');
    displayHeaderCompareIcon();
    toolTip();
    console.log('get', flatsToCompare.getAll())
});
// END: favorites toggling behaviour



