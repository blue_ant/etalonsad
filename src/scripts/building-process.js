var winWidth = $(window).width();
var burger = $('.header__burger');
let swipers = [];

$('.slider-months').each(function(index, el) {
    el.classList.add("s" + index);

    $(this).find('.slider-months-nav__next').addClass('slider-months-nav__next' + index);
    $(this).find('.slider-months-nav__prev').addClass('slider-months-nav__prev' + index);
    $(this).find('.slider-months-fraction').addClass('slider-months-fraction' + index);


    let next = $('.slider-months-nav__next' + index);
    let prev = $('.slider-months-nav__prev' + index);


    swipers.push(new Swiper(el, {
        hashNavigation: true,
        slidesPerView: 2,
        loop: true,
        centeredSlides: true,
        loopAdditionalSlides: 4,
        loopedSlides: 4,
        lazy: {
            loadPrevNext: true,
            loadPrevNextAmount: 2
        },
        preloadImages: false,
        // preloaderClass: 'cssload-loading',
        navigation: {
            nextEl: next,
            prevEl: prev,
        },
        breakpoints: {
            1023: {
                slidesPerView: 1
            }
        },

        speed: 600

    }));
});


let activeSlideThumb = $('.slider-thumbs-months__slide.is-selected').index();

var sliderThumbsMonth = new Swiper('.slider-thumbs-months', {
    initialSlide: activeSlideThumb,
    slidesPerView: 'auto',
    slideToClickedSlide: true,
    observer: true,
    observeParents: true,
});


sliderThumbsMonth.slideTo(activeSlideThumb);

$(".building-process__panorama").fancybox({
    modal: true,
    beforeLoad: function( /*instance, slide*/ ) {
        var winWidth = $(window).width();
        var winHeight = $(window).height();
        var checkLandscapeOrienation = winWidth > winHeight;

        if (checkLandscapeOrienation && winWidth < 767) {
            $('.header').removeClass('header_topLayer');
        } else {
            $('.header').addClass('header_topLayer');
        }

    },
    beforeClose: function( /*instance, slide*/ ) {
        burger.show('fast');
    }
});

$("#tabs").tabs({
    classes: {
        "ui-tabs": "webcam-modal-tabs",
        "ui-tabs-nav": "webcam-modal-tabs__nav",
        "ui-tabs-panel": "webcam-modal-tabs__iframe"
    }
});

function positionTitleMonthSlider() {
    let widthActiveSlideMonth = $('.slider-months-slide.swiper-slide-active').width();
    $('.slider-months__title').css({
        'width': widthActiveSlideMonth,
        'margin-left': -widthActiveSlideMonth / 2,
    });
}

function positionTitleMonthSliderReset() {
    $('.slider-months__title').css({
        'width': 'auto',
        'margin-left': 'auto'
    });
}

if (winWidth < 1024) {
    positionTitleMonthSliderReset();
} else {
    positionTitleMonthSlider();
}


$(window).resize(function() {
    var winWidthResize = $(window).width();
    var winHeightResize = $(window).height();
    var checkLandscapeOrienationResize = winWidthResize > winHeightResize;

    swipers.forEach(function(item, i, swipers) {
        swipers[i].destroy();
    });

    sliderThumbsMonth.destroy();

    swipers = [];
    $('.slider-months').each(function(index, el) {
        el.classList.add("s" + index);

        $(this).find('.slider-months-nav__next').addClass('slider-months-nav__next' + index);
        $(this).find('.slider-months-nav__prev').addClass('slider-months-nav__prev' + index);
        $(this).find('.slider-months-fraction').addClass('slider-months-fraction' + index);

        let next = $('.slider-months-nav__next' + index);
        let prev = $('.slider-months-nav__prev' + index);

        swipers.push(new Swiper(el, {
            hashNavigation: true,
            slidesPerView: 2,
            loop: true,
            centeredSlides: true,
            loopAdditionalSlides: 4,
            loopedSlides: 4,
            lazy: {
                loadPrevNext: true,
                loadPrevNextAmount: 2
            },
            preloadImages: false,
            // preloaderClass: 'cssload-loading',
            navigation: {
                nextEl: next,
                prevEl: prev,
            },
            breakpoints: {
                1023: {
                    slidesPerView: 1
                }
            },
            speed: 600

        }));
    });

    sliderThumbsMonth = new Swiper('.slider-thumbs-months', {
        slidesPerView: 'auto',
        slideToClickedSlide: true,
        observer: true,
        observeParents: true,
        on: {
            click: function( /*swiper, event*/ ) {
                var clicked = this.clickedIndex;
                $(this.slides).removeClass('is-selected');
                $(this.clickedSlide).addClass('is-selected');
                sliderMonthsList.slideTo(clicked, 800, false);
            }
        },
    });

    if (checkLandscapeOrienationResize && winWidthResize < 767) {
        $('.header').removeClass('header_topLayer');
        let checkOpenBuildingModal = !!document.getElementsByClassName("fancybox-is-open").length;
        if (checkOpenBuildingModal) {
            burger.hide('fast');
        }
    } else {
        let checkOpenBuildingModal = !!document.getElementsByClassName("fancybox-is-open").length;
        if (checkOpenBuildingModal) {
            $('.header').addClass('header_topLayer');
            burger.show('fast');
        }
    }

    if (winWidthResize < 1024) {
        positionTitleMonthSliderReset();
    } else {
        positionTitleMonthSlider();
    }

});

let $broadCastFrames = $('.broadcast-iframe');

let changeSizeBroadCastFrame = () => {
    $broadCastFrames.each((index, el) => {
        let $brFrame = $(el);
        let ww = $brFrame.width();
        let hw = Math.round(ww / 1.777777777777778);
        let newFrameSrc = $brFrame.attr('src').split("?")[0] + '?' + ww + 'x' + hw;
        console.log(newFrameSrc)
        $brFrame.attr('src', newFrameSrc);
    });

};

// $(".building-process__webcam").fancybox({
//     afterShow: function() {
//         changeSizeBroadCastFrame();
//     }
// });

$( "#tabs" ).tabs({
    activate: function() {
        changeSizeBroadCastFrame();
    }
});

$(window).on("resize", _.debounce(() => {
    if (document.querySelector(".fancybox-is-open")) {
        changeSizeBroadCastFrame();
    }

}, 100));
