let compareDesktopSliders = [];
let compareMobileSliders = [];

let toggleFavComparePage = () => {
    let comparePage = $('[data-compare-page]');
    let favoritesPage = $('[data-favorites-page]');
    console.log('comparePage', comparePage);
    console.log('favoritesPage', favoritesPage);

    let favoritesTab = $('[data-favorites-tab]');
    let compareTab = $('[data-compare-tab]');

    favoritesTab.on('click', function () {
        console.log('clicl beath')
        comparePage.hide();
        favoritesPage.show();
    });

    compareTab.on('click', function () {
        favoritesPage.hide();
        comparePage.show();
    });
};
toggleFavComparePage();

let initSlider = () => {
    $('.Compare-slider').each(function (index, el) {
        el.classList.add("s" + index);

        let compareId = $(this).parent().data('id-compare');
        let navsTpl = `<div class="Compare-list__sliderNav navs`+ index +`" data-id-compare="`+ compareId +`">
            <a href="" class="Compare-list__sliderNav-prev"></a>
            <a href="" class="Compare-list__sliderNav-next"></a>
        </div>`;
        $('.Compare-list__header_desktop').append(navsTpl);

        compareDesktopSliders.push(new Swiper(el, {
            slidesPerView: 'auto',
            spaceBetween: 0,
            // freeMode: true,
            observer: true,
            observeParents: true,
            resistanceRatio: 0,
            navigation: {
                nextEl: '.navs'+index + ' .Compare-list__sliderNav-next',
                prevEl: '.navs'+index + ' .Compare-list__sliderNav-prev'
            },
            breakpoints: {
                1023: {
                    slidesPerView: 2
                }

            }
        }));
    });

    $('.Compare-mobile-slider').each(function (index, el) {
        el.classList.add("ms" + index);

        $(this).find('.Compare-mobile-slider__pag').addClass('Compare-mobile-slider__pag' + index);
        let pagination = $('.Compare-mobile-slider__pag' + index);

        compareMobileSliders.push(new Swiper(el, {
            slidesPerView: 1,
            spaceBetween: 0,
            observer: true,
            observeParents: true,
            // freeMode: true,
            resistanceRatio: 0,
            // loop:true,
            // loopAdditionalSlides: 2,
            pagination: {
                el: pagination,
                type: 'fraction',
            }
        }))
    })
}

let hoverRow = ()=> {
    let $windowW= $(window).width();
    console.log('$windowW', $windowW)
    if ($windowW > 1023) {
        $('.Compare-slider-slide__row, .Compare-param-list__item, .Compare-param-list__subitem, .Compare-param-list__subitem-2').hover(
            function () {
                let windownWidth = $(window).width();
                let isMobile = windownWidth < 768;
                let activeClassName= $(this).attr('class');
                let classes = activeClassName.split(" ");
                let hoverClassName = _.drop(classes, classes.length - 1);
                let styles = "." + hoverClassName + " {background: #f6f6f6}";

                if (!isMobile) {
                    $(styleRowCont).html(styles);
                }

            },
            function () {
                $(styleRowCont).html('');
            }
        )
    }
}
let styleRowCont = $('#styleRow');

let compareTplStr = `
/*=require ./includes/chunks/Compare.tpl*/
`;

let flatsListBuilder = new FlatsList({
    tpl: compareTplStr,
    $mountEl: $(".Compare__result"),
});

console.log('flatsListBuilder', flatsListBuilder)

let isCheckedDiff = $('#CompareCheckboxDiff').is(':checked');

let getCompare = () => {

    $.ajax({
        url: $('.Compare').data('compare-source'),
        dataType: "json",
        method: 'GET',
        async: true,
        data: {
            onlydiff: isCheckedDiff ? 1 : 0,
            plans: favListPlans.favorites,
            flats: favListFlats.favorites,
        }
    })
        .done((jsonResponse) => {
            let comparePlansFlats = jsonResponse;
            let flatsHave = typeof comparePlansFlats.flats !== "undefined";
            let plansHave = typeof comparePlansFlats.plans !== "undefined";

            for (let i = 0; i < compareDesktopSliders.length; i++) {
                compareDesktopSliders[i].destroy(true, true);
            }

            for (let p = 0; p < compareMobileSliders.length; p++) {
                compareMobileSliders[p].destroy(true, true);
            }


            flatsListBuilder.render({
                data: comparePlansFlats,
            });
            initSlider();
            hoverRow();
            toggleFavComparePage();

            $('[data-id="CompareFlats"] span').html(favListFlats.favorites.length);
            $('[data-id="ComparePlans"] span').html(favListPlans.favorites.length);

            // выключаем пока нет планировок
            // {
            //     let activeTab = $('.FavoriteTabs__link-active').data('id');
            //     $('.Compare-tabs-content').hide();
            //     $('.Compare-list__sliderNav').hide();
            //     $('[data-id-compare="'+ activeTab +'"]').show();
            // }
            //

            let checkboxDiff = $('.Compare__header');
            // if ( !flatsHave && !plansHave ) {
            //     checkboxDiff.addClass('Compare__header_disable');
            // }
            if ( !flatsHave ) {
                checkboxDiff.addClass('Compare__header_disable');
            }

            $('[data-compare-page]').on('click', '.FavoriteTabs__link', function (e) {
                e.preventDefault();
                let id = $(this).data("id");
                let amountComparePlans = favListPlans.favorites.length;
                let amountCompareFlats = favListFlats.favorites.length;

                $(this).parent().find('.FavoriteTabs__link').removeClass('FavoriteTabs__link-active');
                $(this).addClass('FavoriteTabs__link-active');
                $('.Compare-tabs-content').hide();
                $('.Compare-list__sliderNav').hide();
                $('[data-id-compare="'+id+'"]').show();

                if ( id === 'CompareFlats' && !flatsHave && !amountCompareFlats ) {
                    checkboxDiff.addClass('Compare__header_disable');
                } else if (id === 'ComparePlans' && !plansHave && !amountComparePlans ) {
                    checkboxDiff.addClass('Compare__header_disable');
                } else {
                    checkboxDiff.removeClass('Compare__header_disable');
                }

                if (id === 'CompareFlats' && !amountCompareFlats ) {
                    checkboxDiff.addClass('Compare__header_disable');
                } else if ( id === 'ComparePlans' && !amountComparePlans ) {
                    checkboxDiff.addClass('Compare__header_disable');
                } else {
                    checkboxDiff.removeClass('Compare__header_disable');
                }
            });

            $('.Compare-list__sliderNav a').on('click', (event)=> {
                event.preventDefault();
            });

            $('.Compare__header').addClass('Compare__header_visible');

            // remove plans
            $('[data-id-compare="ComparePlans"]').on('click', '.Compare-slider-slide__remove, .FavoriteIcon', function () {
                console.log('plan delete')
                let itemId = $(this).data('remove-id');
                $('[data-remove-id="'+ itemId +'"]').parent().parent().each(function (index, el) {
                    $(this).fadeOut('100', () => {
                        $(this).remove();
                    });
                });
                favListPlans.remove(itemId);
                let amountComparePlans = favListPlans.favorites.length;
                $('[data-id="ComparePlans"] span').html(amountComparePlans);

                if ( !amountComparePlans ) {
                    checkboxDiff.addClass('Compare__header_disable');
                    $('.Compare-tabs-content[data-id-compare="ComparePlans"]').addClass('Compare-tabs-content_disable');
                }
            });
            // remove plans

            // remove flats
            $('[data-id-compare="CompareFlats"]').on('click', '.Compare-slider-slide__remove, .FavoriteIcon', function () {
                console.log('flat delete')
                let itemId = $(this).data('remove-id');
                $('[data-remove-id="'+ itemId +'"]').parent().parent().each(function (index, el) {
                    $(this).fadeOut('100', () => {
                        $(this).remove();
                    });
                });
                $('[data-flat-remove="'+ itemId +'"]').each(function (index, el) {
                    $(this).fadeOut('100', () => {
                        $(this).remove();
                    });
                });
                favListFlats.remove(itemId);
                let amountCompareFlats = favListFlats.favorites.length;
                $('[data-id="CompareFlats"] span').html(amountCompareFlats);

                if ( !amountCompareFlats ) {
                    checkboxDiff.addClass('Compare__header_disable');
                    $('.Compare-tabs-content[data-id-compare="CompareFlats"]').addClass('Compare-tabs-content_disable');
                }
            });
            // remove flats


            // add to favorites
            $('[data-id-compare="CompareFlats"]').on('click', '.FavoriteIcon', (event) => {
                event.preventDefault();
                let $favLink = $(event.currentTarget);

                if ($favLink.hasClass('FavoriteIcon-active')) {
                    favListFlats.remove($favLink.data('flat-type-id'));
                }else{
                    favListFlats.add($favLink.data('flat-type-id'));
                }

                $favLink.toggleClass('FavoriteIcon-active');
                displayHeaderFavIcon();
            });

            $('[data-id-compare="ComparePlans"]').on('click', ".FavoriteIcon", (event)=> {
                event.preventDefault();
                let $favLink = $(event.currentTarget);

                if ($favLink.hasClass('FavoriteIcon-active')) {
                    favListPlans.remove($favLink.data('plan-id'));
                }else{
                    favListPlans.add($favLink.data('plan-id'));
                }

                let planId = $favLink.data('plan-id');
                let activeIcons = $('[data-plan-id="'+ planId +'"]').filter('.FavoriteIcon');
                activeIcons.each(function (index, el) {
                    $(this).toggleClass('FavoriteIcon-active');
                })


                // $favLink.toggleClass('FavoriteIcon-active');
                displayHeaderFavIcon();
                toolTip();

            });

            // add to favorite

            toolTip();
            setActiveFavorite();

        })
        .fail(() => {
            alert("Не удалось получить данные с сервера!\nПопробуйте позже.");
        });

}


$(window).on('resize', ()=> {
    console.log('resize')
})


getCompare();
toggleFavComparePage();

$('#CompareCheckboxDiff').on('change', () => {
    isCheckedDiff = $('#CompareCheckboxDiff').is(':checked');
    getCompare();
});


