$(".Flat_linkBack").on("click", function(event) {
    let isPrevPageOnTheSameSite = _.inRange(document.referrer.indexOf(window.location.host), 5, 9);
    let wasPrevPageFilter = document.referrer.indexOf("bedrooms") !== 1;
    if (isPrevPageOnTheSameSite && wasPrevPageFilter) {
        event.preventDefault();
        window.location.href = document.referrer;
    } else {
        return true;
    }
});

let sliders = [];
$(".FlatsTips_sliderContainer").each(function(index, el) {
    let arrows = el.parentNode.querySelectorAll(".FlatsTips_slideBtn");

    let slider = new Swiper(el, {
        slidesPerView: 4,
        navigation: {
            prevEl: arrows[0],
            nextEl: arrows[1],
        },
        resistanceRatio: 0,
        breakpoints: {
            1279: {
                slidesPerView: 2,
            },
            575: {
                slidesPerView: 1,
            },
        },
    });

    sliders.push(slider);
});

let $tabs = $(".FlatsTips");

let tabsSwiperInst = new Swiper(".FlatsTips_tabsWrapperMobile", {
    slidesPerView: 1,
    navigation: {
        nextEl: ".FlatsTips_slideBtnMobile-next",
        prevEl: ".FlatsTips_slideBtnMobile-prev",
    },

    on: {
        slideChange: () => {
            $tabs.tabs("option", "active", tabsSwiperInst.activeIndex);
        },
    },
});

$tabs.tabs({
    classes: {
        "ui-tabs": "",
        "ui-tabs-nav": "",
        "ui-tabs-tab": "",
        "ui-tabs-panel": "",
        "ui-tabs-active": "FlatsTips_tabsItem-active",
    },
    activate: (event, ui) => {
        let tabIndex = ui.newTab.index();

        sliders[tabIndex].update();
        tabsSwiperInst.slideTo(tabIndex, 0, false);
    },
});


$( ".Flat-type-plans" ).tabs({
    classes: {
        "ui-tabs-nav": "Flat-type-plans__nav",
    }
});

// set favorite
{
    let favIconTitle = $(".Flat-type_info .FavoriteIcon, .FlatsTips_icons .FavoriteIcon")
    let planId = favIconTitle.data('plan-id');
    favListPlans.isFavorite(planId);
    if ( favListPlans.isFavorite(planId) ) {
        favIconTitle.addClass('FavoriteIcon-active');
    }
}
// set comparison
{
    let comparisonIconTitle = $(".Flat-type_info .ComparisonIcon, .FlatsTips_icons .ComparisonIcon")
    let planId = comparisonIconTitle.data('plan-id');
    plansToCompare.isFavorite(planId);
    if ( plansToCompare.isFavorite(planId) ) {
        comparisonIconTitle.addClass('ComparisonIcon-active');
    }
}


toolTip();
// START: favorites toggling behaviour

$(".Flat-type_info, .FlatsTips_icons").on('click', ".FavoriteIcon", (event)=> {
    event.preventDefault();
    let $favLink = $(event.currentTarget);
    console.log('$favLink', $favLink);

    if ($favLink.hasClass('FavoriteIcon-active')) {
        favListPlans.remove($favLink.data('plan-id'));
    }else{
        favListPlans.add($favLink.data('plan-id'));
    }

    $favLink.toggleClass('FavoriteIcon-active');
    displayHeaderFavIcon();
    toolTip();
});


// END: favorites toggling behaviour

// START: compare toggling behaviour

$(".Flat-type_info, .FlatsTips_icons").on('click', ".ComparisonIcon", (event)=> {
    event.preventDefault();
    let $compareLink = $(event.currentTarget);
    console.log('$compareLink', $compareLink);

    if ($compareLink.hasClass('ComparisonIcon-active')) {
        plansToCompare.remove($compareLink.data('plan-id'));
    }else{
        plansToCompare.add($compareLink.data('plan-id'));
    }

    $compareLink.toggleClass('ComparisonIcon-active');
    displayHeaderCompareIcon();
    toolTip();
});

toolTip();
// END: compare toggling behaviour



