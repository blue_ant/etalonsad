$(function() {
    const $document = $(document);
    const active = '_active';
    const burger = $('.header__burger');

    let hideSidebar = () => {
        let $sidebar = $('.call-form-sidebar__inner');

        $sidebar.css({
            transform: 'translateX(-100%)'
        });

        setTimeout(function() {
            $('[data-call-form-sidebar]').removeClass(active);
            $('.call-form-sidebar__inner').removeAttr('style');

            $sidebar.find('.call-form-inner_sendMessage').remove();
            $sidebar.find('.call-form-inner').show();
        }, 300);

        burger.show();
    };

    let showSidebar = () => {
        burger.hide();
        let $formToggleButton = $('[data-call-form-toggle]');
        let $sidebar = $('[data-call-form-sidebar]');
        $formToggleButton.addClass(active);
        $sidebar.addClass(active);
    };


    $('[data-call-form-toggle]').on('click', function(e) {
        e.preventDefault();
        showSidebar();
    });

    $document.on('click', function(e) {
        var $target = $(e.target);
        if (
            $target.is('.call-form-sidebar__close') ||
            $target.is('.call-form-sidebar__btn_ok') ||
            !$target.closest('.call-form-sidebar__inner').length &&
            !$target.closest('[data-call-form-toggle]').length
        ) {
            hideSidebar();
        }
    });

    /*$('[data-mask]').each(function() {
        let $this = $(this);
        let type = $this.data('mask');
        let mask;

        if (type === 'phone') {
            mask = '+7 (999) 999-99-99';
        }
        let options = {
            mask: mask,
            showMaskOnHover: false,
        };

        $this.inputmask(options);
    });*/

    $('.call-form__inp')
        .attr('autocomplete', 'off')
        .on('focus', function(/*event*/) {
            $(this).addClass(active);
        })
        .on('blur', function(/*event*/) {
            let $this = $(this);
            if ($this.val() === '') {
                $this.removeClass(active);
                return true;
            }
        });

    function amount2Word(amount, words) {
        var resOfHundred = amount % 100;
        var restOfTen = amount % 10;
        var resultString;

        switch (true) {
            case (resOfHundred >= 5 && resOfHundred <= 20):
                resultString = ' ' + words[2];

                break;
            default:
                switch (true) {
                    case (restOfTen == 1):
                        resultString = ' ' + words[0];

                        break;
                    case (restOfTen >= 2 && restOfTen <= 4):
                        resultString = ' ' + words[1];

                        break;
                    default:
                        resultString = ' ' + words[2];

                        break;
                }
                break;
        }
        return (resultString);
    }


    $(".call-form").on('submit', function(event) {
        event.preventDefault();
        let $form = $(this);

        let dataToSend = $.extend(true, $form.serializeObject(), {
            Submit: 1,
            url: window.location.href,
        });

        $.ajax({
            url: $form.data("action"),
            type: $form.attr("method"),
            data: dataToSend,
        }).done((response) => {
            let errorCode = parseInt(response.code);
            if (errorCode === 0) {
                $form.trigger('reset').parent().hide();

                let successText = `
<div class="call-form-inner call-form-inner_sendMessage">
    <em class="call-form-sidebar__close"></em>
    <div class="call-form-sidebar__tlt call-form-sidebar__tlt_ok">Заявка отправлена!</div>
    <div class="call-form-sidebar__text">
        ${response.success}
    </div>
    <button class="call-form-sidebar__btn call-form-sidebar__btn_ok">ok</button>
</div>`;
                window.requestAnimationFrame(() => {
                    $form.parent().hide().after(successText);
                });

            } else {
                alert("Не удалось отправить форму! Попробуйте позже или обратитесть по телефону...");
            }
        }).always(( /*response*/ ) => {

        });
        return false;
    });
});
