class Calculator {
    constructor(el) {
    }

    static flatsUpdate() {
        let price = $('#mortgage-calc-price').slider('option', 'value');

        $.ajax({
            url: '/choose/',
            dataType : 'json',
            data: {
                action: 'get_flats',
                limit: 15,
                sorting: 'rand',
                price_from: price - 500000,
                price_to: price + 500000,
            },
            async    : false,
            success  : function(data) {
                let flats = data.flats;

                $flats.html('');

                $.each(flats, function(i, e) {
                    let favourite   = e.favourite ? ' _active' : '';
                    let favTitle    = e.favourite ? 'Убрать из избранного' : 'Добавить в избранное';
                    let isFinish    = e.finish;
                    let finishClass = isFinish ? ' _flag' : '';
                    let finish      = isFinish ? '<span class="flat-icons__item _flag" data-title="Флаг" data-favourite=""></span>' : '';
                    let decor       = e.decor ? '<span class="flat-icons__item _decor" data-title="Квартира с отделкой" data-favourite=""></span>' : '';
                    let template    = `
                        <a href="` + e.href + `" class="calculator-flats__item` + finishClass + `" data-flat-id="` + e.id + `">
                            <div class="flat-icons">
                                <span class="flat-icons__item _favourite` + favourite + `" data-title="` + favTitle + `" data-favourite=""></span>
                                ` + finish + `
                                ` + decor + `
                            </div>
                            <div class="calc-flat-inner">
                                <div class="calculator-flats__img-wrap">
                                    <img src="` + e.pic + `" alt="">
                                </div>
                                <footer class="calculator-flats-footer">
                                    <div class="calculator-flats-footer__item">
                                        <div>` + e.rooms + `к квартира №13</div>
                                        <div>Кор. ` + e.house + `, сек. ` + e.partitions + `, эт. ` + e.floor + `</div>
                                    </div>
                                    <div class="calculator-flats-footer__item">
                                        <div>` + e.area + ` м<sup>2</sup></div>
                                        <div><span class="price">` + Number(e.price).toLocaleString("ru-RU") + ` Р</span></div>
                                    </div>
                                </footer>
                            </div>
                        </a>`;

                    $flats.append(template);
                });

                $flats.removeClass(loading);
            },
            error: function(e) {
                console.log("Не удалось получить данные с сервера!\nПопробуйте позже.");
            }
        });
    }

    static banksUpdate() {
        let that = this;

        this.filterBanks();

        $banks.html('');

        $.each(availableBanks, function(i, e) {
            let id    = e.id;
            let image = e.image;
            let rate  = e.filterRate;

            that.calcCreditSumm(rate);

            let template = `
                <div class="mortgage-banks__row" data-bank-id="` + id + `">
                    <div class="mortgage-banks__col">
                        <img src="` + image + `" class="mortgage-banks__logo" alt="">
                    </div>
                    <div class="mortgage-banks__col">
                        от ` + rate + `%
                    </div>
                    <div class="mortgage-banks__col" data-annuity>
                        ` + Number(creditSumm).toLocaleString("ru-RU") + ` руб.
                    </div>
                </div>`;

            $banks.append(template);
        });
    }

    static filterBanks() {
        let $costSlider = $('#mortgage-calc-price');
        let $feeSlider  = $('#mortgage-fee');
        let $yearSlider = $('#mortgage-age');

        let cost  = $costSlider.slider('option', 'value');
        let fee   = $feeSlider.slider('option', 'value') / cost * 100;
        let terms = Number($yearSlider.slider('option', 'value'));

        let total =  Number(cost - $feeSlider.slider('option', 'value'));

        $('[data-credit-summ] span:last').html(Number(total).toLocaleString("ru-RU") + 'Р');

        availableBanks = _.filter(allBanks, function(o) {
            let filterFee    = o.filterFee;
            let filterTerms  = o.filterTerms;
            let termIsObject = (typeof filterTerms === 'object');
            let feeIsObject  = (typeof filterFee === 'object');

            let filterTermsLow  = termIsObject ? filterTerms[0] : 0;
            let filterTermsHigh = termIsObject ? filterTerms[1] : filterTerms;

            return (feeIsObject ? fee <= filterFee[1] : fee >= filterFee) && (terms >= filterTermsLow && terms <= filterTermsHigh);
        });

        availableBanks = _.sortBy(availableBanks, function(o) {
            return o.filterRate;
        });

        return availableBanks;
    }

    static calcCreditSumm(rate) {
        let type    = $('.switch-type__item._active').data('switch-type');
        let fee     = Number($('#mortgage-fee').slider('option', 'value'));
        let time    = Number($('#mortgage-age').slider('option', 'value'));
        let cost    = Number($('#mortgage-calc-price').slider('option', 'value'));
        let payment = Number($('#mortgage-payment').slider('option', 'value'));

        if (type === 'price') {
            creditSumm = this.calculatePayment(cost, fee, time, rate);
        } else if (type === 'payment') {
            creditSumm = this.calculatePrice(payment, fee, time, rate);

            if (creditSumm < $('#mortgage-calc-price').slider('option', 'min')) {
                creditSumm = $('#mortgage-calc-price').slider('option', 'min');
            }
        }
    }

    static loaderInit() {
        $banks.addClass(loading);
        $flats.addClass(loading);
    }

    static loaderRemove() {
        $banks.removeClass(loading);
        $flats.removeClass(loading);
    }

    static calculatePrice(payment, fee, time, rate) {
        rate = rate / 100 / 12;
        time = time * 12;

        let res1 = rate * Math.pow(1 + rate, time);
        let res2 = Math.pow(1 + rate, time) - 1;
        let res3 = res1 / res2;
        let result = ( (payment/res3) - fee).toFixed();

        return Number(result);
    }

    static calculatePayment(cost, fee, time, rate) {
        rate = rate / 100 / 12;
        time = time * 12;

        let res1 = rate * Math.pow(1 + rate, time);
        let res2 = Math.pow(1 + rate, time) - 1;
        let res3 = res1 / res2;
        let result = ((cost - fee) * res3).toFixed();

        return Number(result);
    }

    static calcMinMaxPayment(value) {
        var $calcPrice = $('#mortgage-calc-price');
        var $paymentInp = $('#mortgage-payment-input');

        let fee   = $('#mortgage-fee').slider('option', 'value');
        let time  = $('#mortgage-age').slider('option', 'value');
        let minPrice = $calcPrice.slider('option', 'min');
        let maxPrice = $calcPrice.slider('option', 'max');
        let minPayment = Calculator.calculatePayment(minPrice, 0, 30, minRate) / 1000;
        let maxPayment = Calculator.calculatePayment(maxPrice, 0, 30, minRate) / 1000;

        let payment = Calculator.calculatePayment(value, fee, time, minRate);

        $('#mortgage-payment')
            .slider({
                max: maxPayment * 1000,
                min: minPayment * 1000,
                value: payment
            })
            .next('.slider-sings')
            .find('.slider-sings__item')
            .eq(0)
            .html(parseFloat(minPayment).toFixed(1) + ' тыс')
            .next()
            .html(parseFloat(maxPayment / 2).toFixed(1) + ' тыс')
            .next()
            .html(parseFloat(maxPayment).toFixed(1) + ' тыс');

        $paymentInp.val(Number(payment).toLocaleString("ru-RU"))
    }
}
