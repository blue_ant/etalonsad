class Sorter {
    constructor(el) {
        let $el = $(el);
        let $sortInput = $el.find("input[name='sort']");
        let $dirInput = $el.find("input[name='sortdir']");
        let $labels = $el.find(".Sorter_lbl");

        {
            let sortVal = $sortInput.val();
            let dirVal = $dirInput.val();
            if (sortVal && dirVal) {
                $labels.each((index, el) => {
                    let $lbl = $(el);
                    if ($lbl.data("val") === sortVal) {
                        $lbl.addClass("Sorter_lbl-" + dirVal);
                        return false;
                    }
                });
            }
        }

        $labels.on("click", (event) => {
            event.preventDefault();
            let $lbl = $(event.currentTarget);
            let currentDirVal = $lbl.attr("class").split("Sorter_lbl-")[1] || "none";
            $labels.not($lbl).removeClass("Sorter_lbl-asc Sorter_lbl-desc");

            switch (currentDirVal) {
                case "none":
                    $lbl.addClass("Sorter_lbl-asc");
                    $sortInput.val($lbl.data("val"));
                    $dirInput.val("asc");
                    break;
                case "asc":
                    $lbl.removeClass("Sorter_lbl-asc").addClass("Sorter_lbl-desc");
                    $sortInput.val($lbl.data("val"));
                    $dirInput.val("desc");
                    break;
                case "desc":
                    $lbl.removeClass("Sorter_lbl-desc");
                    $sortInput.val("");
                    $dirInput.val("");
                    break;
            }

            $sortInput.trigger("change");
        });
        this.$el = $el;
    }
}
