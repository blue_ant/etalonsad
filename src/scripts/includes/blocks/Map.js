const $window = $(window);

class Map {
	constructor($map) {
		let that     = this;
		let mapID    = $map.attr('id');
		let mapType  = $map.data('map');
		let initData = $map.data('map-init');

		ymaps.ready(() => {
			let center   = initData.center ? initData.center : '';
			let zoom     = initData.zoom ? initData.zoom : '';
			let controls = '' ? initData.controls : [];
			let options  = {
				center   : center,
				zoom     : zoom,
				controls : controls
			}

			switch (mapType) {
				case 'contacts':
					that.contactsMap(mapID, initData, options, $map);

					break;
				case 'location-route':
					that.routeMap(mapID, initData, options);

					break;
				case 'location-infrastructure':
					let url = $map.data('url');

					that.infraMap(mapID, initData, options, url);

					break;
				default:
					console.info('Не определен тип карты');
			}
		});
	}

	contactsMap(id, data, options, $map) {
		var $arrow      = $('.close-region-office');
		var $region     = $('.region-item');
		var $regionList = $('.region-list');
		var $tabs       = $('[data-contacts-map]');

		let regionInit   = $tabs.eq(2).data('contacts-map');

		var myMap = new ymaps.Map(id, options);

		ymapsTouchScroll(myMap);

		myMap.behaviors.disable('scrollZoom');

		var zoomControl = new ymaps.control.ZoomControl({
			options: {
				size: "small",
				position: {
					right: 20,
					bottom: 40
				}
			}
		});
		myMap.controls.add(zoomControl);

		Map.addMarker(myMap, data, data.type, id);

		$tabs.on('click', function() {
			let $curTab    = $(this);
			let curOptions = $curTab.data('contacts-map');
			var type       = curOptions.type;

			/*$arrow.hide();*/

			myMap.geoObjects.removeAll();

			switch (type) {
				case 'offices':
					let $offices = $('[data-office-placemark]');

					$offices.each(function() {
						let officeData = $(this).data('office-placemark');
						let officeID   = officeData.id;
						let zIndex = officeData.zIndex;

						Map.addMarker(myMap, officeData, type, officeID, zIndex);
					});

					let $officesToolp = $('[data-office-toolp]');

					$officesToolp.each(function() {
						let officeToolp = $(this).data('office-toolp');
						let officeID   = officeToolp.id;
						let zIndex = officeToolp.zIndex;

						Map.addMarker(myMap, officeToolp, type, officeID, zIndex);

					});

					break;
				case 'regions':
					let $regions = $('[data-region-placemark]');

					$regions.each(function() {
						let $regionLink = $(this);
						let regionData  = $regionLink.data('region-placemark');
						let regionID    = regionData.id;

						Map.addMarker(myMap, regionData, type, regionID);

						$regionLink.click(function(e) {
							e.preventDefault();

							Map.toggleRegions(myMap, regionID, regionData);
						});
					});

					break;
				default:
					Map.addMarker(myMap, curOptions);
			}

			Map.setCenter(myMap, curOptions);
		});

		$arrow.click(function() {
			$region.hide();
			$regionList.show();
			$(this).hide();

			Map.setCenter(myMap, regionInit);
		});
	}

	routeMap(id, data, options) {
		var myMap = new ymaps.Map(id, options);

		myMap.behaviors.disable('scrollZoom');

		Map.addMarker(myMap, data, data.type, id);
	}

	infraMap(id, data, options, url) {
		var myMap = new ymaps.Map(id, options);

		myMap.behaviors.disable('scrollZoom');
		var zoomControl = new ymaps.control.ZoomControl({
	        options: {
	            size: "small",
	            position: {
	            	top: 20,
	            	right: 20
	            }
	        }
	    });

		myMap.controls.add(zoomControl);

		$.getJSON(url, function(data) {
			Map.addCollection(myMap, data);
		});

		let $showAll = $('[data-infra-show]');
		let $hideAll = $('[data-infra-hide]');

		$showAll.on('click', (e) => {
			e.preventDefault();

			$('.longread4-category-links__item.longread4-category-links__item_off').click();
		});

		$hideAll.on('click', (e) => {
			e.preventDefault();

			$('.longread4-category-links__item:not(.longread4-category-links__item_off)').click();
		});
	}

	static addCollection(myMap, data) {
		for (var i = 0, l = data.length; i < l; i++) {
	        Map.createGroup(myMap, data[i]);
	    }
	}

	static createGroup(myMap, group) {
		let collection = new ymaps.GeoObjectCollection(null, { preset: group.style });
		let type  = group.type;
		let $type = $('[data-category]');
		let off   = 'longread4-category-links__item_off';

		myMap.geoObjects.add(collection);

		$type.filter('[data-category="' + type + '"]').on('click', function(e) {
			let $this = $(this);

			e.preventDefault();

			if ($this.hasClass(off)) {
				$this.removeClass(off);
				myMap.geoObjects.add(collection);
			} else {
				$this.addClass(off);
				myMap.geoObjects.remove(collection);
			}
		});

		for (var j = 0, m = group.items.length; j < m; j++) {
            Map.addGroupMarkers(group.items[j], collection, group.icon);
        }
	}

	static addGroupMarkers(item, collection, iconUrl) {
		let placemark = new ymaps.Placemark(item.center, {
			hintContent: item.name
		}, {
			iconLayout: "default#image",
			iconImageHref: iconUrl,
			iconImageSize: [30, 30],
			iconImageOffset: [-15, -15],
		});

		collection.add(placemark);
	}

	static toggleRegions(myMap, regionID, data) {
		var $region     = $('.region-item');
		var $regionList = $('.region-list');
		var $arrow      = $('.close-region-office');

		$arrow.show();
		$regionList.hide();

		$region.hide().filter('#' + regionID).show();
		Map.setCenter(myMap, data);
	}

	static addMarker(myMap, options, type, id) {
		let center = options.center;
		let icon   = options.icon;
		let style  = options.style;
		let title  = options.title;
		let placemark;

		if ( style !== undefined ) {

			placemark = new ymaps.Placemark(center,
				{
					hintContent: '',
				},
				{
					balloonPanelMaxMapArea: 0,
					iconLayout: "default#image",
					iconImageHref: icon.url,
					iconImageSize: icon.size,
					iconImageOffset: [(-1 * icon.size[0]) / 2, -1 * icon.size[1]],
				}
			);

			placemark.events
				.add('click', function (event) {
					console.log('click')
					$('body').find('.contacts__tooltip').remove();
					let _this = event.get('target').getOverlaySync().getLayoutSync().getElement(); // Родительский контейнер html.
					let pinner = _this.children[0]; // Вложенный элемент.
					let tpl = `<div class="contacts__tooltip _${style}">${title}</div>`;

					$(pinner).addClass('marker_hover');
					$(pinner).append(tpl);
				})
				.add('mouseenter', function (event) {
					console.log('hover')
					let _this = event.get('target').getOverlaySync().getLayoutSync().getElement(); // Родительский контейнер html.
					let pinner = _this.children[0]; // Вложенный элемент.
					let tpl = `<div class="contacts__tooltip _${style}">${title}</div>`;

					$(pinner).addClass('marker_hover');
					$(pinner).append(tpl);
				})
				.add('mouseleave', function (event) {
					let _this = event.get('target').getOverlaySync().getLayoutSync().getElement(); // Родительский контейнер html.
					let pinner = _this.children[0]; // Вложенный элемент.

					$(pinner).removeClass('marker_hover');
					$(pinner).find('.contacts__tooltip').remove();
				})

		} else {
			placemark = new ymaps.Placemark(center,
				{
					hintContent: options.title,
				},
				{
					balloonPanelMaxMapArea: 0,
					iconLayout: "default#image",
					iconImageHref: icon.url,
					iconImageSize: icon.size,
					iconImageOffset: [(-1 * icon.size[0]) / 2, -1 * icon.size[1]],
				}
			);
		}





		myMap
			.geoObjects
			.add(placemark);

		switch (type) {
			case 'offices':
				Map.officeClick(placemark, id);

				break;
			case 'regions':
				Map.regionClick(myMap, placemark, id, options)

				break;
			case 'route':
				Map.drawRoute(myMap, options)

				break;
			default:
				console.log(type);
		}
	}

	static setCenter(myMap, options) {
		let center = options.center;
		let zoom   = options.zoom;

		myMap.setCenter(center, zoom);
	}

	static regionClick(myMap, placemark, regionID, options) {
		placemark.events.add('click', function () {
			Map.toggleRegions(myMap, regionID, options);

			Map.setCenter(myMap, options);
		});
	}

	static officeClick(placemark, officeID) {
		console.log(officeID)
		placemark.events.add('click', function () {
			let $offices = $('.contact-offices-item__type');

			$offices.hide().filter('#' + officeID).show();
		});
	}

	static drawRoute(myMap, options) {
		let route       = options.route;
		let routeStart  = route.start;
		let routeEnd    = route.end;
		let routeMiddle = route.middle;

		var referencePoints = [
            routeStart,
            routeEnd
        ];

		var multiRoute = new ymaps.multiRouter.MultiRoute({
	        referencePoints: referencePoints,
	        params: {results: 2}
	    }, {
	    	zoomMargin: 10,
	        wayPointFinishIconLayout: "default#image",
	        wayPointFinishIconImageHref: route.startIco,
	        wayPointFinishIconImageSize: [18, 18],
	        wayPointFinishIconImageOffset: [-9, -9],
	        // Позволяет скрыть иконки путевых точек маршрута.
	        // wayPointVisible: false,

	        // Внешний вид транзитных точек.
	        viaPointIconRadius: 7,
	        viaPointIconFillColor: "#000088",
	        viaPointActiveIconFillColor: "#E63E92",
	        // Транзитные точки можно перетаскивать, при этом
	        // маршрут будет перестраиваться.
	        viaPointDraggable: true,
	        // Позволяет скрыть иконки транзитных точек маршрута.
	        viaPointVisible: false,

	        // Внешний вид точечных маркеров под путевыми точками.
	        pinIconFillColor: "#000088",
	        pinActiveIconFillColor: "#B3B3B3",
	        // Позволяет скрыть точечные маркеры путевых точек.
	        // pinVisible:false,

	        // Внешний вид линии маршрута.
	        routeStrokeWidth: 2,
	        routeStrokeColor: "#000088",
	        routeActiveStrokeWidth: 6,
	        routeActiveStrokeColor: "#ff0000",

	        // Внешний вид линии пешеходного маршрута.
	        routeActivePedestrianSegmentStrokeStyle: "solid",
	        routeActivePedestrianSegmentStrokeColor: "#00CDCD",

	        // Автоматически устанавливать границы карты так, чтобы маршрут был виден целиком.
	        boundsAutoApply: true
	    });

		myMap.geoObjects.add(multiRoute);

		var placemark = new ymaps.Placemark(routeMiddle,
			{
				hintContent: '',
			},
			{
				iconLayout: "default#image",
				iconImageHref: route.middleIco,
				iconImageSize: [120, 50],
				iconImageOffset: [-20, 0],
			}
		);

		myMap
			.geoObjects
			.add(placemark);
	}
}
