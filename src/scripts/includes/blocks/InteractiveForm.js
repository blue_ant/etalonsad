$.validator.addMethod(
    "regex",
    function(value, element, regexp) {
        var re = new RegExp(regexp);
        return this.optional(element) || re.test(value);
    },
    "Поле заполнено не верно"
);

class InteractiveForm {
    constructor(el, opts = {}) {

        this.$form = $(el);

        let $form = this.$form;

        if ($form.data("action")) {
            $form.prop('action', $form.data("action"));
        }

        $form.find('[name="phone"]').inputmask({
            mask: "+7 999 999-99-99",
            showMaskOnHover: false,
        });

        $form.find('[name="captcha"]').inputmask({
            mask: "9999",
            showMaskOnHover: false
        });

        $form.find('.booking-popup__inp')
			.on('focus', (e) => {
				$(e.currentTarget).addClass('_focus')
			})
			.on('focusout', (e) => {
				if ($(e.currentTarget).val() == '') {
					$(e.currentTarget).removeClass('_focus');
				}
				else {
					$(e.currentTarget).addClass('_focus');
				}
			})
		
			$(document).on('click', (e) => {
				if ($(e.target).parent('.booking-popup__inpLabel').length == 0) {
					$form.find('.booking-popup__inp').each((i, e) => {
						if ($(e).val() !== '') {
							$(e).addClass('_focus');
						}
					})
				}
			});

        let validatorOpts = {
            rules: {
                phone: {
                    required: true,
                    regex: /\+7\s\d\d\d\s\d\d\d\-\d\d\-\d\d/
                },
				email: {
					required: true,
					regex: /\S+@\S+\.\S+/
				},
                captcha: {
                    required: true,
                    regex: /\d\d\d\d/
                },

            },
            errorElement: "em",
            onfocusout: (el/*, event*/) => {
                if ($(el).val() !== '') {
					$(el).valid();

				}
            },

            focusCleanup: false,
            submitHandler: opts.submitHandler || this.standartFormHandler, //(form)=>{}
            errorPlacement: ($errorLabel, $el) => {
                if ($el.attr('name') === "agree") {
                    return true;
                } else {
                    $errorLabel.addClass('Form_hint Form_hint-error');
                    $el.after($errorLabel);
                    return true;
                }
            },
        };

        if (opts.validatorParams) {
            $.extend(true, validatorOpts, opts.validatorParams);
        }

        if (opts.successBlockMod) {
            $.extend(true, opts, { successBlockMod: "default" });
        }

        this.opts = opts;
        this.validator = $form.validate(validatorOpts);

    }

    standartFormHandler(form) {
        let $form = $(form);
        // window.pagePreloader.show();

        let dataToSend = $.extend(true, $form.serializeObject(), {
            Submit: 1,
            url: window.location.href,
        });

        $.ajax({
            url: form.action,
            type: form.method,
            data: dataToSend,
        }).done((response) => {
            let errorCode = parseInt(response.code);
            if (errorCode === 0) {
                let successText =
                    `<div class="FormSuccess">` +
                    `<div class="FormSuccess_title">Заявка отправлена</div>` +
                    `<div class="FormSuccess_text ">${response.success}</div>` +
                    `<button class="FormSuccess_btn" data-fancybox-close>Хорошо, спасибо</button>` +
                    `</div>`;
                window.requestAnimationFrame(() => {
                    $form.parent().find('.booking-popup__title, .booking-popup__desc').hide();
                    $form.html(successText);
                });

            } else {
                alert("Не удалось отправить форму! Попробуйте позже или обратитесть по телефону...");
            }
        }).always(( /*response*/ ) => {
            // window.pagePreloader.hide();
        });
    }

    destroy() {
        this.validator.destroy();
        this.$form.find('input').inputmask('remove');
    }

}