class FilterForm {
    constructor(
        el,
        opts = {
            submitHandler: $.noop,
        }
    ) {
        if (!el) {
            console.error('Filter class constructor requires "el" argument!');
            return;
        }

        this.offset = 0;

        const submitHandler = _.debounce(opts.submitHandler, 600);

        let $filterForm = $(el);
        this.$filterForm = $filterForm;

        this.restoreStateFromURL();

        if (!$filterForm.length) {
            console.error('FilterForm constructor can\'t find given "el" in DOM!');
            return;
        }

        let multicheckboxes = [];
        $filterForm.find(".Multycheckbox").each((index, el) => {
            multicheckboxes.push(new Multicheckbox(el));
        });

        let rangeSliders = [];
        $filterForm.find(".RangeSlider").each(function(index, el) {
            rangeSliders.push(new RangeSlider(el));
        });

        let selectLists = [];

        $filterForm.find("select").each((index, el) => {
            selectLists.push(
                $(el).selectmenu({
                    change: function(event, ui) {
                        $(ui.item.element).trigger("change");
                    },
                    appendTo: ".FilterForm",
                    classes: {
                        "ui-selectmenu-button": "FilterForm_select",
                        "ui-selectmenu-menu": "FilterForm_selectlistMenu",
                    },
                    position: {
                        my: "left top",
                        at: "left bottom",
                        collision: "flip",
                    },
                })
            );
        });

        $filterForm.find(".FilterForm_accordion").accordion({
            collapsible: true,
            animate: 0,
            active: 0,
            classes: {
                "ui-accordion-content": "FilterForm_collapsibleGroup",
            },
        });

        let $inputs = $($filterForm.get(0).elements);

        $inputs.on("change", (event) => {
            event.preventDefault();
            this.offset = 0;
            $filterForm.trigger("submit");
        });

        $filterForm.on("submit", (event) => {
            event.preventDefault();
            submitHandler($filterForm);
            this.saveStateToURL();
            return false;
        });

        $(".FiltersOpts_resetBtn").on("click", () => {
            this.resetFilterForm();
        });


        // remember thirdparties url params
        {
            let urlParams = parseQueryString(window.location.search);

            $inputs.each((index, el)=> {
                if (urlParams[el.name]) {
                    delete urlParams[el.name];
                }
            });

            let otherPossibleParams = [
                "flat_num",
                "sort",
                "sortdir",
                "offset",
            ];
            
            for (var i = otherPossibleParams.length - 1; i >= 0; i--) {
                delete urlParams[otherPossibleParams[i]];
            }

            let thirdpartyUrlParamsStr = "";
            for (let key in urlParams) {
                thirdpartyUrlParamsStr += `&${key}=${urlParams[key]}`;
            }

            this.thirdpartyUrlParamsStr = thirdpartyUrlParamsStr;
        }

        this.$inputs = $inputs;
        this.rangeSliders = rangeSliders;
        this.multicheckboxes = multicheckboxes;

        $filterForm.trigger("submit");
    }

    resetFilterForm() {
        this.$filterForm[0].reset();
        this.rangeSliders.forEach((rangeSliderInst) => {
            rangeSliderInst.reset();
        });

        this.multicheckboxes.forEach((multiCheckboxInst) => {
            multiCheckboxInst.reset();
        });
    }

    saveStateToURL() {
        let queryStr = this.$filterForm.serialize() + `&offset=${this.offset}`;
        let newURL = location.pathname + "?" + queryStr + this.thirdpartyUrlParamsStr;
        history.replaceState({}, document.title, newURL);
    }

    restoreStateFromURL() {
        let storedValues = parseQueryString(window.location.search);

        // console.log("restored form states:", storedValues);
        let $inps = $(this.$filterForm.get(0).elements);

        for (let p in storedValues) {
            if (storedValues.offset) {
                this.offset = storedValues.offset;
            }
            // TODO: optimize selector for filtering inputs
            $inps.filter(`[name='${p}'], [name='${p}[]']`).each((index, el) => {
                if (el.type && (el.type === "checkbox" || el.type === "radio")) {
                    let isChecked = true;

                    if (_.isArray(storedValues[p])) {
                        isChecked = _.includes(storedValues[p], el.value);
                    }

                    el.checked = isChecked;
                } else if (el.tagName === "SELECT") {
                    if (storedValues[p]) {
                        for (var i = 0; i < el.options.length; i++) {
                            let opt = el.options[i];
                            if (storedValues[p].includes(opt.value)) {
                                opt.selected = "selected";
                            }
                        }
                    }
                } else {
                    el.value = storedValues[p];
                }
            });
        }
    }
}
