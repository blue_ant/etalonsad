class RangeSliderMortgage {
    constructor(el) {
        if (!el) {
            console.error("RangeSlider constructor can\'t find given \"el\" in DOM!");
            return;
        }

        let $slider  = $(el);
        var $wrapper = $slider.closest('.page-form__lbl');
        var $input   = $wrapper.find('input');
        var value    = $input.val();
        var step     = $input.data('step');
        var minRange = $input.data('min-range');
        var maxRange = $input.data('max-range');
        var type     = $slider.data('slider');
        var isClac   = $slider.closest('[data-calculator]').length;
        var that     = this;
        var init     = false;
        var modified = false;
        var timeout;

        $input.val(Number(value).toLocaleString("ru-RU"));

        $slider.slider({
            range : 'min',
            step  : step,
            min   : minRange,
            max   : maxRange,
            value : value,
            slide : function(event, ui) {
                let value = ui.value;

                $input.val(Number(value).toLocaleString("ru-RU"));

                if (isClac) {
                    Calculator.loaderInit();
                }

                if (type === 'age') {
                    let $age = $('.page-form__age');

                    $age.text(that.amount2Word(value, ['год', 'года', 'лет']));
                } else if (type === 'price') {
                    let curFeeVal = Number($('[data-input="fee"]').val().replace(/\D/g,'').substring(0,8));
                    let currency  = (value > 999999) ? ' млн' : ' тыс';
                    let curKoef   = (value > 999999) ? 1000000 : 100000;
                    let fee       = $('#mortgage-fee').slider('option', 'value');
                    let time      = $('#mortgage-age').slider('option', 'value');
                    let payment   = Calculator.calculatePayment(value, fee, time, minRate);

                    $('[data-slider="fee"]')
                        .slider({
                            max: value * .8
                        })
                        .next('.slider-sings')
                        .find('.slider-sings__item')
                        .eq(1)
                        .html(parseFloat(value / 2 / curKoef * .8).toFixed(1) + currency)
                        .next()
                        .html(parseFloat(value / curKoef * .8).toFixed(1) + currency);

                    $('[data-input="fee"]')
                        .attr('data-max-range', value * .8)
                        .prop('data-max-range', value * .8)
                        .data('max-range', value * .8);

                    $('#mortgage-payment').slider({
                        value: payment
                    });

                    $('#mortgage-payment-input').val(Number(payment).toLocaleString("ru-RU"));

                    if (value * .8 < curFeeVal) {
                        $('[data-input="fee"]').val(Number(value * .8).toLocaleString("ru-RU"));
                        $('[data-slider="fee"]').slider({
                            value: value * .8
                        });
                    }
                } else if (type === 'payment') {
                    let fee   = $('#mortgage-fee').slider('option', 'value');
                    let time  = $('#mortgage-age').slider('option', 'value');
                    let minPrice = $('#mortgage-calc-price').slider('option', 'min');
                    let price = Calculator.calculatePrice(value, fee, time, minRate);

                    if (price < minPrice) {
                        price = minPrice;
                    }

                    $('#mortgage-calc-price').slider({
                        value: price
                    });
                    $('#mortgage-price-input').val(Number(price).toLocaleString("ru-RU"));

                    let curFeeVal = Number($('[data-input="fee"]').val().replace(/\D/g,'').substring(0,8));
                    let currency  = (price > 999999) ? ' млн' : ' тыс';
                    let curKoef   = (price > 999999) ? 1000000 : 100000;

                    $('[data-slider="fee"]')
                        .slider({
                            max: price * .8
                        })
                        .next('.slider-sings')
                        .find('.slider-sings__item')
                        .eq(1)
                        .html(parseFloat(price / 2 / curKoef * .8).toFixed(1) + currency)
                        .next()
                        .html(parseFloat(price / curKoef * .8).toFixed(1) + currency);

                    if (price * .8 < $('#mortgage-fee').slider('option', 'value')) {
                        $('#mortgage-fee').slider({
                            value: price * .8
                        });
                        $('#mortgage-fee-input').val(Number(price * .8).toLocaleString("ru-RU"));
                    }
                }
            },
            create: function(e, ui) {
                let total   = $('[data-calculator]').find('[data-slider]').length;
                let current = $('[data-slider].ui-slider').length;

                if (isClac) {
                    if (total === current) {
                        var value = $('#mortgage-calc-price').slider('option', 'value');

                        Calculator.loaderRemove();
                        Calculator.calcMinMaxPayment(value);
                        Calculator.banksUpdate();
                        Calculator.flatsUpdate();
                    }
                }
            },
            stop: function(e, ui) {
                if (isClac) {
                    Calculator.banksUpdate();
                    Calculator.flatsUpdate();
                    Calculator.loaderRemove();
                }
            }
        });

        $input.keydown(function (e) {
            if (modified) {
                clearTimeout(timeout);
            }

            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [9, 27, 13, 110, 190]) !== -1 ||
                 // Allow: Ctrl+A, Command+A
                (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                 // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                     // let it happen, don't do anything
                    console.log('do nothing');

                    return;
            }

            // Ensure that it is a number and stop the keypress
            if ( $.inArray(e.keyCode, [46, 8]) === -1 && (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105) ) {
                e.preventDefault();

                return;
            }

            let $this = $(this);

            inputInit();

            function inputInit() {
                let value  = $this.val().replace(/\D/g,'');
                let maxVal = $this.data('max-range');
                let minVal = $this.data('min-range');
                let $curSlider = $this.closest('.page-form__step').find('[data-slider]');

                modified = true;

                timeout = setTimeout(startLoad, 500);

                function startLoad() {
                    if (value > maxVal) {
                        value = maxVal;
                    } else if (value < minVal) {
                        value = minVal;
                    }

                    if (isClac) {
                        Calculator.loaderInit();
                    }

                    if (type === 'age') {
                        let $age = $('.page-form__age');

                        $age.text(that.amount2Word(value, ['год', 'года', 'лет']));

                        $('[data-slider="age"]').slider({
                            value: value
                        });
                    } else if (type === 'price') {
                        let curFeeVal = Number($('[data-input="fee"]').val().replace(/\D/g,'').substring(0,8));
                        let currency  = (value > 999999) ? ' млн' : ' тыс';
                        let curKoef   = (value > 999999) ? 1000000 : 100000;
                        let fee       = $('#mortgage-fee').slider('option', 'value');
                        let time      = $('#mortgage-age').slider('option', 'value');
                        let payment   = Calculator.calculatePayment(value, fee, time, minRate);

                        $('[data-slider="fee"]')
                            .slider({
                                max: value * .8
                            })
                            .next('.slider-sings')
                            .find('.slider-sings__item')
                            .eq(1)
                            .html(parseFloat(value / 2 / curKoef * .8).toFixed(1) + currency)
                            .next()
                            .html(parseFloat(value / curKoef * .8).toFixed(1) + currency);

                        $('[data-input="fee"]')
                            .attr('data-max-range', value * .8)
                            .prop('data-max-range', value * .8)
                            .data('max-range', value * .8);

                        $('#mortgage-payment').slider({
                            value: payment
                        });

                        $('#mortgage-payment-input').val(Number(payment).toLocaleString("ru-RU"));

                        if (value * .8 < curFeeVal) {
                            $('[data-input="fee"]').val(Number(value * .8).toLocaleString("ru-RU"));
                            $('[data-slider="fee"]').slider({
                                value: value * .8
                            });
                        }

                        $('[data-slider="price"]').slider({
                            value: value
                        });
                    } else if (type === 'fee') {
                        $('[data-slider="fee"]').slider({
                            value: value
                        });
                    } else if (type === 'payment') {
                        let fee   = $('#mortgage-fee').slider('option', 'value');
                        let time  = $('#mortgage-age').slider('option', 'value');
                        let minPrice = $('#mortgage-calc-price').slider('option', 'min');
                        let price = Calculator.calculatePrice(value, fee, time, minRate);

                        if (price < minPrice) {
                            price = minPrice;
                        }

                        $('#mortgage-calc-price').slider({
                            value: price
                        });
                        $('#mortgage-price-input').val(Number(price).toLocaleString("ru-RU"));

                        let curFeeVal = Number($('[data-input="fee"]').val().replace(/\D/g,'').substring(0,8));
                        let currency  = (price > 999999) ? ' млн' : ' тыс';
                        let curKoef   = (price > 999999) ? 1000000 : 100000;

                        $('[data-slider="fee"]')
                            .slider({
                                max: price * .8
                            })
                            .next('.slider-sings')
                            .find('.slider-sings__item')
                            .eq(1)
                            .html(parseFloat(price / 2 / curKoef * .8).toFixed(1) + currency)
                            .next()
                            .html(parseFloat(price / curKoef * .8).toFixed(1) + currency);

                        if (price * .8 < $('#mortgage-fee').slider('option', 'value')) {
                            $('#mortgage-fee').slider({
                                value: price * .8
                            });
                            $('#mortgage-fee-input').val(Number(price * .8).toLocaleString("ru-RU"));
                        }

                        $('[data-slider="payment"]').slider({
                            value: value
                        });
                    }

                    if (isClac) {
                        Calculator.banksUpdate();
                        Calculator.flatsUpdate();
                        Calculator.loaderRemove();
                    }

                    console.log('banks updated')

                    modified = false;
                }
            }
        });

        $input.on('focus', function(e) {
            let $this = $(this);
            let value = $this.val().replace(/\D/g,'');

            $this.val(value);
            modified = false;
        });

        $input.on('blur', function() {
            let $this      = $(this);
            let value      = $this.val().replace(/\D/g,'');
            let maxVal     = $this.data('max-range');
            let minVal     = $this.data('min-range');
            let $curSlider = $this.closest('.page-form__step').find('[data-slider]');

            if (value > maxVal) {
                value = maxVal;
            } else if (value < minVal) {
                value = minVal;
            } else if (value === '') {
                value = 0;
            }

            $this.val(Number(value).toLocaleString("ru-RU"));
        });
    }

    amount2Word(amount, words) {
        var resOfHundred = amount % 100;
        var restOfTen = amount % 10;
        var resultString;

        switch (true) {
            case (resOfHundred >= 5 && resOfHundred <= 20):
                resultString = ' ' + words[2];

                break;
            default:
                switch (true) {
                    case (restOfTen == 1):
                        resultString = ' ' + words[0];

                        break;
                    case (restOfTen >= 2 && restOfTen <= 4):
                        resultString = ' ' + words[1];

                        break;
                    default:
                        resultString = ' ' + words[2];

                        break;
                }
                break;
        }
        return (resultString);
    }
}
