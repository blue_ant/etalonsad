class FavoritesList {
    constructor(name) {
        const STORE_NAME = name;

        let favorites = localStorage.getItem(STORE_NAME);

        if (favorites) {
            favorites = JSON.parse(favorites);
        } else {
            favorites = [];
        }

        this.favorites = favorites;
        this.STORE_NAME = STORE_NAME;
    }

    add(id) {

        if (!_.includes(this.favorites, id)) {
            this.favorites.push(id);
            localStorage.setItem(this.STORE_NAME, JSON.stringify(this.favorites));
        }

        console.log("favorites now is: ", this.favorites);
    }

    remove(id) {
        this.favorites = _.without(this.favorites, id);
        localStorage.setItem(this.STORE_NAME, JSON.stringify(this.favorites));

        console.log("favorites now is: ", this.favorites);
    }

    isFavorite(id) {
        return _.includes(this.favorites, id);
    }

    getAll() {
        return this.favorites;
    }
}