class SorterMortgage {
  constructor() {
    let $sorter = $('[data-mortgage-sorter]');
    let $renderBodyBlock = $('[data-mortgage-body]')
    
    this.$sorter = $sorter;
    this.$renderBodyBlock = $renderBodyBlock;
    this.renderBanks('rateBefore', '_asc');
    $('[data-sort="rateBefore"]').addClass('_asc');
    this.initSortableCol();
  }

  renderBanks(type, modeClass) {
    console.log(type, modeClass)
    let banksRowTpl = _.template(
      `<div class="mortgage-banks__row" data-bank-id="<%= id %>">
          <div class="mortgage-banks__col">
              <img src="<%= logo %>" class="mortgage-banks__logo" alt="">
          </div>
          <div class="mortgage-banks__col">
          от <%= firstPayPercent %>%
          </div>
          <div class="mortgage-banks__col">
                  до <%= years %> лет
          </div>
          <div class="mortgage-banks__col">
              от <%= rateBefore %>%
          </div>
          <!--<div class="mortgage-banks__col">
              от <%= rateAfter %>%
          </div>-->
      </div>`.replace(/\s{2,}/g, "")
    );

    let ajaxUrl = this.$sorter.data('ajax');
    let $renderBlock = this.$renderBodyBlock;

    $.ajax({
        type: "get",
        url: ajaxUrl,
        success: function(response){
            console.log('response', response)
            let banksList = response.banksMortgage;
            
            if (type && modeClass == '_asc') {
              banksList = _.sortBy(banksList, type);
            }
            else if (type && modeClass == '_desc') {
              banksList = _.sortBy(banksList, type).reverse();
            }
            console.log(banksList)
            $renderBlock.html('');
            $(banksList).each(function (i, e) {
                $renderBlock.append(banksRowTpl(e));
            });

            $document.on('click', '[data-bank-id]', function() {
                var $rows = $('[data-bank-id]');
                let $row   = $(this);
                let bankID = $row.data('bank-id');
                var bankContent;
                var bankImage;
                var bankLicense;

                if ($row.hasClass(active)) {
                    $('.mortgage-banks__row-more').each(function() {
                        $(this).stop()
                            .animate({
                                height: 0
                            }, function() {
                                $row.removeClass(active);
                                $(this).remove();
                            });
                    });

                    return;
                }

                $('.mortgage-banks__row-more').each(function() {
                    $(this).stop()
                        .animate({
                            height: 0
                        }, function() {
                            $(this).remove();
                        });
                });

                $rows.removeClass(active);

                $.each(banksList, function(i, el) {
                    if (bankID === el.id) {
                        bankContent = el.descr;
                        bankImage   = el.image;
                        bankLicense = el.license;
                    }
                });

                if ($document.width() < 576) {
                    $row.after('<div class="mortgage-modal mortgage-banks__row-more"><div class="mortgage-modal__inner">' + '<div class="mortgage-modal_top"><div class="mortgage-modal_license">' + bankLicense + '</div></div>' + bankContent + '<div><a href="#" class="mortgage-modal_btn" data-fancybox-close onclick="SCBopen(1);">Оформить заявку</a></div></div></div>');

                    let $infoRow = $row.next();

                    $infoRow.stop().animate({
                        height: $infoRow.get(0).scrollHeight
                    });

                    $row.addClass(active);


                    setTimeout(function() {
                        let offset = $row.offset().top - 70;

                        $('html, body').stop().animate({
                            scrollTop: offset
                        });
                    }, 400);
                } else {
                    $modal.html('<div class="mortgage-modal_top"><div class="mortgage-modal_img"><img src="' + bankImage + '" alt=""></div><div class="mortgage-modal_license">' + bankLicense + '</div></div>' + bankContent + '<div><a href="#" class="mortgage-modal_btn" data-fancybox-close onclick="SCBopen(1);">Оформить заявку</a></div>');
                    $.fancybox.open($modal, {
                        scrolling: 'visible',
                        hideScrollbar: false,
                        backFocus: false,
                        toolbar  : false,


                        helpers: {
                            overlay: {
                                locked: false
                            }
                        }
                    });
                }
            });
        }
    });
  }

  initSortableCol() {
    let that = this;
    let $sorter = this.$sorter;
    let $sortItem = $sorter.find('[data-sort]');
    let currentClass = '';
    let sorterType = null;

    $sortItem.on('click', function (e) {
      $sortItem.not($(this)).removeClass('_asc _desc');
      sorterType = $(this).data('sort');

      if ($(this).hasClass('_asc')) {
        $(this).removeClass('_asc');
        $(this).addClass('_desc');
        currentClass = '_desc';
      }
      else if ($(this).hasClass('_desc')) {
        $(this).removeClass('_desc');
        currentClass = '';
        sorterType = null;
      }
      else {
        $(this).addClass('_asc');
        currentClass = '_asc';
      }

      that.renderBanks(sorterType, currentClass)


    })
  }

}