<%
let bookmarkIco = '<svg class="SvgIco SvgIco-bookmark" xmlns="http://www.w3.org/2000/svg" width="13" height="18" viewBox="0 0 13 18"><path class="SvgIco_path" d="M7.9 14.3c-.8-.5-2-.5-2.8 0l-5 3V2c0-1.1.9-2 2-2h9c1.1 0 2 .9 2 2v15.4l-5.2-3.1z"/></svg>';

let brushIco = '<svg class="SvgIco-brush" xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18"><path class="SvgIco_path" d="M11.1 4.4L6.9.2c-.3-.3-.7-.3-1.1 0l-.7.7c-.1.2-.1.4 0 .6l1.2 1.9c0 .1 0 .1-.1.1h-.1L4.3 2.3c-.2-.1-.4-.1-.6 0L1.9 4.1c-.1.2-.2.4-.1.6l.6 1.1c0 .1 0 .1-.1.1l-1.1-.6c-.2-.1-.3-.1-.5 0l-.5.5c-.4.4-.4 1 0 1.3l4.1 4.1.1-.1L11 4.6c0-.1 0-.1.1-.2zm5.5 8.8c-1.2-.6-2.4-1.4-3.5-2.3l-.1-.1c-.6-.7-.6-1.7 0-2.3l.9-1c.2-.2.2-.6 0-.8l-1.5-1.5c-.2-.2-.6-.2-.9 0L5 11.7c-.2.2-.2.6 0 .8l.2.2 1.9-1.9c.2-.2.5-.2.7 0 .2.2.2.5 0 .7L6 13.4l.6.6c.2.2.6.2.9 0l.9-1h.1c.7-.6 1.7-.5 2.4.2l.3.4c.8.9 1.4 2 1.9 3.1.4.9 1.3 1.4 2.3 1.4.9 0 1.8-.5 2.3-1.4.7-1.3.2-2.8-1.1-3.5zm-1 3.4c-.6 0-1-.5-1-1s.4-1 1-1 1 .5 1 1c0 .6-.5 1-1 1z"/></svg>';

let starIco = '<svg class="SvgIco SvgIco-star" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" ><path class="SvgIco_path" d="M528.1 171.5L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6zM388.6 312.3l23.7 138.4L288 385.4l-124.3 65.3 23.7-138.4-100.6-98 139-20.2 62.2-126 62.2 126 139 20.2-100.6 98z"/></svg>';

let brushIcon = '<div class="brushIcon"></div>';
%>

<% if(data.total !== 0) {%>


<div class="CardList CardList-flats" id="gridViewResult">
    <% _.forEach(data.flats, function(card) {
    let actionIconRed = '<div class="actionIcon actionIcon_red" title="' + card.action1Text + '"></div>';
    let actionIconOrange = '<div class="actionIcon actionIcon_orange"   title="' + card.action2Text + '"></div>';
    let actionIconBlue = '<div class="actionIcon actionIcon_blue"   title="' + card.action3Text + '"></div>';

    %>
    <% if(card.order){%>
    <% let promo = card; %>

    <div class="FlatCard FlatCard-promo">
        <div class="FlatCard__promo" style="background-image: url('<%= promo.img %>')">
            <a href="<%= promo.href %>" class="FlatCard__promoLink"></a>
        </div>
    </div>

    <% } else{ %>

    <div class="FlatCard<%= card.oldPrice ? ' FlatCard-light' : '' %>">
        <div class="FlatCard_picWrap">
            <% if(card.pic) {%>
            <a href="<%= card.href %>" class="FlatCard_pic" style="background-image: url(<%= card.pic %>)"></a>
            <%}else{%>
            <a href="<%= card.href %>" class="FlatCard_pic" style="background-image: url(/img/no-image.svg)"></a>
            <% }; %>
        </div>
        <% if(card.salePrice) {%>
            <div class="FlatCard_picSale" title="<%= card.saleInfo %>">?</div>
        <% }; %>
        <table class="FlatCard_details<%= card.salePrice ? ' _sale' : '' %>">
        <% if(card.salePrice) {%>
            <tr>
                <td class="FlatCard_param"></td>
                <td class="FlatCard_param"><%= card.area %> м<sup>2</sup></td>
            </tr>
            <tr>
                <% if(card.apts) {%>
                <td class="FlatCard_param"><%= card.rooms ? card.rooms + " к апартаменты "  : "апартаменты-студия " %>  №<%= card.num %></td>
                <td class="FlatCard_param<%= card.salePrice ? ' _cross' : '' %>"><strong><%= card.price.toLocaleString("ru-RU") %> Р</strong></td>
                <%}else{%>
                <td class="FlatCard_param"><%= card.rooms ? card.rooms + " к квартира "  : "квартира-студия " %>  №<%= card.num %></td>
                <td class="FlatCard_param<%= card.salePrice ? ' _cross' : '' %>"><strong><%= card.price.toLocaleString("ru-RU") %> Р</strong></td>
                <% }; %>
            </tr>
            <tr>
                <td class="FlatCard_param">Кор. <%= card.house %>, сек. <%= card.section %>, эт. <%= card.floor %></td>
                <td class="FlatCard_param _salePrice"><strong><%= card.salePrice.toLocaleString("ru-RU") %> Р</strong></td>
            </tr>
        <%}else{%>
            <tr>
                <% if(card.apts) {%>
                <td class="FlatCard_param"><%= card.rooms ? card.rooms + " к апартаменты "  : "апартаменты-студия " %>  №<%= card.num %></td>
                <%}else{%>
                <td class="FlatCard_param"><%= card.rooms ? card.rooms + " к квартира "  : "квартира-студия " %>  №<%= card.num %></td>
                <% }; %>
                <td class="FlatCard_param"><%= card.area %> м<sup>2</sup></td>
            </tr>
            <tr>
                <td class="FlatCard_param">Кор. <%= card.house %>, сек. <%= card.section %>, эт. <%= card.floor %></td>
                <td class="FlatCard_param<%= card.salePrice ? ' _cross' : '' %>"><strong><%= card.price.toLocaleString("ru-RU") %> Р</strong></td>
            </tr>
        <% }; %>
        </table>
        <div class="FlatCard_iconsWrap">
            <% if(card.action1) {%>
            <span class="FlatCard_ico FlatCard_ico-active"><%= actionIconRed%></span>
            <% }; %>

            <% if(card.action2) {%>
            <span class="FlatCard_ico FlatCard_ico-active"><%= actionIconOrange%></span>
            <% }; %>

            <% if(card.action3) {%>
            <span class="FlatCard_ico FlatCard_ico-active"><%= actionIconBlue%></span>
            <% }; %>


            <% if(card.oldPrice) {%>
            <span class="FlatCard_ico FlatCard_ico-active" title="Квартира по акции"><%= bookmarkIco%></span>
            <% }; %>

            <% if(card.finish) {%>
            <span href="#" class="FlatCard_ico FlatCard_ico-active" title="Квартира с отделкой"><%= brushIco %></span>
            <% }; %>
            <!-- <a href="#" class="FlatCard_ico FlatCard_ico-control"><%= starIco%></a> -->
        </div>
        <div class="FlatCard_iconsWrap FlatCard_iconsWrap-left">
            <div data-flat-type-id="<%= card.id %>" data-name="FavoriteIcon" class="FavoriteIcon toolTip FavoriteIcon-card"></div>
        </div>
    </div>
    <% } %>
    <% }); %>
</div>


<div class="Tbl Tbl-flats" id="listViewResult">

    <% _.forEach(data.flats, function(flt) {

    let actionIconRed = '<div class="actionIcon actionIcon_red" title="' + flt.action1Text + '"></div>';
    let actionIconOrange = '<div class="actionIcon actionIcon_orange"   title="' + flt.action2Text + '"></div>';
    let actionIconBlue = '<div class="actionIcon actionIcon_blue"   title="' + flt.action3Text + '"></div>';

    %>

    <% if(flt.order){%>

    <% } else{ %>

    <a href="<%= flt.href %>" class="Tbl_tr<%= flt.oldPrice ? ' Tbl_tr-light' : '' %>">
        <div class="Tbl_td"><%= flt.house %></div>
        <div class="Tbl_td hidden-xs"><%= flt.section %> секция</div>
        <div class="Tbl_td"><%= flt.floor %> <span class='hidden-xs hidden-sm'>этаж</span></div>
        <div class="Tbl_td"><%= flt.num %></div>
        <div class="Tbl_td">
            <% if(flt.rooms !== 0) {%>
            <%=flt.rooms%> к<span class='hidden-xs hidden-sm hidden-md'>омн.</span>
            <%}else{%>
            ст<span class='hidden-xs hidden-sm hidden-md'>удия</span>
            <% }; %>
        </div>
        <div class="Tbl_td"><%= flt.area %> м<sup>2</sup></div>
        <div class="Tbl_td">
            <% if(flt.salePrice) {%>
                <div class="Tbl_priceWrap">
                    <strong class="Tbl_price _cross"><%= flt.price.toLocaleString("ru-RU") %> Р</strong>
                    <strong class="Tbl_price _salePrice"><%= flt.salePrice.toLocaleString("ru-RU") %> Р</strong>
                    <div class="Tbl_picSale" title="<%= flt.saleInfo %>"></div>
                </div>
            <%}else{%>
                <%= flt.price.toLocaleString("ru-RU") %>
            <% }; %>
        </div>
        <div class="Tbl_td _font-0">
            <div class="Tbl_icoWrap Tbl_icoWrap-light">

            </div>
            <div class="Tbl_icoWrap Tbl_icoWrap-light" title="Квартира с отделкой">
                <%= flt.finish ? brushIcon : '&nbsp;' %>
            </div>
        </div>
        <div class="Tbl_td _font-0" data-name="FavoriteIcon">
            <div data-flat-type-id="<%= flt.id %>" data-name="FavoriteIcon" class="FavoriteIcon toolTip FavoriteIcon-table"></div>
        </div>
        <!--<div class="Tbl_td">-->
        <!--<div class="Tbl_icoWrap Tbl_icoWrap-light" title="Квартира с отделкой">-->
        <!--&lt;!&ndash;<%= flt.finish ? brushIcon : '&nbsp;' %>&ndash;&gt;-->
        <!--</div>-->
        <!--</div>-->
        <!-- <div class="Tbl_td">
            <div class="Tbl_icoWrap Tbl_icoWrap-light">
                <%= starIco %>
            </div>
        </div> -->
    </a>
    <% } %>
    <% }); %>

</div>






<%}else{%>
<div class="NoPlansFound" style="text-align:center;padding:40px;"><p class="NoPlansFound_text">Квартиры по заданным параметрам не найдены,<br class="hidden-xs"> пожалуйста измените параметры поиска.</p></div>
<% }; %>