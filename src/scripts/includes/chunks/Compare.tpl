<%
let flatsHave = typeof data.flats !== "undefined";
let plansHave = typeof data.plans !== "undefined";

let activeTabFlats = (flatsHave && !plansHave) || (flatsHave && plansHave) || (!flatsHave && !plansHave) ;
let activeTabPlans = !flatsHave && plansHave;
%>

<div class="Compare-param">
    <% if (flatsHave) {
    let flatsBaseSectionFields = data.flats.params.baseSection.names;
    let flatsFootageSectionFields = data.flats.params.footageSection.names;
    let flatsPromosSectionFields = data.flats.params.promosSection.names;

    let flatsBaseSectionFieldsCount = flatsBaseSectionFields.length;
    let flatsFootageSectionFieldsCount = flatsFootageSectionFields.length;
    let flatsPromosSectionFieldsCount = flatsPromosSectionFields.length;

    %>
    <div class="Compare-tabs-content Compare-tabs-content_active" data-id-compare="CompareFlats">
        <div class="Compare-param-list">
            <section class="Compare-param-list__section <%= flatsFootageSectionFieldsCount ? 'Compare-param-list__section_border': ' ' %>">
                <% _.forEach(flatsBaseSectionFields, function(field, index) { %>
                <div class="Compare-param-list__item hover-n-<%= index %>"><%= field %></div>
                <% }); %>
            </section>
            <section class="Compare-param-list__section <%= flatsPromosSectionFieldsCount ? 'Compare-param-list__section_border': ' ' %>">
                <% _.forEach(flatsFootageSectionFields, function(field, index) { %>
                <div class="<%= flatsFootageSectionFields[index] === 'Отделка' || flatsFootageSectionFields[index] === 'Общая площадь' ? 'Compare-param-list__item' : 'Compare-param-list__subitem' %> hover-n-<%= flatsBaseSectionFieldsCount+index %>">
                    <%= field %>
                </div>
                <% }); %>
            </section>
            <section class="Compare-param-list__section Compare-param-list__section_action">
                <% _.forEach(flatsPromosSectionFields, function(field, index) { %>
                <div class="<%= flatsPromosSectionFields[index] === 'Акции' ? 'Compare-param-list__item' : 'Compare-param-list__subitem' %> hover-n-<%= flatsBaseSectionFieldsCount+flatsFootageSectionFieldsCount+index %>">
                    <%= field %>
                </div>
                <% }); %>
            </section>
        </div>
    </div>
    <% } else {%>
    <div class="Compare-tabs-content Compare-tabs-content_active Compare-tabs-content_disable" data-id-compare="CompareFlats">
        <div class="Compare-param-list">
            <section class="Compare-param-list__section Compare-param-list__section_border">
                <div class="Compare-param-list__item">Стоимость</div>
                <div class="Compare-param-list__item">Сдача</div>
                <div class="Compare-param-list__item">Корпус</div>
                <div class="Compare-param-list__item">Секция</div>
                <div class="Compare-param-list__item">Этаж</div>
                <div class="Compare-param-list__item">Количество комнат</div>
                <div class="Compare-param-list__item">Планировка</div>
            </section>
            <section class="Compare-param-list__section Compare-param-list__section_border">
                <div class="Compare-param-list__item">Отделка</div>
                <div class="Compare-param-list__item">Общая площадь</div>
                <div class="Compare-param-list__subitem">Прихожая</div>
                <div class="Compare-param-list__subitem">Комната</div>
                <div class="Compare-param-list__subitem">Кухня-гостиная</div>
                <div class="Compare-param-list__subitem">Санузел</div>
                <div class="Compare-param-list__subitem">Кладовая</div>
                <div class="Compare-param-list__subitem">Балкон</div>
            </section>
            <section class="Compare-param-list__section Compare-param-list__section_action">
                <div class="Compare-param-list__item">Акции</div>
            </section>
        </div>
    </div>
    <% }; %>

    <% if (plansHave) {
    let plansBaseSectionFields = data.plans.params.baseSection.names;
    let plansFootageSectionFields = data.plans.params.footageSection.names;

    let plansBaseSectionFieldsCount = plansBaseSectionFields.length;
    let plansFootageSectionFieldsCount = plansFootageSectionFields.length;
    %>
    <div class="Compare-tabs-content" data-id-compare="ComparePlans">
        <div class="Compare-param-list">
            <section class="Compare-param-list__section <%= plansFootageSectionFieldsCount ? 'Compare-param-list__section_border': ' ' %>">
                <% _.forEach(plansBaseSectionFields, function(field, index) { %>
                <div class="Compare-param-list__item hover-n-<%= index %>"><%= field %></div>
                <% }); %>
            </section>

            <section class="Compare-param-list__section">
                <% _.forEach(plansFootageSectionFields, function(field, index) { %>
                <div class="<%= field === 'Отделка' || field === 'Общая площадь' ? 'Compare-param-list__item' : 'Compare-param-list__subitem' %> hover-n-<%= plansBaseSectionFieldsCount+index %>">
                    <% if ( field === 'Общая площадь'){ %>
                    <span class="hidden-xs">Общая площадь</span>
                    <span class="hidden-xxl hidden-xl hidden-lg hidden-md hidden-sm">Об. площадь</span>
                    <% } else { %>
                    <%= field %>
                    <% } %>

                </div>
                <% }); %>
            </section>

        </div>
    </div>
    <% } else { %>
    <div class="Compare-tabs-content Compare-tabs-content_disable" data-id-compare="ComparePlans">
        <div class="Compare-param-list">
            <section class="Compare-param-list__section Compare-param-list__section_border">
                <div class="Compare-param-list__item">Стоимость</div>
                <div class="Compare-param-list__item">Количество комнат</div>
            </section>

            <section class="Compare-param-list__section">
                <div class="Compare-param-list__item">Отделка</div>
                <div class="Compare-param-list__item">Общая площадь</div>
                <div class="Compare-param-list__subitem">Прихожая</div>
                <div class="Compare-param-list__subitem">Комната</div>
                <div class="Compare-param-list__subitem">Кухня-гостиная</div>
                <div class="Compare-param-list__subitem">Санузел</div>
                <div class="Compare-param-list__subitem">Кладовая</div>
                <div class="Compare-param-list__subitem">Балкон</div>
            </section>

        </div>
    </div>
    <% };%>
</div>
<div class="Compare-list">
    <div class="Compare-list__header Compare-list__header_desktop">
        <div class="Compare-list__pages">
            <button class="Compare-list__favorites" data-favorites-tab>Избранное</button>
            <button class="Compare-list__compare _active" data-compare-tab>Сравненеие</button>
        </div>
        <!--        выключаем пока нет планировок-->
        <!--        <div class="Compare-list__tabs">-->
        <!--            <a class="FavoriteTabs__link <%= activeTabFlats ? 'FavoriteTabs__link-active' : ' ' %>" href="" data-id="CompareFlats">Квартиры <span>0</span></a>-->
        <!--            <a class="FavoriteTabs__link <%= activeTabPlans ? 'FavoriteTabs__link-active' : ' ' %>" href="" data-id="ComparePlans">Планировки <span>0</span></a>-->
        <!--        </div>-->
        <!--        выключаем пока нет планировок-->
    </div>

    <div class="Compare-list__header Compare-list__header_mobile">
        <div class="Compare-mobile__title">
            <button class="Compare-list__favorites" data-favorites-tab>Избранное</button>
            <button class="Compare-list__compare _active" data-compare-tab>Сравненеие</button>
        </div>
        <!--        выключаем пока нет планировок-->
        <!--        <div class="Compare-list__tabs">-->
        <!--            <a class="FavoriteTabs__link FavoriteTabs__link-active" href="" data-id="CompareFlats">Квартиры <span>0</span></a>-->
        <!--            <a class="FavoriteTabs__link" href="" data-id="ComparePlans">Планировки <span>0</span></a>-->
        <!--        </div>-->
        <!--        выключаем пока нет планировок-->
    </div>

    <% if (flatsHave) {
    let flatsBaseSectionFields = data.flats.params.baseSection.names;
    let flatsFootageSectionFields = data.flats.params.footageSection.names;
    let flatsPromosSectionFields = data.flats.params.promosSection.names;

    let flatsCount = data.flats.id.length;

    let flatsBaseSectionValue = data.flats.params.baseSection.values;
    let flatsFootageSectionValue = data.flats.params.footageSection.values;
    let flatsPromosSectionValue = data.flats.params.promosSection.values;

    let flatsPics =  data.flats.pics;
    let flatsTitles = data.flats.titles;
    let flatsNumbers = data.flats.numbers;
    let flatsId = data.flats.id;
    let flatsLinks = data.flats.links;

    let flatsBaseSectionFieldsCount = flatsBaseSectionFields.length;
    let flatsFootageSectionFieldsCount = flatsFootageSectionFields.length;
    let flatsPromosSectionFieldsCount = flatsPromosSectionFields.length; %>
    <div class="Compare-tabs-content" style="display:block;" data-id-compare="CompareFlats">
        <div class="Compare-slider swiper-container">
            <div class="swiper-wrapper">
                <% for (let s=0; s < flatsCount; s++) { %>
                <div class="Compare-slider-slide swiper-slide">
                    <div class="Compare-slider-slide__row Compare-slider-slide__row-n-1">
                        <a href="<%= flatsLinks[s] %>" class="Compare-slider-slide__img" style="background-image: url('<%= flatsPics[s] %>');"></a>
                        <% for (let n=0; n < flatsBaseSectionValue[s].length; n++) { %>
                        <% if ( flatsBaseSectionFields[n] === 'Количество комнат'){ %>
                        <div class="Compare-slider-slide__title"><%= flatsBaseSectionValue[s][n] %>-комн. <%= flatsTitles[s] %> № <%= flatsNumbers[s] %></div>
                        <% } %>
                        <% } %>
                        <div class="FavoriteIcon FavoriteIcon-compare toolTip" data-remove-id="<%= flatsId[s] %>" data-flat-type-id="<%= flatsId[s] %>"></div>
                    </div>

                    <section class="Compare-slider-slide__section <%= flatsFootageSectionFieldsCount ? 'Compare-slider-slide__section_border': ' ' %>">
                        <% for (let n=0; n < flatsBaseSectionValue[s].length; n++) { %>
                        <% if ( flatsBaseSectionFields[n] === 'Стоимость'){ %>
                        <div class="Compare-slider-slide__row Compare-slider-slide__row_price hover-n-<%= n %>"><%= flatsBaseSectionValue[s][n].toLocaleString("ru-RU") %> Р</div>
                        <% } else { %>
                        <div class="Compare-slider-slide__row hover-n-<%= n %>"><%= flatsBaseSectionValue[s][n] %></div>
                        <% } %>
                        <% } %>
                    </section>

                    <section class="Compare-slider-slide__section <%= flatsPromosSectionFieldsCount ? 'Compare-slider-slide__section_border': ' ' %>" >
                        <% for (let l=0; l < flatsFootageSectionValue[s].length; l++) { %>
                        <% if ( flatsFootageSectionFields[l] === 'Отделка'){ %>
                        <div class="Compare-slider-slide__row hover-n-<%= flatsBaseSectionFieldsCount+l %>">
                            <% if ( flatsFootageSectionValue[s][l] ){ %>
                            Есть
                            <% } else { %>
                            —
                            <% } %>
                        </div>
                        <% } else { %>
                        <div class="Compare-slider-slide__row Compare-slider-slide__row_area hover-n-<%= flatsBaseSectionFieldsCount+l %>">
                            <% if (flatsFootageSectionValue[s][l]) {%>
                            <%= flatsFootageSectionValue[s][l] %> м<sup>2</sup>
                            <% } else { %>
                            —
                            <% }; %>
                        </div>
                        <% } %>
                        <% } %>
                    </section>

                    <section class="Compare-slider-slide__section Compare-slider-slide__section_action">
                        <% for (let m=0; m < flatsPromosSectionValue[s].length; m++) { %>
                        <% if ( flatsPromosSectionFields[m] === 'Акции'){ %>
                        <div class="Compare-slider-slide__row hover-n-<%= flatsBaseSectionFieldsCount+flatsFootageSectionFieldsCount+m %>">&nbsp;</div>
                        <% } else { %>
                        <div class="Compare-slider-slide__row hover-n-<%= flatsBaseSectionFieldsCount+flatsFootageSectionFieldsCount+m %>">
                            <% if ( flatsPromosSectionValue[s][m] ){ %>
                            <div class="actionIcon actionIcon_orange"></div>
                            <% } else { %>
                            —
                            <% } %>
                        </div>
                        <% } %>
                        <% } %>
                    </section>

                </div>
                <% } %>

            </div>
        </div>

        <div class="Compare-mobile">
            <div class="Compare-mobile-wrapper-fields">
                <div class="Compare-mobile-slider__col Compare-mobile-slider__col_visible">
                    <section class="Compare-param-list__section Compare-param-list__section_border">
                        <% _.forEach(flatsBaseSectionFields, function(field, index) { %>
                        <% if (field === "Количество комнат") { %>
                        <div class="Compare-param-list__item">
                            <span class="hidden-xs hidden-sm">Количество комнат</span>
                            <span class="hidden-md hidden-lg">Кол-во комнат</span>
                        </div>
                        <% } else { %>
                        <div class="Compare-param-list__item"><%= field %></div>
                        <% }; %>
                        <% }); %>
                    </section>

                    <section class="Compare-param-list__section Compare-param-list__section_footage">
                        <% _.forEach(flatsFootageSectionFields, function(field, index) { %>
                        <div class="<%= field === 'Отделка' || field === 'Общая площадь' ? 'Compare-param-list__item' : 'Compare-param-list__subitem' %>">
                            <%= field %>
                        </div>
                        <% }); %>
                    </section>
                </div>
                <div class="Compare-action-mobile">
                    <section class="Compare-param-list__section">
                        <% _.forEach(flatsPromosSectionFields, function(field, index) { %>
                        <div class="<%= field === 'Акции' ? 'Compare-param-list__item' : 'Compare-param-list__subitem' %>">
                            <%= field %>
                        </div>
                        <% }); %>
                    </section>
                </div>
            </div>
            <div class="Compare-mobile-slider Compare-mobile-slider_n1">
                <div class="swiper-wrapper">
                    <% for (let s=0; s < flatsCount; s++) { %>
                    <div class="Compare-mobile-slider__slide swiper-slide">
                        <div class="Compare-mobile-slider__pag Compare-mobile-slider__pag_n1"></div>
                        <div class="Compare-slider-slide__row Compare-slider-slide__row-n-1">
                            <a href="<%= flatsLinks[s] %>" class="Compare-slider-slide__img" style="background-image: url('https://etalonsad.ru//upload/information_system_2/3/7/6/item_376/information_items_property_49.svg');"></a>
                            <% for (let n=0; n < flatsBaseSectionValue[s].length; n++) { %>
                            <% if ( flatsBaseSectionFields[n] === 'Количество комнат'){ %>
                            <div class="Compare-slider-slide__title"><%= flatsBaseSectionValue[s][n] %>-комн. <%= flatsTitles[s] %> № <%= flatsNumbers[s] %></div>
                            <% } %>
                            <% } %>
                            <div class="FavoriteIcon FavoriteIcon-compare toolTip" data-flat-type-id="<%= flatsId[s] %>" data-remove-id="<%= flatsId[s] %>"></div>
                        </div>
                        <div class="Compare-mobile-slider__content Compare-mobile-slider__content_slider1">
                            <div class="Compare-mobile-slider__col Compare-mobile-slider__col_hidden">
                                <section class="Compare-param-list__section <%= flatsFootageSectionFieldsCount ? 'Compare-param-list__section_border': ' ' %>">
                                    <% for (let i=0; i < flatsBaseSectionFields.length; i++) { %>
                                    <div class="Compare-param-list__item"><%= flatsBaseSectionFields[i] %></div>
                                    <% } %>
                                </section>

                                <section class="Compare-param-list__section Compare-param-list__section_footage">
                                    <% for (let p=0; p < flatsFootageSectionFields.length; p++) { %>
                                    <div class="<%= flatsFootageSectionFields[p] === 'Отделка' || flatsFootageSectionFields[p] === 'Общая площадь' ? 'Compare-param-list__item' : 'Compare-param-list__subitem' %>">
                                        <%= flatsFootageSectionFields[p] %>
                                    </div>
                                    <% } %>
                                </section>
                            </div>
                            <div class="Compare-mobile-slider__col">
                                <section class="Compare-slider-slide__section <%= flatsFootageSectionFieldsCount ? 'Compare-param-list__section_border': ' ' %>">
                                    <% for (let n=0; n < flatsBaseSectionValue[s].length; n++) { %>
                                    <% if ( flatsBaseSectionFields[n] === 'Стоимость'){ %>
                                    <div class="Compare-slider-slide__row Compare-slider-slide__row_price"><%= flatsBaseSectionValue[s][n].toLocaleString("ru-RU") %> Р</div>
                                    <% } else { %>
                                    <div class="Compare-slider-slide__row"><%= flatsBaseSectionValue[s][n] %></div>
                                    <% } %>
                                    <% } %>
                                </section>

                                <section class="Compare-slider-slide__section">
                                    <% for (let l=0; l < flatsFootageSectionValue[s].length; l++) { %>
                                    <% if ( flatsFootageSectionFields[l] === 'Отделка'){ %>
                                    <div class="Compare-slider-slide__row">
                                        <% if ( flatsFootageSectionValue[s][l] ){ %>
                                        Есть
                                        <% } else { %>
                                        —
                                        <% } %>
                                    </div>
                                    <% } else { %>
                                    <div class="Compare-slider-slide__row Compare-slider-slide__row_area">
                                        <% if (flatsFootageSectionValue[s][l]) {%>
                                        <%= flatsFootageSectionValue[s][l] %> м<sup>2</sup>
                                        <% } else { %>
                                        —
                                        <% }; %>
                                    </div>
                                    <% } %>
                                    <% } %>
                                </section>
                            </div>
                        </div>
                        <div class="Compare-mobile-slider__content Compare-mobile-slider__content_action">
                            <div class="Compare-mobile-slider__col Compare-mobile-slider__col_action">
                                <section class="Compare-param-list__section Compare-param-list__section_action">
                                    <% for (let k=0; k < flatsPromosSectionFields.length; k++) { %>
                                    <div class="<%= flatsPromosSectionFields[k] === 'Акции' ? 'Compare-param-list__item' : 'Compare-param-list__subitem' %>">
                                        <%= flatsPromosSectionFields[k] %>
                                    </div>
                                    <% } %>
                                </section>
                            </div>
                            <div class="Compare-mobile-slider__col Compare-mobile-slider__col_action">
                                <section class="Compare-slider-slide__section Compare-slider-slide__section_action">
                                    <% for (let m=0; m < flatsPromosSectionValue[s].length; m++) { %>
                                    <% if ( flatsPromosSectionFields[m] === 'Акции'){ %>
                                    <div class="Compare-slider-slide__row">&nbsp;</div>
                                    <% } else { %>
                                    <div class="Compare-slider-slide__row">
                                        <% if ( flatsPromosSectionValue[s][m] ){ %>
                                        <div class="actionIcon actionIcon_orange"></div>
                                        <% } else { %>
                                        —
                                        <% } %>
                                    </div>
                                    <% } %>
                                    <% } %>
                                </section>
                            </div>
                        </div>
                    </div>
                    <% } %>
                </div>
            </div>
            <div class="Compare-mobile-slider Compare-mobile-slider_n2">
                <div class="swiper-wrapper">
                    <% for (let s=0; s < flatsCount; s++) { %>
                    <div class="Compare-mobile-slider__slide swiper-slide">
                        <div class="Compare-mobile-slider__pag Compare-mobile-slider__pag_n2"></div>
                        <div class="Compare-slider-slide__row Compare-slider-slide__row-n-1">
                            <a href="<%= flatsLinks[s] %>" class="Compare-slider-slide__img" style="background-image: url('https://etalonsad.ru//upload/information_system_2/3/7/6/item_376/information_items_property_49.svg');"></a>
                            <% for (let n=0; n < flatsBaseSectionValue[s].length; n++) { %>
                            <% if ( flatsBaseSectionFields[n] === 'Количество комнат'){ %>
                            <div class="Compare-slider-slide__title"><%= flatsBaseSectionValue[s][n] %>-к. <%= flatsTitles[s] %> № <%= flatsNumbers[s] %></div>
                            <% } %>
                            <% } %>
                            <div class="FavoriteIcon FavoriteIcon-compare toolTip" data-flat-type-id="<%= flatsId[s] %>" data-remove-id="<%= flatsId[s] %>"></div>
                        </div>
                        <div class="Compare-mobile-slider__content Compare-mobile-slider__content_slider2">
                            <div class="Compare-mobile-slider__col Compare-mobile-slider__col_hidden">
                                <section class="Compare-param-list__section <%= flatsFootageSectionFieldsCount ? 'Compare-param-list__section_border': ' ' %>">
                                    <% for (let i=0; i < flatsBaseSectionFields.length; i++) { %>
                                    <div class="Compare-param-list__item"><%= flatsBaseSectionFields[i] %></div>
                                    <% } %>
                                </section>

                                <section class="Compare-param-list__section Compare-param-list__section_footage">
                                    <% for (let p=0; p < flatsFootageSectionFields.length; p++) { %>
                                    <div class="<%= flatsFootageSectionFields[p] === 'Отделка' || flatsFootageSectionFields[p] === 'Общая площадь' ? 'Compare-param-list__item' : 'Compare-param-list__subitem' %>">
                                        <%= flatsFootageSectionFields[p] %>
                                    </div>
                                    <% } %>
                                </section>
                            </div>
                            <div class="Compare-mobile-slider__col">
                                <section class="Compare-slider-slide__section <%= flatsFootageSectionFieldsCount ? 'Compare-param-list__section_border': ' ' %>">
                                    <% for (let n=0; n < flatsBaseSectionValue[s].length; n++) { %>
                                    <% if ( flatsBaseSectionFields[n] === 'Стоимость'){ %>
                                    <div class="Compare-slider-slide__row Compare-slider-slide__row_price"><%= flatsBaseSectionValue[s][n].toLocaleString("ru-RU") %> Р</div>
                                    <% } else { %>
                                    <div class="Compare-slider-slide__row"><%= flatsBaseSectionValue[s][n] %></div>
                                    <% } %>
                                    <% } %>
                                </section>

                                <section class="Compare-slider-slide__section">
                                    <% for (let l=0; l < flatsFootageSectionValue[s].length; l++) { %>
                                    <% if ( flatsFootageSectionFields[l] === 'Отделка'){ %>
                                    <div class="Compare-slider-slide__row">
                                        <% if ( flatsFootageSectionValue[s][l] ){ %>
                                        Есть
                                        <% } else { %>
                                        —
                                        <% } %>
                                    </div>
                                    <% } else { %>
                                    <div class="Compare-slider-slide__row Compare-slider-slide__row_area">
                                        <% if (flatsFootageSectionValue[s][l]) {%>
                                        <%= flatsFootageSectionValue[s][l] %> м<sup>2</sup>
                                        <% } else { %>
                                        —
                                        <% }; %>
                                    </div>
                                    <% } %>
                                    <% } %>
                                </section>
                            </div>
                        </div>
                        <div class="Compare-mobile-slider__content Compare-mobile-slider__content_action">
                            <div class="Compare-mobile-slider__col Compare-mobile-slider__col_action">
                                <section class="Compare-param-list__section Compare-param-list__section_action">
                                    <% for (let k=0; k < flatsPromosSectionFields.length; k++) { %>
                                    <div class="<%= flatsPromosSectionFields[k] === 'Акции' ? 'Compare-param-list__item' : 'Compare-param-list__subitem' %>">
                                        <%= flatsPromosSectionFields[k] %>
                                    </div>
                                    <% } %>
                                </section>
                            </div>
                            <div class="Compare-mobile-slider__col Compare-mobile-slider__col_action">
                                <section class="Compare-slider-slide__section Compare-slider-slide__section_action">
                                    <% for (let m=0; m < flatsPromosSectionValue[s].length; m++) { %>
                                    <% if ( flatsPromosSectionFields[m] === 'Акции'){ %>
                                    <div class="Compare-slider-slide__row">&nbsp;</div>
                                    <% } else { %>
                                    <div class="Compare-slider-slide__row">
                                        <% if ( flatsPromosSectionValue[s][m] ){ %>
                                        <div class="actionIcon actionIcon_orange"></div>
                                        <% } else { %>
                                        —
                                        <% } %>
                                    </div>
                                    <% } %>
                                    <% } %>
                                </section>
                            </div>
                        </div>
                    </div>
                    <% } %>
                </div>
            </div>
        </div>
    </div>
    <% } else { %>
    <div class="Compare-tabs-content" data-id-compare="CompareFlats">
        <div class="Compare-list__empty">К сравнению не добавлено квартир</div>
    </div>
    <% }; %>

    <% if (plansHave) {
    let plansBaseSectionFields = data.plans.params.baseSection.names;
    let plansFootageSectionFields = data.plans.params.footageSection.names;

    let plansCount = data.plans.id.length;

    let plansBaseSectionValue = data.plans.params.baseSection.values;
    let plansFootageSectionValue = data.plans.params.footageSection.values;

    let plansPics =  data.plans.pics;
    let plansTitles = data.plans.titles;
    let plansId = data.plans.id;
    let plansLinks = data.plans.links;

    let plansBaseSectionFieldsCount = plansBaseSectionFields.length;
    let plansFootageSectionFieldsCount = plansFootageSectionFields.length; %>
    <div class="Compare-tabs-content" data-id-compare="ComparePlans">
        <div class="Compare-slider swiper-container">
            <div class="swiper-wrapper">
                <% for (let s=0; s < plansCount; s++) { %>
                <div class="Compare-slider-slide swiper-slide">
                    <div class="Compare-slider-slide__row Compare-slider-slide__row-n-1">
                        <a href="<%= plansLinks[s] %>" class="Compare-slider-slide__img" style="background-image: url('<%= plansPics[s] %>');"></a>
                        <div class="Compare-slider-slide__title"><%= plansTitles[s] %></div>
                        <div class="FavoriteIcon FavoriteIcon-compare toolTip" data-remove-id="<%= plansId[s] %>" data-plan-id="<%= plansId[s] %>"></div>
                    </div>

                    <section class="Compare-slider-slide__section <%= plansFootageSectionFieldsCount ? 'Compare-slider-slide__section_border': ' ' %>">
                        <% for (let n=0; n < plansBaseSectionValue[s].length; n++) { %>
                        <% if ( plansBaseSectionFields[n] === 'Стоимость'){ %>
                        <div class="Compare-slider-slide__row Compare-slider-slide__row_price hover-n-<%= n %>">от <%= plansBaseSectionValue[s][n].toLocaleString("ru-RU") %> Р</div>
                        <% } else { %>
                        <div class="Compare-slider-slide__row hover-n-<%= n %>"><%= plansBaseSectionValue[s][n] %></div>
                        <% }; %>
                        <% } %>
                    </section>

                    <section class="Compare-slider-slide__section">
                        <% for (let l=0; l < plansFootageSectionValue[s].length; l++) { %>
                        <% if ( plansFootageSectionFields[l] === 'Отделка'){ %>
                        <div class="Compare-slider-slide__row hover-n-<%= plansBaseSectionFieldsCount+l %>">
                            <% if ( plansFootageSectionValue[s][l] ){ %>
                            Есть
                            <% } else { %>
                            —
                            <% } %>
                        </div>
                        <% } else if ( plansFootageSectionFields[l] === 'Общая площадь') { %>
                        <div class="Compare-slider-slide__row Compare-slider-slide__row_area hover-n-<%= plansBaseSectionFieldsCount+l %>"><%= plansFootageSectionValue[s][l][0] %> — <%= plansFootageSectionValue[s][l][1] %> м<sup>2</sup></div>
                        <% } else { %>
                        <div class="Compare-slider-slide__row Compare-slider-slide__row_area hover-n-<%= plansBaseSectionFieldsCount+l %>">
                            <% if ( plansFootageSectionValue[s][l] ) { %>
                            <%= plansFootageSectionValue[s][l] %> м<sup>2</sup>
                            <% } else {%>
                            —
                            <% }; %>
                        </div>
                        <% } %>
                        <% } %>
                    </section>
                </div>
                <% };%>
            </div>
        </div>

        <div class="Compare-mobile">
            <div class="Compare-mobile-wrapper-fields">
                <div class="Compare-mobile-slider__col Compare-mobile-slider__col_visible">
                    <section class="Compare-param-list__section Compare-param-list__section_border">
                        <% _.forEach(plansBaseSectionFields, function(field, index) { %>
                        <% if (field === "Количество комнат") { %>
                        <div class="Compare-param-list__item">
                            <span class="hidden-xs hidden-sm">Количество комнат</span>
                            <span class="hidden-md hidden-lg">Кол-во комнат</span>
                        </div>
                        <% } else { %>
                        <div class="Compare-param-list__item"><%= field %></div>
                        <% }; %>
                        <% }); %>
                    </section>

                    <section class="Compare-param-list__section">
                        <% _.forEach(plansFootageSectionFields, function(field, index) { %>
                        <div class="<%= field === 'Отделка' || field === 'Общая площадь' ? 'Compare-param-list__item' : 'Compare-param-list__subitem' %>">
                            <% if (field === "Общая площадь") { %>
                            <span class="hidden-xs">Общая площадь</span>
                            <span class="hidden-md hidden-lg hidden-xl hidden-xxl">Об. площадь</span>
                            <% } else { %>
                            <%= field %>
                            <% }; %>
                        </div>
                        <% }); %>
                    </section>
                </div>
            </div>
            <div class="Compare-mobile-slider Compare-mobile-slider_n1">
                <div class="Compare-mobile-slider__pag Compare-mobile-slider__pag_n1"></div>
                <div class="swiper-wrapper">
                    <% for (let s=0; s < plansCount; s++) { %>
                    <div class="Compare-mobile-slider__slide swiper-slide">
                        <div class="Compare-slider-slide__row Compare-slider-slide__row-n-1">
                            <a href="<%= plansLinks[s] %>" class="Compare-slider-slide__img" style="background-image: url('<%= plansPics[s] %>');"></a>
                            <div class="Compare-slider-slide__title"><%= plansTitles[s] %></div>
                            <div class="FavoriteIcon FavoriteIcon-compare toolTip" data-remove-id="<%= plansId[s] %>" data-plan-id="<%= plansId[s] %>"></div>
                        </div>
                        <div class="Compare-mobile-slider__content Compare-mobile-slider__content_slider1">
                            <div class="Compare-mobile-slider__col Compare-mobile-slider__col_hidden">
                                <section class="Compare-param-list__section <%= plansFootageSectionFieldsCount ? 'Compare-param-list__section_border': ' ' %>">
                                    <% for (let i=0; i < plansBaseSectionFields.length; i++) { %>
                                    <div class="Compare-param-list__item"><%= plansBaseSectionFields[i] %></div>
                                    <% } %>
                                </section>

                                <section class="Compare-param-list__section">
                                    <% for (let p=0; p < plansFootageSectionFields.length; p++) { %>
                                    <div class="<%= plansFootageSectionFields[p] === 'Отделка' || plansFootageSectionFields[p] === 'Общая площадь' ? 'Compare-param-list__item' : 'Compare-param-list__subitem' %>">
                                        <%= plansFootageSectionFields[p] %>
                                    </div>
                                    <% } %>
                                </section>
                            </div>
                            <div class="Compare-mobile-slider__col">
                                <section class="Compare-slider-slide__section <%= plansFootageSectionFieldsCount ? 'Compare-param-list__section_border': ' ' %>">
                                    <% for (let n=0; n < plansBaseSectionValue[s].length; n++) { %>
                                    <% if ( plansBaseSectionFields[n] === 'Стоимость'){ %>
                                    <div class="Compare-slider-slide__row Compare-slider-slide__row_price">от <%= (plansBaseSectionValue[s][n] / 1000000).toFixed(1) %> млн Р</div>
                                    <% } else { %>
                                    <div class="Compare-slider-slide__row"><%= plansBaseSectionValue[s][n] %></div>
                                    <% } %>
                                    <% } %>
                                </section>

                                <section class="Compare-slider-slide__section">
                                    <% for (let l=0; l < plansFootageSectionValue[s].length; l++) { %>
                                    <% if ( plansFootageSectionFields[l] === 'Отделка'){ %>
                                    <div class="Compare-slider-slide__row">
                                        <% if ( plansFootageSectionValue[s][l] ){ %>
                                        Есть
                                        <% } else { %>
                                        —
                                        <% } %>
                                    </div>
                                    <% } else if ( plansFootageSectionFields[l] === 'Общая площадь') { %>
                                    <div class="Compare-slider-slide__row Compare-slider-slide__row_area hover-n-<%= plansBaseSectionFieldsCount+l %>"><%= plansFootageSectionValue[s][l][0] %>-<%= plansFootageSectionValue[s][l][1] %> м<sup>2</sup></div>
                                    <% } else { %>
                                    <div class="Compare-slider-slide__row Compare-slider-slide__row_area">
                                        <% if ( plansFootageSectionValue[s][l] ) {%>
                                        <%= plansFootageSectionValue[s][l] %> м<sup>2</sup>
                                        <% } else { %>
                                        —
                                        <% }; %>
                                    </div>
                                    <% } %>
                                    <% } %>
                                </section>
                            </div>
                        </div>
                    </div>
                    <% }; %>
                </div>
            </div>
            <div class="Compare-mobile-slider Compare-mobile-slider_n2">
                <div class="Compare-mobile-slider__pag Compare-mobile-slider__pag_n2"></div>
                <div class="swiper-wrapper">
                    <% for (let s=0; s < plansCount; s++) { %>
                    <div class="Compare-mobile-slider__slide swiper-slide">
                        <div class="Compare-slider-slide__row Compare-slider-slide__row-n-1">
                            <a href="<%= plansLinks[s] %>" class="Compare-slider-slide__img" style="background-image: url('<%= plansPics[s] %>');"></a>
                            <div class="Compare-slider-slide__title"><%= plansTitles[s] %></div>
                            <div class="FavoriteIcon FavoriteIcon-compare toolTip" data-remove-id="<%= plansId[s] %>" data-plan-id="<%= plansId[s] %>"></div>
                        </div>
                        <div class="Compare-mobile-slider__content Compare-mobile-slider__content_slider2">
                            <div class="Compare-mobile-slider__col Compare-mobile-slider__col_hidden">
                                <section class="Compare-param-list__section <%= plansFootageSectionFieldsCount ? 'Compare-param-list__section_border': ' ' %>">
                                    <% for (let i=0; i < plansBaseSectionFields.length; i++) { %>
                                    <div class="Compare-param-list__item"><%= plansBaseSectionFields[i] %></div>
                                    <% } %>
                                </section>

                                <section class="Compare-param-list__section">
                                    <% for (let p=0; p < plansFootageSectionFields.length; p++) { %>
                                    <div class="<%= plansFootageSectionFields[p] === 'Отделка' || plansFootageSectionFields[p] === 'Общая площадь' ? 'Compare-param-list__item' : 'Compare-param-list__subitem' %>">
                                        <%= plansFootageSectionFields[p] %>
                                    </div>
                                    <% } %>
                                </section>
                            </div>
                            <div class="Compare-mobile-slider__col">
                                <section class="Compare-slider-slide__section Compare-slider-slide__section_border">
                                    <% for (let n=0; n < plansBaseSectionValue[s].length; n++) { %>
                                    <% if ( plansBaseSectionFields[n] === 'Стоимость'){ %>
                                    <div class="Compare-slider-slide__row Compare-slider-slide__row_price">от <%= (plansBaseSectionValue[s][n] / 1000000).toFixed(1) %> млн Р</div>
                                    <% } else { %>
                                    <div class="Compare-slider-slide__row"><%= plansBaseSectionValue[s][n] %></div>
                                    <% } %>
                                    <% } %>
                                </section>

                                <section class="Compare-slider-slide__section">
                                    <% for (let l=0; l < plansFootageSectionValue[s].length; l++) { %>
                                    <% if ( plansFootageSectionFields[l] === 'Отделка'){ %>
                                    <div class="Compare-slider-slide__row">
                                        <% if ( plansFootageSectionValue[s][l] ){ %>
                                        Есть
                                        <% } else { %>
                                        —
                                        <% } %>
                                    </div>
                                    <% } else if ( plansFootageSectionFields[l] === 'Общая площадь') { %>
                                    <div class="Compare-slider-slide__row Compare-slider-slide__row_area hover-n-<%= plansBaseSectionFieldsCount+l %>"><%= plansFootageSectionValue[s][l][0] %>-<%= plansFootageSectionValue[s][l][1] %> м<sup>2</sup></div>
                                    <% } else { %>
                                    <div class="Compare-slider-slide__row Compare-slider-slide__row_area">
                                        <% if ( plansFootageSectionValue[s][l] ) {%>
                                        <%= plansFootageSectionValue[s][l] %> м<sup>2</sup>
                                        <% } else { %>
                                        —
                                        <% }; %>
                                    </div>
                                    <% } %>
                                    <% } %>
                                </section>
                            </div>
                        </div>
                    </div>
                    <% } %>
                </div>
            </div>
        </div>
    </div>
    <% } else { %>
    <div class="Compare-tabs-content" data-id-compare="ComparePlans">
        <div class="Compare-list__empty">К сравнению не добавлено планировок</div>
    </div>
    <% }; %>

</div>