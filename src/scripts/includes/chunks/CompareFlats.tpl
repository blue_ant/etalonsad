
<%
    let fields = data.fields;
    let params = data.params;
    let i, j, k;
%>

<div class="Compare">
    <div class="Compare-param">
        <div class="Compare-param__header">
            <div class="Compare__title">Сравнение</div>
            <div class="SubCheckboxes__item">
                <input type="checkbox" id="differences" class="SubCheckboxes__input" name=""/>
                <label for="differences">Показывать только отличия</label>
            </div>
        </div>

        <div class="Compare-tabs-content Compare-tabs-content_active" data-id-compare="CompareFlats">
            <div class="Compare-param-list">
                <section class="Compare-param-list__section">
                    <% for (i = 0; i < 7; i++) { %>
                        <div class="Compare-param-list__item Compare-param-list__item-n-<%= i+2 %> hover-n-<%= i+2 %>"><%= fields[i] %></div>
                    <% }; %>
                </section>

                <section class="Compare-param-list__section">
                    <% for (i = 7; i < 15; i++) { %>
                        <% if (i === 7 || i === 8) {%>
                            <div class="Compare-param-list__item Compare-param-list__item-n-<%= i+2 %> hover-n-<%= i+2 %>"><%= fields[i] %></div>
                        <% } else{ %>
                            <div class="Compare-param-list__subitem Compare-param-list__subitem-n-<%= i+2 %> hover-n-<%= i+2 %>"><%= fields[i] %></div>
                        <% } %>
                    <% }; %>
                </section>
                <section class="Compare-param-list__section">
                    <% for (i = 15; i < fields.length; i++) { %>
                        <% if (i === 15) {%>
                            <div class="Compare-param-list__item Compare-param-list__item-n-<%= i+2 %> hover-n-<%= i+2 %>"><%= fields[i] %></div>
                        <% } else{ %>
                            <div class="Compare-param-list__subitem-2 Compare-param-list__subitem-2-n-<%= i+2 %> hover-n-<%= i+2 %>"><%= fields[i] %></div>
                        <% } %>
                    <% }; %>
                </section>

            </div>
        </div>
        <div class="Compare-tabs-content" data-id-compare="ComparePlans">
            <div class="Compare-param-list">
                <section class="Compare-param-list__section">
                    <div class="Compare-param-list__item Compare-param-list__item-n-2 hover-n-2">Стоимость</div>
                    <div class="Compare-param-list__item Compare-param-list__item-n-3 hover-n-3">Балкон</div>
                    <div class="Compare-param-list__item Compare-param-list__item-n-4 hover-n-4">Количество комнат</div>
                </section>

                <section class="Compare-param-list__section">
                    <div class="Compare-param-list__item Compare-param-list__item-n-9  hover-n-9">Отделка</div>

                    <div class="Compare-param-list__item Compare-param-list__item-n-10 hover-n-10">Общая площадь</div>
                    <div class="Compare-param-list__subitem Compare-param-list__subitem-n-11 hover-n-11">Прихожая</div>
                    <div class="Compare-param-list__subitem Compare-param-list__subitem-n-12 hover-n-12">Комната</div>
                    <div class="Compare-param-list__subitem Compare-param-list__subitem-n-13 hover-n-13">Кухня-гостиная</div>
                    <div class="Compare-param-list__subitem Compare-param-list__subitem-n-14 hover-n-14">Санузел</div>
                    <div class="Compare-param-list__subitem Compare-param-list__subitem-n-15 hover-n-15">Кладовая</div>
                    <div class="Compare-param-list__subitem Compare-param-list__subitem-n-16 hover-n-16">Балкон</div>
                </section>

            </div>
        </div>
    </div>

    <div class="Compare-list">
        <div class="Compare-list__header Compare-list__header_desktop">
            <div class="Compare-list__tabs">
                <a class="FavoriteTabs__link FavoriteTabs__link-active" href="" data-id="CompareFlats">Квартиры <span>0</span></a>
                <a class="FavoriteTabs__link" href="" data-id="ComparePlans">Планировки <span>0</span></a>
            </div>
            <div class="Compare-list__sliderNav">
                <a href="" class="Compare-list__sliderNav-prev"></a>
                <a href="" class="Compare-list__sliderNav-next"></a>
            </div>
        </div>

        <div class="Compare-list__header Compare-list__header_mobile">
            <div class="Compare-mobile__title">Сравнение</div>
            <div class="Compare-list__tabs">
                <a class="FavoriteTabs__link FavoriteTabs__link-active" href="" data-id="CompareFlats">Квартиры <span>0</span></a>
                <a class="FavoriteTabs__link" href="" data-id="ComparePlans">Планировки <span>0</span></a>
            </div>
        </div>

        <div class="Compare-tabs-content Compare-tabs-content_active" data-id-compare="CompareFlats">
            <div class="Compare-slider swiper-container">
                <div class="swiper-wrapper">
                    <% for (j = 0; j < params.length; j++) { %>
                        <div class="Compare-slider-slide swiper-slide">

                            <section class="Compare-slider-slide__row Compare-slider-slide__row-n-1">
                                <% for (k = 0; k < 2; k++) { %>
                                    <% if (k === 0) {%>
                                        <div class="Compare-slider-slide__img" style="background-image: url('<%= params[j][k] %>');"></div>
                                    <% } else{ %>
                                        <div class="Compare-slider-slide__title"><%= params[j][k] %></div>
                                    <% } %>
                                <% }; %>
                                <div class="FavoriteIcon FavoriteIcon-compare"></div>
                                <div class="Compare-slider-slide__remove"></div>
                            </section>


                            <section class="Compare-slider-slide__section">
                            <% for (k = 2; k < 9; k++) { %>
                                <div class="Compare-slider-slide__row Compare-slider-slide__row-n-<%= k %> hover-n-<%= k %>"><%= params[j][k] %></div>
                            <% }; %>
                            </section>

                            <section class="Compare-slider-slide__section">
                                <% for (k = 9; k < 17; k++) { %>
                                        <div class="Compare-slider-slide__row Compare-slider-slide__row-n-<%= k %> hover-n-<%= k %>"><%= params[j][k] %></div>
                                <% }; %>
                            </section>


                            <section class="Compare-slider-slide__section">
                                <% for (k = 17; k < params[j].length; k++) { console.log(k, 'k') %>
                                <div class="Compare-slider-slide__row Compare-slider-slide__row-n-<%= k %> hover-n-<%= k %>"><%= params[j][k] %></div>
                                <% }; %>
                            </section>

                        </div>
                    <% }; %>

                </div>
            </div>

            <div class="Compare-mobile">
                <div class="Compare-mobile__checkbox">
                    <div class="SubCheckboxes__item">
                        <input type="checkbox" id="differences-mobile" class="SubCheckboxes__input" name=""/>
                        <label for="differences-mobile">Показывать только отличия</label>
                    </div>
                </div>
                <div class="Compare-mobile-wrapper-fields">
                    <div class="Compare-mobile-slider__col Compare-mobile-slider__col_visible">
                        <section class="Compare-param-list__section">
                            <div class="Compare-param-list__item Compare-param-list__item-n-2 hover-n-2">Стоимость</div>
                            <div class="Compare-param-list__item Compare-param-list__item-n-3 hover-n-3">Сдача</div>
                            <div class="Compare-param-list__item Compare-param-list__item-n-4 hover-n-4">Корпус</div>
                            <div class="Compare-param-list__item Compare-param-list__item-n-5 hover-n-5">Секция</div>
                            <div class="Compare-param-list__item Compare-param-list__item-n-6 hover-n-6">Этаж</div>
                            <div class="Compare-param-list__item Compare-param-list__item-n-7 hover-n-7"><span class="hidden-xs hidden-sm">Количество комнат </span><span class="hidden-xxl hidden-xl hidden-lg hidden-md">Кол-во комнат</span></div>
                            <div class="Compare-param-list__item Compare-param-list__item-n-8 hover-n-8">Планировка</div>
                        </section>

                        <section class="Compare-param-list__section">
                            <div class="Compare-param-list__item Compare-param-list__item-n-9  hover-n-9">Отделка</div>

                            <div class="Compare-param-list__item Compare-param-list__item-n-10 hover-n-10">Общая площадь</div>
                            <div class="Compare-param-list__subitem Compare-param-list__subitem-n-11 hover-n-11">Прихожая</div>
                            <div class="Compare-param-list__subitem Compare-param-list__subitem-n-12 hover-n-12">Комната</div>
                            <div class="Compare-param-list__subitem Compare-param-list__subitem-n-13 hover-n-13">Кухня-гостиная</div>
                            <div class="Compare-param-list__subitem Compare-param-list__subitem-n-14 hover-n-14">Санузел</div>
                            <div class="Compare-param-list__subitem Compare-param-list__subitem-n-15 hover-n-15">Кладовая</div>
                            <div class="Compare-param-list__subitem Compare-param-list__subitem-n-16 hover-n-16">Балкон</div>
                        </section>
                    </div>
                    <div class="Compare-action-mobile">
                        <section class="Compare-param-list__section">
                            <div class="Compare-param-list__item Compare-param-list__item-n-17 hover-n-17">Акции</div>
                            <div class="Compare-param-list__item Compare-param-list__item-n-18 hover-n-18">Скидка 2% при заключении договора в июне.</div>
                            <div class="Compare-param-list__item Compare-param-list__item-n-19 hover-n-19">Бесплатная региестрация договора.</div>
                            <div class="Compare-param-list__item Compare-param-list__item-n-20 hover-n-20">Рассрочка до 2023 года.</div>
                            <div class="Compare-param-list__item Compare-param-list__item-n-21 hover-n-21">Рассрочка до 2023 года.</div>
                            <div class="Compare-param-list__item Compare-param-list__item-n-22 hover-n-22">Рассрочка до 2023 года.</div>
                        </section>
                    </div>
                </div>
                <div class="Compare-mobile-slider Compare-mobile-slider_n1">
                    <div class="Compare-mobile-slider__pag Compare-mobile-slider__pag_n1"></div>
                    <div class="swiper-wrapper">
                        <div class="Compare-mobile-slider__slide swiper-slide">
                            <div class="Compare-slider-slide__row Compare-slider-slide__row-n-1">
                                <div class="Compare-slider-slide__img" style="background-image: url('https://etalonsad.ru//upload/information_system_2/3/7/6/item_376/information_items_property_49.svg');"></div>
                                <div class="Compare-slider-slide__title">1 к квартира №13</div>
                                <div class="FavoriteIcon FavoriteIcon-compare"></div>
                                <div class="Compare-slider-slide__remove Compare-slider-slide__remove_mobile"></div>
                            </div>
                            <div class="Compare-mobile-slider__content Compare-mobile-slider__content_slider1">
                                <div class="Compare-mobile-slider__col Compare-mobile-slider__col_hidden">
                                    <section class="Compare-param-list__section">
                                        <div class="Compare-param-list__item Compare-param-list__item-n-2 hover-n-2">Стоимость</div>
                                        <div class="Compare-param-list__item Compare-param-list__item-n-3 hover-n-3">Сдача</div>
                                        <div class="Compare-param-list__item Compare-param-list__item-n-4 hover-n-4">Корпус</div>
                                        <div class="Compare-param-list__item Compare-param-list__item-n-5 hover-n-5">Секция</div>
                                        <div class="Compare-param-list__item Compare-param-list__item-n-6 hover-n-6">Этаж</div>
                                        <div class="Compare-param-list__item Compare-param-list__item-n-7 hover-n-7">Количество комнат</div>
                                        <div class="Compare-param-list__item Compare-param-list__item-n-8 hover-n-8">Планировка</div>
                                    </section>

                                    <section class="Compare-param-list__section">
                                        <div class="Compare-param-list__item Compare-param-list__item-n-9  hover-n-9">Отделка</div>

                                        <div class="Compare-param-list__item Compare-param-list__item-n-10 hover-n-10">Общая площадь</div>
                                        <div class="Compare-param-list__subitem Compare-param-list__subitem-n-11 hover-n-11">Прихожая</div>
                                        <div class="Compare-param-list__subitem Compare-param-list__subitem-n-12 hover-n-12">Комната</div>
                                        <div class="Compare-param-list__subitem Compare-param-list__subitem-n-13 hover-n-13">Кухня-гостиная</div>
                                        <div class="Compare-param-list__subitem Compare-param-list__subitem-n-14 hover-n-14">Санузел</div>
                                        <div class="Compare-param-list__subitem Compare-param-list__subitem-n-15 hover-n-15">Кладовая</div>
                                        <div class="Compare-param-list__subitem Compare-param-list__subitem-n-16 hover-n-16">Балкон</div>
                                    </section>
                                </div>
                                <div class="Compare-mobile-slider__col">
                                    <section class="Compare-slider-slide__section">
                                        <div class="Compare-slider-slide__row Compare-slider-slide__row-n-2 hover-n-2">3 198 789 Р</div>
                                        <div class="Compare-slider-slide__row Compare-slider-slide__row-n-3 hover-n-3">1 кв 2019</div>
                                        <div class="Compare-slider-slide__row Compare-slider-slide__row-n-4 hover-n-4">2а</div>
                                        <div class="Compare-slider-slide__row Compare-slider-slide__row-n-5 hover-n-5">3</div>
                                        <div class="Compare-slider-slide__row Compare-slider-slide__row-n-6 hover-n-6">6</div>
                                        <div class="Compare-slider-slide__row Compare-slider-slide__row-n-7 hover-n-7">1</div>
                                        <div class="Compare-slider-slide__row Compare-slider-slide__row-n-8 hover-n-8">Типовая 4</div>
                                    </section>

                                    <section class="Compare-slider-slide__section">
                                        <div class="Compare-slider-slide__row Compare-slider-slide__row-n-9 hover-n-9">Есть</div>
                                        <div class="Compare-slider-slide__row Compare-slider-slide__row-n-10 hover-n-10">43.4 м2</div>
                                        <div class="Compare-slider-slide__row Compare-slider-slide__row-n-11 hover-n-11">4.9 м2</div>
                                        <div class="Compare-slider-slide__row Compare-slider-slide__row-n-12 hover-n-12">16.6 м2</div>
                                        <div class="Compare-slider-slide__row Compare-slider-slide__row-n-13 hover-n-13">12.4 м2</div>
                                        <div class="Compare-slider-slide__row Compare-slider-slide__row-n-14 hover-n-14">16.6 м2</div>
                                        <div class="Compare-slider-slide__row Compare-slider-slide__row-n-15 hover-n-15">3.4 м2</div>
                                        <div class="Compare-slider-slide__row Compare-slider-slide__row-n-16 hover-n-16">3.2 м2</div>
                                    </section>
                                </div>
                            </div>
                            <div class="Compare-mobile-slider__content Compare-mobile-slider__content_action">
                                <div class="Compare-mobile-slider__col Compare-mobile-slider__col_action">
                                    <section class="Compare-param-list__section Compare-param-list__section_action">
                                        <div class="Compare-param-list__item  hover-n-17">Акции</div>

                                        <div class="Compare-param-list__subitem-2 Compare-param-list__subitem-2-n-18 hover-n-18">Скидка 2% при заключении договора в июне.</div>
                                        <div class="Compare-param-list__subitem-2 Compare-param-list__subitem-2-n-19 hover-n-19">Скидка 2% при заключении договора в июне.</div>
                                        <div class="Compare-param-list__subitem-2 Compare-param-list__subitem-2-n-20 hover-n-20">Рассрочка до 2023 года.</div>
                                        <div class="Compare-param-list__subitem-2 Compare-param-list__subitem-2-n-21 hover-n-21">Рассрочка до 2023 года.</div>
                                        <div class="Compare-param-list__subitem-2 Compare-param-list__subitem-2-n-22 hover-n-22">Рассрочка до 2023 года.</div>
                                    </section>
                                </div>
                                <div class="Compare-mobile-slider__col Compare-mobile-slider__col_action">
                                    <section class="Compare-slider-slide__section">
                                        <div class="Compare-slider-slide__row Compare-slider-slide__row-n-17 hover-n-17">&nbsp;</div>
                                        <div class="Compare-slider-slide__row Compare-slider-slide__row-n-18 hover-n-18"><div class="actionIcon actionIcon_red" title="1"></div></div>
                                        <div class="Compare-slider-slide__row Compare-slider-slide__row-n-19 hover-n-19"><div class="actionIcon actionIcon_blue" title="1"></div></div>
                                        <div class="Compare-slider-slide__row Compare-slider-slide__row-n-20 hover-n-20"><div class="actionIcon actionIcon_orange" title="1"></div></div>
                                        <div class="Compare-slider-slide__row Compare-slider-slide__row-n-21 hover-n-21"><div class="actionIcon actionIcon_orange" title="1"></div></div>
                                        <div class="Compare-slider-slide__row Compare-slider-slide__row-n-22 hover-n-22"><div class="actionIcon actionIcon_orange" title="1"></div></div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="Compare-mobile-slider Compare-mobile-slider_n2">
                    <div class="Compare-mobile-slider__pag Compare-mobile-slider__pag_n2"></div>
                    <div class="swiper-wrapper">
                        <div class="Compare-mobile-slider__slide swiper-slide">
                            <div class="Compare-slider-slide__row Compare-slider-slide__row-n-1">
                                <div class="Compare-slider-slide__img" style="background-image: url('https://etalonsad.ru//upload/information_system_2/3/7/6/item_376/information_items_property_49.svg');"></div>
                                <div class="Compare-slider-slide__title">1 к квартира №13</div>
                                <div class="FavoriteIcon FavoriteIcon-compare"></div>
                                <div class="Compare-slider-slide__remove Compare-slider-slide__remove_mobile"></div>
                            </div>
                            <div class="Compare-mobile-slider__content Compare-mobile-slider__content_slider2">
                                <div class="Compare-mobile-slider__col Compare-mobile-slider__col_hidden">
                                    <section class="Compare-param-list__section">
                                        <div class="Compare-param-list__item Compare-param-list__item-n-2 hover-n-2">Стоимость</div>
                                        <div class="Compare-param-list__item Compare-param-list__item-n-3 hover-n-3">Сдача</div>
                                        <div class="Compare-param-list__item Compare-param-list__item-n-4 hover-n-4">Корпус</div>
                                        <div class="Compare-param-list__item Compare-param-list__item-n-5 hover-n-5">Секция</div>
                                        <div class="Compare-param-list__item Compare-param-list__item-n-6 hover-n-6">Этаж</div>
                                        <div class="Compare-param-list__item Compare-param-list__item-n-7 hover-n-7">Количество комнат</div>
                                        <div class="Compare-param-list__item Compare-param-list__item-n-8 hover-n-8">Планировка</div>
                                    </section>

                                    <section class="Compare-param-list__section">
                                        <div class="Compare-param-list__item Compare-param-list__item-n-9  hover-n-9">Отделка</div>

                                        <div class="Compare-param-list__item Compare-param-list__item-n-10 hover-n-10">Общая площадь</div>
                                        <div class="Compare-param-list__subitem Compare-param-list__subitem-n-11 hover-n-11">Прихожая</div>
                                        <div class="Compare-param-list__subitem Compare-param-list__subitem-n-12 hover-n-12">Комната</div>
                                        <div class="Compare-param-list__subitem Compare-param-list__subitem-n-13 hover-n-13">Кухня-гостиная</div>
                                        <div class="Compare-param-list__subitem Compare-param-list__subitem-n-14 hover-n-14">Санузел</div>
                                        <div class="Compare-param-list__subitem Compare-param-list__subitem-n-15 hover-n-15">Кладовая</div>
                                        <div class="Compare-param-list__subitem Compare-param-list__subitem-n-16 hover-n-16">Балкон</div>
                                    </section>
                                </div>
                                <div class="Compare-mobile-slider__col">
                                    <section class="Compare-slider-slide__section">
                                        <div class="Compare-slider-slide__row Compare-slider-slide__row-n-2 hover-n-2">3 198 789 Р</div>
                                        <div class="Compare-slider-slide__row Compare-slider-slide__row-n-3 hover-n-3">1 кв 2019</div>
                                        <div class="Compare-slider-slide__row Compare-slider-slide__row-n-4 hover-n-4">2а</div>
                                        <div class="Compare-slider-slide__row Compare-slider-slide__row-n-5 hover-n-5">3</div>
                                        <div class="Compare-slider-slide__row Compare-slider-slide__row-n-6 hover-n-6">6</div>
                                        <div class="Compare-slider-slide__row Compare-slider-slide__row-n-7 hover-n-7">1</div>
                                        <div class="Compare-slider-slide__row Compare-slider-slide__row-n-8 hover-n-8">Типовая 4</div>
                                    </section>

                                    <section class="Compare-slider-slide__section">
                                        <div class="Compare-slider-slide__row Compare-slider-slide__row-n-9 hover-n-9">Есть</div>
                                        <div class="Compare-slider-slide__row Compare-slider-slide__row-n-10 hover-n-10">43.4 м2</div>
                                        <div class="Compare-slider-slide__row Compare-slider-slide__row-n-11 hover-n-11">4.9 м2</div>
                                        <div class="Compare-slider-slide__row Compare-slider-slide__row-n-12 hover-n-12">16.6 м2</div>
                                        <div class="Compare-slider-slide__row Compare-slider-slide__row-n-13 hover-n-13">12.4 м2</div>
                                        <div class="Compare-slider-slide__row Compare-slider-slide__row-n-14 hover-n-14">16.6 м2</div>
                                        <div class="Compare-slider-slide__row Compare-slider-slide__row-n-15 hover-n-15">3.4 м2</div>
                                        <div class="Compare-slider-slide__row Compare-slider-slide__row-n-16 hover-n-16">3.2 м2</div>
                                    </section>
                                </div>
                            </div>
                            <div class="Compare-mobile-slider__content Compare-mobile-slider__content_action">
                                <div class="Compare-mobile-slider__col Compare-mobile-slider__col_action">
                                    <section class="Compare-param-list__section Compare-param-list__section_action">
                                        <div class="Compare-param-list__item  hover-n-17">Акции</div>

                                        <div class="Compare-param-list__subitem-2 Compare-param-list__subitem-2-n-18 hover-n-18">Скидка 2% при заключении договора в июне.</div>
                                        <div class="Compare-param-list__subitem-2 Compare-param-list__subitem-2-n-19 hover-n-19">Скидка 2% при заключении договора в июне.</div>
                                        <div class="Compare-param-list__subitem-2 Compare-param-list__subitem-2-n-20 hover-n-20">Рассрочка до 2023 года.</div>
                                        <div class="Compare-param-list__subitem-2 Compare-param-list__subitem-2-n-21 hover-n-21">Рассрочка до 2023 года.</div>
                                        <div class="Compare-param-list__subitem-2 Compare-param-list__subitem-2-n-22 hover-n-22">Рассрочка до 2023 года.</div>
                                    </section>
                                </div>
                                <div class="Compare-mobile-slider__col Compare-mobile-slider__col_action">
                                    <section class="Compare-slider-slide__section">
                                        <div class="Compare-slider-slide__row Compare-slider-slide__row-n-17 hover-n-17">&nbsp;</div>
                                        <div class="Compare-slider-slide__row Compare-slider-slide__row-n-18 hover-n-18"><div class="actionIcon actionIcon_red" title="1"></div></div>
                                        <div class="Compare-slider-slide__row Compare-slider-slide__row-n-19 hover-n-19"><div class="actionIcon actionIcon_blue" title="1"></div></div>
                                        <div class="Compare-slider-slide__row Compare-slider-slide__row-n-20 hover-n-20"><div class="actionIcon actionIcon_orange" title="1"></div></div>
                                        <div class="Compare-slider-slide__row Compare-slider-slide__row-n-21 hover-n-21"><div class="actionIcon actionIcon_orange" title="1"></div></div>
                                        <div class="Compare-slider-slide__row Compare-slider-slide__row-n-22 hover-n-22"><div class="actionIcon actionIcon_orange" title="1"></div></div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="Compare-tabs-content" data-id-compare="ComparePlans">
            <div class="Compare-slider swiper-container">
                <div class="swiper-wrapper">
                    <div class="Compare-slider-slide swiper-slide">
                        <div class="Compare-slider-slide__row Compare-slider-slide__row-n-1">
                            <div class="Compare-slider-slide__img" style="background-image: url('https://etalonsad.ru//upload/information_system_2/3/7/6/item_376/information_items_property_49.svg');"></div>
                            <div class="Compare-slider-slide__title">Планировка, типовая {{n}}</div>
                            <div class="FavoriteIcon FavoriteIcon-compare"></div>
                            <div class="Compare-slider-slide__remove"></div>
                        </div>

                        <section class="Compare-slider-slide__section">
                            <div class="Compare-slider-slide__row Compare-slider-slide__row-n-2 hover-n-2">3 198 789 Р</div>
                            <div class="Compare-slider-slide__row Compare-slider-slide__row-n-3 hover-n-3">Есть</div>
                            <div class="Compare-slider-slide__row Compare-slider-slide__row-n-4 hover-n-4">1</div>
                        </section>

                        <section class="Compare-slider-slide__section">
                            <div class="Compare-slider-slide__row Compare-slider-slide__row-n-9 hover-n-9">Есть</div>
                            <div class="Compare-slider-slide__row Compare-slider-slide__row-n-10 hover-n-10">43.4 м2</div>
                            <div class="Compare-slider-slide__row Compare-slider-slide__row-n-11 hover-n-11">4.9 м2</div>
                            <div class="Compare-slider-slide__row Compare-slider-slide__row-n-12 hover-n-12">16.6 м2</div>
                            <div class="Compare-slider-slide__row Compare-slider-slide__row-n-13 hover-n-13">12.4 м2</div>
                            <div class="Compare-slider-slide__row Compare-slider-slide__row-n-14 hover-n-14">16.6 м2</div>
                            <div class="Compare-slider-slide__row Compare-slider-slide__row-n-15 hover-n-15">3.4 м2</div>
                            <div class="Compare-slider-slide__row Compare-slider-slide__row-n-16 hover-n-16">3.2 м2</div>
                        </section>


                    </div>


                </div>
            </div>
        </div>
    </div>

</div>








