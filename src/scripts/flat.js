$(".Flat_linkBack").on("click", function(event) {
    let isPrevPageOnTheSameSite = _.inRange(document.referrer.indexOf(window.location.host), 5, 9);
    let wasPrevPageFilter = document.referrer.indexOf("bedrooms") !== 1;
    if (isPrevPageOnTheSameSite && wasPrevPageFilter) {
        event.preventDefault();
        window.location.href = document.referrer;
    } else {
        return true;
    }
});

let sliders = [];
$(".FlatsTips_sliderContainer").each(function(index, el) {
    let arrows = el.parentNode.querySelectorAll(".FlatsTips_slideBtn");

    let slider = new Swiper(el, {
        slidesPerView: 4,
        navigation: {
            prevEl: arrows[0],
            nextEl: arrows[1],
        },
        resistanceRatio: 0,
        breakpoints: {
            1279: {
                slidesPerView: 2,
            },
            767: {
                slidesPerView: 1,
            },
        },
    });

    sliders.push(slider);
});

let $tabs = $(".FlatsTips");

let tabsSwiperInst = new Swiper(".FlatsTips_tabsWrapperMobile", {
    slidesPerView: 1,
    navigation: {
        nextEl: ".FlatsTips_slideBtnMobile-next",
        prevEl: ".FlatsTips_slideBtnMobile-prev",
    },

    on: {
        slideChange: () => {
            $tabs.tabs("option", "active", tabsSwiperInst.activeIndex);
        },
    },
});

$tabs.tabs({
    classes: {
        "ui-tabs": "",
        "ui-tabs-nav": "",
        "ui-tabs-tab": "",
        "ui-tabs-panel": "",
        "ui-tabs-active": "FlatsTips_tabsItem-active",
    },
    activate: (event, ui) => {
        let tabIndex = ui.newTab.index();

        sliders[tabIndex].update();
        tabsSwiperInst.slideTo(tabIndex, 0, false);
    },
});

$(".Flat_infoTabs").tabs({
    classes: {
        "ui-tabs": "",
        "ui-tabs-nav": "",
        "ui-tabs-tab": "",
        "ui-tabs-panel": "",
        "ui-tabs-active": "Flat_infoTabsNavItem-active",
    },
});
toolTip();


let favIconTitle = $(".Flat-type_info .FavoriteIcon, .FlatsTips_icons .FavoriteIcon")
let flatId = favIconTitle.data('flat-type-id');
favListFlats.isFavorite(flatId);
if ( favListFlats.isFavorite(flatId) ) {
    favIconTitle.addClass('FavoriteIcon-active');
}

$(".Flat_info, .FlatsTips_icons").on('click', ".FavoriteIcon", (event)=> {
    event.preventDefault();
    let $favLink = $(event.currentTarget);
    console.log('$favLink', $favLink)

    if ($favLink.hasClass('FavoriteIcon-active')) {
        favListFlats.remove($favLink.data('flat-type-id'));
    }else{
        favListFlats.add($favLink.data('flat-type-id'));
    }

    $favLink.toggleClass('FavoriteIcon-active');
    displayHeaderFavIcon();
    toolTip();
});
// END: favorites toggling behaviour

// START: comparison toggling behaviour
$(".Flat_info, .FlatsTips_icons").on('click', ".ComparisonIcon", (event)=> {
    event.preventDefault();
    let $compareLink = $(event.currentTarget);
    console.log('$favLink', $compareLink)

    if ($compareLink.hasClass('ComparisonIcon-active')) {
        flatsToCompare.remove($compareLink.data('flat-type-id'));
    }else{
        flatsToCompare.add($compareLink.data('flat-type-id'));
    }

    $compareLink.toggleClass('ComparisonIcon-active');

});

setActiveFavorite();
// END: comparison toggling behaviour

$('.PromoActionString').each(function () {
    let title = $(this).attr('title');
    $(this).tooltip({
        classes: {
            "ui-tooltip": "PromoActionString_toolp"
        },
        content: function () {
            return title;
        }
    })
})

const widgetID = $('[data-widget-id]').data('widget-id');

Planoplan.init({
    uid: widgetID,
    el: 'plan-widget'
});

new InteractiveForm('[data-booking-form]');


