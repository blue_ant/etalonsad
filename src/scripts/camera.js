$(() => {
    var $window = $(window);

    $('[data-camera]').each(function() {
        let $wrapper = $(this);
        let $camera  = $wrapper.find('iframe');
        let curSrc   = $camera.attr('src').split('?')[0];

        setCameraSize();

        function setCameraSize() {
            let curWidth     = $wrapper.width();
            let cameraWidth  = Math.round(curWidth);
            let cameraHeight = Math.round(cameraWidth / (16/9));
            let updateSrc    = curSrc + '?' + cameraWidth + 'x' + cameraHeight;

            $camera.css({
                width: cameraWidth,
                height: cameraHeight
            }).attr('src', updateSrc);
        }

        // $window.on('resize', setCameraSize);
    });
});
