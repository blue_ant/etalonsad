const FLATS_PER_PAGE = 24;

new ViewSwitch(".ViewSwitch", {
    /*onChange: ($activeBtn) => {
        console.log($activeBtn.attr('href'));
    }*/
});

let FavListPlansTplStr = `
/*=require ./includes/chunks/FavListPlans.tpl*/
`;

let FavListFlatsTplStr = `
/*=require ./includes/chunks/FavListFlats.tpl*/
`;

let favPlansListBuilder = new FlatsList({
    tpl: FavListPlansTplStr,
    $mountEl: $("#filterResultPlans"),
});

let favFlatsListBuilder = new FlatsList({
    tpl: FavListFlatsTplStr,
    $mountEl: $("#filterResultFlats"),
});




let $progress = $(".PickupFlatLayout_progress");
let $resultsContFlats = $("#filterResultFlats");
let $resultsContPlans = $("#filterResultPlans");
let $resultsCont = $(".PickupFlatLayout_resultsCont");

let emptyTextFlats = "<div class='PickupFlatLayout_resultsCont_empty'>В избранном нет квартир...</div>";
let emptyTextPlans = "<div class='PickupFlatLayout_resultsCont_empty'>В избранном нет планировок...</div>";

let filter = new FilterForm("#filterForm", {
    submitHandler: ($filterForm) => {
        // console.log(" --- submitHandler fired---", "filter.offset: ", filter.offset);

        $resultsCont.addClass("PickupFlatLayout_resultsCont-loading");
        $progress.show().animate({ width: "33%", opacity: 1 }, 600);

        $.ajax({
            url: $filterForm.attr("target"),
            dataType: "json",
            method: $filterForm.attr("method"),
            async: true,
            data: $.extend(true, $filterForm.serializeObject(), {
                // plans: favListPlans.favorites,
                flats: favListFlats.favorites
            }),
            xhr: () => {
                let xhr = new window.XMLHttpRequest();
                xhr.addEventListener("progress", (event) => {
                    if (event.lengthComputable) {
                        let percent = Math.ceil((100 * event.loaded) / event.total);
                        $progress.stop(true, false).animate({ width: percent + "%" }, 400);
                        if (percent === 100) {
                            $progress.animate({ opacity: 0 }, 350, () => {
                                $progress.removeAttr("style");
                            });
                        }
                    }

                    else {
                        $progress.stop(true, false).animate({ width: '100' + "%" }, 400);
                        $progress.animate({ opacity: 0 }, 350, () => {
                            $progress.removeAttr("style");
                        });
                    }
                });

                return xhr;
            },
        })
            .done((jsonResponse) => {
                console.log('jsonResponse', jsonResponse)
                // fix pics urls in development enviroment
                if (window.location.href.indexOf("/filters.html") != -1) {
                    console.warn("rewrite json response for dev enviroment usage...");
                    jsonResponse.plans.forEach((flt) => {
                        flt.pic = "https://etalonsad.ru/" + flt.pic;
                        flt.href = "/flat.html";
                    });
                    console.log('jsonResponse', jsonResponse)
                }


                let favPlansFlats = jsonResponse;


                //---- filter favorites
                favPlans = _.filter(favPlansFlats.plans, (pln) => {
                    return _.includes(favListPlans.favorites, pln.id);
                });
                console.log('favPlans', favPlans)

                favFlats = _.filter(favPlansFlats.flats, (pln) => {
                    return _.includes(favListFlats.favorites, pln.id);
                });

                console.log('favFlats', favFlats)

                // mark all plans as favorite
                favPlans.forEach((pln) => {
                    pln.favorite = 1;
                });

                favFlats.forEach((pln) => {
                    pln.favorite = 1;
                });

                // render favorite plans list
                // убираем вывод планрировок, пока их нет
                // if (favPlans.length) {
                //     favPlansListBuilder.render({
                //         data: favPlans,
                //     });
                //
                // } else {
                //     $resultsContPlans.html(emptyTextPlans);
                // }

                if (favFlats.length) {
                    favFlatsListBuilder.render({
                        data: favFlats,
                    });
                } else {
                    $resultsContFlats.html(emptyTextFlats);
                }




                $('[data-id="FavListFlats"] span').html(favFlats.length);
                // убираем вывод планрировок, пока их нет
                // $('[data-id="FavListPlans"] span').html(favPlans.length);

                $resultsCont.removeClass("PickupFlatLayout_resultsCont-loading");
            })
            .fail(() => {
                alert("Не удалось получить данные с сервера!\nПопробуйте позже.");
            });
    },
});

let sorter = new Sorter(".Sorter");
let sorterTiles = new Sorter(".Sorter-tiles");

$("#filterResult").tooltip({
    items: ".actionIcon",
    classes: {
        "ui-tooltip": "actionIconTooltip",
    },
});

toolTip();

// START: favorites toggling behaviour

$("#filterResultPlans").on('click', '.FavoriteIcon', (event) => {
    event.preventDefault();
    let $favLink = $(event.currentTarget);
    let planId = $favLink.data('plan-id');

    $('[data-plan-remove="'+ planId +'"]').each(function (index, el) {
        $(this).fadeOut('100', () => {
            $(this).remove();
        });
    })

    favListPlans.remove($favLink.data('plan-id'));
    $('[data-id="FavListPlans"] span').html(favListPlans.favorites.length);
    displayHeaderFavIcon();

    let amountFavorites = favListPlans.favorites.length;
    if (amountFavorites < 1) {
        $resultsContPlans.html(emptyTextPlans);
    }

    toolTip();
});

// END: favorites toggling behaviour


// START: favorites toggling behaviour

$("#filterResultFlats").on('click', '.FavoriteIcon', (event) => {
    event.preventDefault();
    let $favLink = $(event.currentTarget);
    let flatId = $favLink.data('flat-type-id');

    $('[data-flat-remove="'+ flatId +'"]').each(function (index, el) {
        $(this).fadeOut('100', () => {
            $(this).remove();
        });
    })
    $('[data-remove-id="'+ flatId +'"]').parent().parent().each(function (index, el) {
        $(this).fadeOut('100', () => {
            $(this).remove();
        });
    });

    favListFlats.remove($favLink.data('flat-type-id'));
    $('[data-id="FavListFlats"] span').html(favListFlats.favorites.length);
    displayHeaderFavIcon();

    let amountFavorites = favListFlats.favorites.length;
    if (amountFavorites < 1) {
        $resultsContFlats.html(emptyTextFlats);
    }

    toolTip();
});

// END: favorites toggling behaviour

// выключаем табы, пока нет планировок
// $('.FavoriteTabs__link').click(function (e) {
//     e.preventDefault();
//     let id = $(this).data("id");
//     $(this).parent().find('.FavoriteTabs__link').removeClass('FavoriteTabs__link-active');
//     $(this).addClass('FavoriteTabs__link-active');
//     $('.FavoriteTabs__block').removeClass('FavoriteTabs__block-visible')
//     $('#' + id).addClass('FavoriteTabs__block-visible');
//     $('.favoriteRemove').removeClass('favoriteRemove-active');
//     $('[data-favlist="'+id+'"]').addClass('favoriteRemove-active');
// })

$('[data-favorites-page]').on("click",".FavoriteTabs__link", function (e) {
    e.preventDefault();
    let id = $(this).data("id");
    $(this).parent().find('.FavoriteTabs__link').removeClass('FavoriteTabs__link-active');
    $(this).addClass('FavoriteTabs__link-active');
    $('.FavoriteTabs__block').removeClass('FavoriteTabs__block-visible')
    $('#' + id).addClass('FavoriteTabs__block-visible');
    $('.favoriteRemove').removeClass('favoriteRemove-active');
    $('[data-favlist="'+id+'"]').addClass('favoriteRemove-active');
} );



$('.favoriteRemove').click(function (e) {
    e.preventDefault();
    let favList = $(this).data("favlist");

    let plansWrapper = $('#filterResultPlans');
    let flatsWrapper = $('#filterResultFlats');


    if (favList === 'FavListPlans') {
        plansWrapper.find('[data-plan-id]').each(function (index, el) {
            let planId = $(this).data("plan-id");
            favListPlans.remove(planId);
        });
        let itemPlan = plansWrapper.find('.Tbl_tr');
        itemPlan.fadeOut('100', () => {
            itemPlan.remove();
        });
        $('[data-id="FavListPlans"] span').html(favListPlans.favorites.length);

        let amountFavorites = favListPlans.favorites.length;
        if (amountFavorites < 1) {
            $resultsContPlans.html(emptyTextPlans);
        }
    } else if (favList === 'FavListFlats') {
        flatsWrapper.find('[data-flat-type-id]').each(function (index, el) {
            let flatId = $(this).data("flat-type-id");
            favListFlats.remove(flatId);
        });
        let itemFlat = flatsWrapper.find('.Tbl_tr');
        itemFlat.fadeOut('100', () => {
            itemFlat.remove();
        });
        $('[data-id="FavListFlats"] span').html(favListFlats.favorites.length);

        let amountFavorites = favListFlats.favorites.length;
        if (amountFavorites < 1) {
            $resultsContFlats.html(emptyTextFlats);
        }
    }



    displayHeaderFavIcon();
    console.log('favListPlans', favListPlans)
})




