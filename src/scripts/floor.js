let $scheme = $(".FloorScheme");

{
    let bgimg = document.querySelector(".FloorScheme_bg");
    let $svg  = $scheme.find("svg");
    var img   = new Image();

    img.onload = function() {
        $svg
            .attr({
                viewBox: `0 0 ${this.naturalWidth} ${this.naturalHeight}`,
                width: this.naturalWidth,
                height: this.naturalHeight,
            })
            .css({
                display: "block",
            });

        let urlParams = parseQueryString(window.location.search);

        if (urlParams.active_flat_id) {
            $svg.find(`[data-id="${urlParams.active_flat_id}"]`).addClass("FloorScheme_flat-active");
        }
    };

    img.src = $(bgimg).attr('src');
}

$scheme.find("[data-url]").each(function(index, el) {
    let $el = $(el);
    let data = window.floorTolltips[index];

    $el
        .tooltip({
            items: $el,
            track: true,
            tooltipClass: "floor__plan-tooltip",
            content: `
        <div class="floor__plan-tooltip-row">
            <div class="floor__plan-tooltip-number">${data.apts ? 'Апартаменты' : 'Квартира'} № ${data.num}</div>
            <div class="floor__plan-tooltip-text">${data.rooms ?  data.rooms + ' комн.' : 'студия'}</div>
        </div>
        <div class="floor__plan-tooltip-row">
            <div class="floor__plan-tooltip-price">${data.price} \u20bd</div>
            <div class="floor__plan-tooltip-text">${data.area} м<span>2</span></div>
        </div>    
    `
        })
        .on("click", function() {
            window.location.href = $(this).attr("data-url");
        });
});

