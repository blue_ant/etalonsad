const $window = $(window);

$(() => {
    $('[data-map]').each(function() {
        let map = new Map($(this));
    });

    $('.contact-offices-types').tabs({
		classes: {
			"ui-tabs": "contact-offices-types-tabs",
			"ui-tabs-nav": "contact-offices-types-tabs__nav",
			"ui-tabs-tab": "contact-offices-types-tabs__tab",
			"ui-tabs-panel": "contact-offices-types-tabs__panel"
		}
	});

	var winWidth = $window.width();

	if (winWidth < 768) {
		$('.contacts-header-links').appendTo('#office-object');
	} else {
		$('.contacts-header-links').appendTo('.contacts-header-inner');
	}

	$window.resize(function() {
		winWidth = $window.width();

		if (winWidth < 768) {
			$('.contacts-header-links').appendTo('#office-object');
		} else {
			$('.contacts-header-links').appendTo('.contacts-header-inner');
		}
	});


	if (winWidth > 1023) {
        $(".contact-offices-item__text").mCustomScrollbar({
            scrollInertia: 0,
            mouseWheel: { preventDefault: true },
        });
    } else {
        $(".contact-offices-item__text").mCustomScrollbar("destroy");
    }
    $window.resize(function() {
        winWidth = $window.width();

        if (winWidth > 1023) {
            $(".contact-offices-item__text").mCustomScrollbar({
                scrollInertia: 0,
                mouseWheel: { preventDefault: true },
            });
        } else {
            $(".contact-offices-item__text").mCustomScrollbar("destroy");
        }
    });

});

function teamScroll() {
    if (window.matchMedia("(max-width: 1023px)").matches) {
        $("[data-team]").mCustomScrollbar('destroy');
    } else {
        $("[data-team]").mCustomScrollbar({
            scrollInertia: 0,
            mouseWheel: { preventDefault: true },
        });
    }
}
function teamScrollXS() {
    if (window.matchMedia("(max-width: 1023px)").matches) {
        $('.contact-offices-types-tabs__nav').css('width', $(window).width() + 'px');
    } else {
        $('.contact-offices-types-tabs__nav').css('width', '');
    }
}
teamScroll();
setTimeout(teamScrollXS, 400);
$(window).on('resize', function () {
    teamScroll();
    teamScrollXS();
});