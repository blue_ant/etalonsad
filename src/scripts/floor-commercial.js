let $scheme = $(".FloorScheme");

{
    let bgimg = document.querySelector(".FloorScheme_bg");
    let $svg = $scheme.find("svg");
    let poll = setInterval(function() {
        if (bgimg.naturalWidth) {
            clearInterval(poll);
            $svg
                .attr({
                    viewBox: `0 0 ${bgimg.naturalWidth} ${bgimg.naturalHeight}`,
                    width: bgimg.naturalWidth,
                    height: bgimg.naturalHeight,
                })
                .css({
                    display: "block",
                });

            let urlParams = parseQueryString(window.location.search);
            if (urlParams.active_flat_id) {
                $svg.find(`[data-id="${urlParams.active_flat_id}"]`).addClass("FloorScheme_flat-active");
            }
        }
    }, 10);
}

$scheme.find("[data-url]").each(function(index, el) {
    let $el = $(el);
    let data = window.floorTolltips[index];

    $el
        .tooltip({
            items: $el,
            track: true,
            tooltipClass: "floor__plan-tooltip",
            content:
            '<div class="floor__plan-tooltip-row">' +
            '<div class="floor__plan-tooltip-number">Помещение</div>' +
            '</div>' +
            '<div class="floor__plan-tooltip-row">' +
            '<div class="floor__plan-tooltip-price">' + data.price + ' \u20bd</div>' +
            '<div class="floor__plan-tooltip-text">' + data.area + ' м<span>2</span></div>' +
            '</div>',
        })
        .on("click", function() {
            window.location.href = $(this).attr("data-url");
        });
});
