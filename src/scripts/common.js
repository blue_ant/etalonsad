/*=require ./includes/blocks/*.js */
/*=require ./includes/chunks/google_map_styles.js */

//parseQueryString function extracted from mithrill.js library

/*let $subscribeForm = $('[data-subscribe]');

new InteractiveForm($subscribeForm);*/

function parseQueryString(string) {
	if (string === "" || string == null) return {};
	if (string.charAt(0) === "?") string = string.slice(1);
	var entries = string.split("&"),
		data0 = {},
		counters = {};
	for (var i = 0; i < entries.length; i++) {
		var entry = entries[i].split("=");
		var key5 = decodeURIComponent(entry[0]);
		var value = entry.length === 2 ? decodeURIComponent(entry[1]) : "";
		if (value === "true") value = true;
		else if (value === "false") value = false;
		var levels = key5.split(/\]\[?|\[/);
		var cursor = data0;
		if (key5.indexOf("[") > -1) levels.pop();
		for (var j = 0; j < levels.length; j++) {
			var level = levels[j],
				nextLevel = levels[j + 1];
			var isNumber = nextLevel == "" || !isNaN(parseInt(nextLevel, 10));
			var isValue = j === levels.length - 1;
			if (level === "") {
				var key5 = levels.slice(0, j).join();
				if (counters[key5] == null) counters[key5] = 0;
				level = counters[key5]++;
			}
			if (cursor[level] == null) {
				cursor[level] = isValue ? value : isNumber ? [] : {};
			}
			cursor = cursor[level];
		}
	}
	return data0;
}

// Инициализация форм
webshim.setOptions('forms', {
	lazyCustomMessages: true,
	addValidators: true,
});

webshim.setOptions('basePath', '/libraries/shims/');

//start polyfilling
webshim.polyfill('forms');

const $document = $(document);
const $body     = $('body');
const active    = '_active';
const loading   = '_loading';

$(document).ready(function() {

    if ($(".Flat_planLink-floor").length) {
        let $floor = $(".Flat_planLink-floor");

        {
            let bgimg = document.querySelector(".FloorScheme_bg");
            let $svg  = $floor.find("svg");
            var img   = new Image();

            img.onload = function() {
                $svg
                    .attr({
                        viewBox: `0 0 ${this.naturalWidth} ${this.naturalHeight}`,
                        width: this.naturalWidth,
                        height: this.naturalHeight,
                    })
                    .css({
                        display: "block",
                    });

                let urlParams = parseQueryString(window.location.search);

                if (urlParams.active_flat_id) {
                    $svg.find(`[data-id="${urlParams.active_flat_id}"]`).addClass("FloorScheme_flat-active");
                }
            };

            img.src = $(bgimg).attr('src');
        }
    }

    $(function() {
        $('.footer-subscribe__link').click(function(e) {
            e.preventDefault();
            $(this).addClass('footer-subscribe__link_hidden');
            //$('.footer__phone').addClass('footer__phone_translate');
            //$('.footer-subscribe__form').slideDown(1000);
            $('.footer-subscribe__form').addClass('_show');
        });
    });

    $('[data-video]').each(function() {
        let $wrapper = $(this);
        let $inner   = $wrapper.find('.longread-video__bg');
        let $video   = $wrapper.find('iframe');
        let curSrc   = $video.attr('src');

        $inner.on('click', function() {
            $(this).remove();
            $video.attr('src', curSrc + '&autoplay=1');
        });
    });

    var $window = $(window);

    let checkWidth = ()=> {
        var windowsize = $window.width();
        if (windowsize < 768) {
            $('.footer-socials').appendTo('.footer-menu-col_subscribe-wrap');
            $('.full-menu-link_select-flat').prependTo($('.full-menu .full-menu-col').eq(0));
        } else {
            $('.footer-socials').appendTo('.footer-bottom');
            $('.full-menu-link_select-flat').prependTo($('.full-menu .full-menu-col').last());
        }

        if (windowsize < 768) {
            $('.main-content-img__text_sec4').insertAfter('.main-section__title_sec4');
        } else {
            $('.main-content-img__text_sec4').appendTo('.main-section__img_sec4');
        }
        if (windowsize < 1280 && windowsize > 767) {


        }
    };
    checkWidth();

    $(window).resize(function() {
        checkWidth();
    });

    $.fn.hyphenates = function() {
        var e = "[абвгдеёжзийклмнопрстуфхцчшщъыьэюя]";
        var t = "[аеёиоуыэюя]";
        var n = "[бвгджзклмнпрстфхцчшщ]";
        var r = "[йъь]";
        var i = "­";
        var s = new RegExp("(" + r + ")(" + e + e + ")", "ig");
        var o = new RegExp("(" + t + ")(" + t + e + ")", "ig");
        var u = new RegExp("(" + t + n + ")(" + n + t + ")", "ig");
        var a = new RegExp("(" + n + t + ")(" + n + t + ")", "ig");
        var f = new RegExp("(" + t + n + ")(" + n + n + t + ")", "ig");
        var l = new RegExp("(" + t + n + n + ")(" + n + n + t + ")", "ig");
        this.each(function() {
            var e = $(this).html();
            e = e.replace(s, "$1" + i + "$2");
            e = e.replace(o, "$1" + i + "$2");
            e = e.replace(u, "$1" + i + "$2");
            e = e.replace(a, "$1" + i + "$2");
            e = e.replace(f, "$1" + i + "$2");
            e = e.replace(l, "$1" + i + "$2");
            $(this).html(e);
        });
    };

    $('.main-section__title_sec5').hyphenate('en-us').hyphenates();

    var popupMenu = $('.full-menu-popup'),
        heightpopupMenu = popupMenu.height();

    popupMenu.css({ 'top': + -heightpopupMenu + 'px' });

    setTimeout(function() {
        popupMenu.show();
    }, 50);

    $('.header__burger, .full-menu-overlay').click(function() {
        heightpopupMenu = $('.full-menu-popup').height();
        $('.header__burger').toggleClass('open');

        $('.full-menu-popup').toggleClass('full-menu-popup_open');


        var checkOpenMenu = popupMenu.hasClass('full-menu-popup_open');
        if (checkOpenMenu) {
            popupMenu.css({ 'top': +0 + 'px' });
            $('.full-menu-overlay').addClass("active");
            //$('body').prepend('<div class="full-menu-overlay"></div>')
        } else {
            popupMenu.css({ 'top': + -heightpopupMenu + 'px' });
            //$('.full-menu-overlay').remove();
            $('.full-menu-overlay').removeClass("active");
        }
    });

    $('.footer-subscribe__input').on('blur', function() {
        var $this = $(this);
        var $label = $(this).parent().find('.footer-subscribe__label');
        if ($this.val() !== "") {
            $label.addClass('not-empty');
        } else {
            $label.removeClass('not-empty');
        }
    });

    $('.main-section__link').hover(
        function() {
            $(this).parent().parent().parent().find('.btn-more').addClass('btn-more_hover')
        },
        function() {
            $(this).parent().parent().parent().find('.btn-more').removeClass('btn-more_hover');
        }
    );

    $(".footer-subscribe__form").on('submit', function(event) {
        event.preventDefault();
        let $form = $(this);

        let dataToSend = $.extend(true, $form.serializeObject(), {
            Submit: 1,
            url: window.location.href,
        });

        $.ajax({
            url: $form.data("action"),
            type: "json",
            method: "POST",
            data: dataToSend,
        }).done((response) => {
            let errorCode = parseInt(response.code);

            if (errorCode === 0) {
                $form.trigger('reset').hide();

                let successText = `
                    <div class="footer-subscribe-inner footer-subscribe-inner_sendMessage">
                        <em class=footer-subscribe__close"></em>
                        <div class="footer-subscribe__text">
                            ${response.success}
                        </div>
                    </div>`;
                window.requestAnimationFrame(() => {
                    $form.hide().after(successText);
                });

            } else {
                alert("Не удалось отправить форму! Попробуйте позже или обратитесть по телефону...");
            }
        }).always(( /*response*/ ) => {

        });

        return false;
    });

    $('.page-form__inp, .page-form__txtarea')
        .attr('autocomplete', 'off')
        .on('focus', function() {
            let $this    = $(this);
            let $wrapper = $this.closest('.page-form__step');
            let isDate   = $wrapper.hasClass('_date');

            if (isDate) {
                $wrapper
                    .addClass(active)
                    .find('.page-form__inp')
                    .addClass(active);

                return;
            }

            $this.addClass(active);
        })
        .on('blur', function() {
            let $this    = $(this);
            let $wrapper = $this.closest('.page-form__step');
            let $inputs  = $wrapper.find('.page-form__inp');
            let isDate   = $wrapper.hasClass('_date');

            if ($this.closest('.calculator-aside').length) return;

            if (isDate) {
                if ($inputs.eq(0).val() === '' && $inputs.eq(1) === '') {
                    $wrapper
                        .removeClass(active)
                        .find('.page-form__inp')
                        .removeClass(active);
                }

                return;
            }

            if ($this.val() === '') {
                $this.removeClass(active);

                return true;
            }
        });

    $('[data-mask]').each(function() {
        let $this = $(this);
        let type  = $this.data('mask');
        let mask;
        let options;

        if (type === 'date') {
            $this.datepicker({
                closeText: "Закрыть",
                prevText: "",
                nextText: "",
                currentText: "Сегодня",
                monthNames: [ "Январь","Февраль","Март","Апрель","Май","Июнь","Июль","Август","Сентябрь","Октябрь","Ноябрь","Декабрь"],
                monthNamesShort: [ "Янв","Фев","Мар","Апр","Май","Июн","Июл","Авг","Сен","Окт","Ноя","Дек"],
                dayNames: [ "воскресенье","понедельник","вторник","среда","четверг","пятница","суббота"],
                dayNamesShort: [ "вск","пнд","втр","срд","чтв","птн","сбт"],
                dayNamesMin: [ "Вс","Пн","Вт","Ср","Чт","Пт","Сб"],
                weekHeader: "Нед",
                dateFormat: "dd.mm.yy",
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: "",
                showOptions: {
                    direction: "up"
                },
                onSelect: function(val, inst) {
                    let $label = $this.closest('.page-form__lbl');

                    if (val === '') {
                        $label.find('.ui-datepicker-remover').remove();
                    } else {
                        if (!$label.find('.ui-datepicker-remover').length) {
                            $label.append('<span class="ui-datepicker-remover"></span>');
                        }
                    }

                    $('[data-mask="time-simple"]').focus();
                    $(inst.dpDiv).addClass('_selected');
                },
                beforeShow: function (input, inst) {
                    var rect = input.getBoundingClientRect();

                    setTimeout(function () {
                        inst.dpDiv.css({ top: rect.top - 300, left: rect.left + 0 });
                    }, 0);
                }
            }).on('keyup', function() {
                if ($(this).val() === '') {
                    $('.ui-datepicker-remover').remove();
                }
            });

            $document.on('click', '.ui-datepicker-remover', function() {
                $this.val('');
                $(this).remove();
            });

            return;
        }

        if (type === 'phone') {
            mask = '+7 (999) 999-99-99';
        } else if (type === 'time') {
            mask = 'с 99:99 до 99:99';
        }

        if (type === 'time-simple') {
            mask = '00 : 00';

            options = {
                alias: 'datetime',
                placeholder: "00 : 00",
                inputFormat: 'HH : MM',
                showMaskOnHover: false,
                showMaskOnFocus: true
            };
        } else {
            options = {
                mask: mask,
                showMaskOnHover: false,
                //disablePredictiveText: true
            };
        }

        $this.inputmask(options);
    });

    $document.on('mouseover', '[data-title]', function(e) {
        let $this    = $(this);
        let position = $this.offset();
        let title    = $this.data('title');
        let width    = $body.width();
        let icoWidth = $this.width();

        $body.append('<div class="title">' + title + '</div>');

        let $tltPopup = $('.title');
        let popupWidth = $tltPopup.outerWidth();

        if (position.left + popupWidth > width) {
            $tltPopup.css({
                left     : position.left - popupWidth - icoWidth - 20,
                top      : position.top
            });
        } else {
            $tltPopup.css({
                left     : position.left,
                top      : position.top
            });
        }
    });

    $document.on('mouseout', '[data-title]', function(e) {
        $('.title').remove();
    });

    $document.on('mouseover', '[data-title]', function(e) {
        let $this    = $(this);
        let position = $this.offset();
        let title    = $this.data('title');
        let width    = $body.width();
        let icoWidth = $this.width();

        $body.append('<div class="title">' + title + '</div>');

        let $tltPopup = $('.title');
        let popupWidth = $tltPopup.outerWidth();

        if (position.left + popupWidth > width) {
            $tltPopup.css({
                left     : position.left - popupWidth - icoWidth - 20,
                top      : position.top
            });
        } else {
            $tltPopup.css({
                left     : position.left,
                top      : position.top
            });
        }

    });

    $document.on('mouseout', '[data-title]', function(e) {
        $('.title').remove();
    });

    if ($('.sub-menu').length) {
        var curTab = $('.sub-menu__item._active').index();
    }

    $('[data-sidebar-toggle]').on('click', function(e) {
        e.preventDefault();

        let $this    = $(this);
        let $sidebar = $('[data-sidebar]');
        let $tabs    = $('.sub-menu__item');
        let href     = $this.data('sidebar-toggle');
        let $form    = $('#' + href);

        $form.appendTo($sidebar.find('.page-sidebar__inner'));

        $tabs.removeClass(active);
        $this.addClass(active);
        $sidebar.addClass(active);
        $('#' + href).show();

        if ($this.hasClass('_fix')) {
            let $form    = $('[data-mortgage-form]');
            let $sliders = $form.find('[data-slider]');

            $sliders.each(function() {
                let $slider = $(this);
                let type    = $slider.data('slider');
                let value   = $slider.slider('option', 'value');

                $('#mortgage-sidebar-' + type).slider('value', value);
                $('#mortgage-sidebar-' + type).slider('disable');
                $('#mortgage-sidebar-' + type + '-input').val(Number(value).toLocaleString("ru-RU"));
                $('#mortgage-sidebar-' + type + '-input').attr('disabled', true);
            });
        }
    });

    $document.on('click', function(e) {
        var $target = $(e.target);

        if ($target.is('[data-form-update]')) {
            let $sidebar = $('[data-sidebar]');
            let $success = $sidebar.find('.page-form__success');

            $success.remove();
            $sidebar.find('.page-sidebar__tlt').show().next('.page-form').show();
        }

        if (
            $target.is('.page-sidebar__close') ||
            $target.is('.call-form-sidebar__btn_ok') ||
            (
                !$target.is('.ui-datepicker-next') &&
                !$target.is('.ui-datepicker-prev') &&
                !$target.is('.ui-datepicker-remover') &&
                !$target.closest('.ui-datepicker').length &&
                !$target.is('.ui-datepicker') &&
                !$target.closest('.page-sidebar__inner').length &&
                !$target.closest('[data-sidebar-toggle]').length
            )
        ) {
            $('.page-sidebar__inner')
                .css({
                    transform: 'translateX(-100%)'
                });

                setTimeout(function() {
                    $('[data-sidebar]')
                        .removeClass(active)
                        .find('[data-slider]')
                        .slider('enable');
                    $('.page-sidebar__inner').removeAttr('style');
                    $('.page-sidebar__wrapper').hide();
                }, 300);

            if ($('.sub-menu__item').length > 1) {
                $('.sub-menu__item')
                    .removeClass(active)
                    .eq(curTab)
                    .addClass(active);
            } else {
                $('.sub-menu__item')
                    .removeClass(active);
            }
        }
    });

    $('.page-form').on('submit', function(e) {
        e.preventDefault();
        console.log('TETETET')

        let $form     = $(this);
        let isMail    = $form.hasClass('_mail');
        let isContact = $form.hasClass('_contact');

        let dataToComagic;
        let type = $form.data('form-type');

        if (isMail) {
            return;
        }

        let $title     = $form.prev('.page-sidebar__tlt');
        let $submit    = $form.find('input[type=submit]');
        let url        = $form.data('action');

        let dataToSend = $.extend(true, $form.serObject(), {
            Submit: 1,
            url: window.location.href,
        });

        let isParking  = $form.closest('.parking-form').length;

        if (isParking || $form.closest('#parking-inner').length) {
            dataToSend = $.extend(true, $form.serObject(), {
                Submit: 1,
                url: window.location.href,
            });
        }

        $submit.attr('disabled', true);

        $.ajax({
            url     : $form.data("action"),
            type    : $form.attr("method"),
            data    : dataToSend,
            success : function(response, textStatus, req) {

                let dateString = req.getResponseHeader('Date');
                if (dateString.indexOf('GMT') === -1) {
                    dateString += ' GMT';
                }
                let date = new Date(dateString);
                let dateHourse = date.getUTCHours()+3;
                let dateMinutes = date.getUTCMinutes();

                let isWorkingTime = (dateHourse >= 11 && dateHourse <= 20) && (dateMinutes >= 0 && dateMinutes <= 29) ? true : false;

                if (isParking) {
                    window.location.href = response.formUrl;

                    return;
                }

                let errorCode = parseInt(response.code);

                if (errorCode === 0) {

                    $form.trigger('reset').hide();
                    $title.hide()
                        .after('<div class="page-form__success"><div class="page-sidebar__tlt">Заявка отправлена!</div><p>' + response.success + '</p><button data-form-update class="button _orange">Ок</button></div>');

                } else {
                    console.log("Не удалось отправить форму! Попробуйте позже или обратитесть по телефону...");
                }

                $submit.attr('disabled', false);

                if (type === 'flat') {
                    dataToComagic = {
                        name: dataToSend.name,
                        phone: dataToSend.phone,
                        message: dataToSend.flat
                    };
                } else if (type === 'mortgage') {
                    let message = {
                        'Имя': dataToSend.name,
                        'Телефон': dataToSend.phone,
                        'Почта' : dataToSend.email,
                        'Время для звонка': dataToSend.time,
                        'Запрашиваемая сумма': dataToSend.summ,
                        'Первоначальный взнос': dataToSend.summfirst,
                        'Срок кредитования': dataToSend.years
                    }
                    dataToComagic = {
                        message: JSON.stringify(message)
                    }
                } else if (type === 'parking') {
                    let houses = [];
                    $.each(dataToSend, function(item, i) {
                        let bld = item.split('house_');

                        if (bld.length > 1) {
                            houses.push(i)
                        }
                    });
                    houses = houses.join(', ');

                    dataToComagic = {
                        name: dataToSend.name,
                        phone: dataToSend.phone,
                        message: houses
                    };

                } else {
                    dataToComagic = {
                        name: dataToSend.name,
                        phone: dataToSend.phone
                    };
                }
                if ( typeof Comagic !== "undefined") {
                    sendComagicRequest(dataToComagic, type, function () {
                        console.log('Запрос отправлен');
                    }, isWorkingTime);
                }
            }
        });
    });

    $('[data-select]').selectmenu({
        width: false,
        create: function(e, ui) {
            $('.ui-selectmenu-menu').addClass('_custom');
        }
    });

    $('[name="parking-address"]').selectmenu( "disable" );

    $('[name="parking-recipient"]').selectmenu({
        change: function( event, ui ) {
            let selectParkingAddress = $('[name="parking-address"]');
            let selectParkingRecepient = $('[name="parking-recipient"]');

            let activeRecepient = selectParkingRecepient.find('option:selected').data('recepient');
            console.log('activeRecepient', activeRecepient)
            selectParkingAddress.find('option').attr('disabled','disabled');
            $('[data-address-recepient="'+ activeRecepient +'"]').removeAttr('disabled');

            selectParkingAddress[0].selectedIndex = 0;
            $('[name="parking-address"]').selectmenu( "enable" );
            selectParkingAddress.selectmenu("refresh");
        }
    });

    $('#mail-banner-form, #mail-bannerV2-form').on('submit', function(e) {
        e.preventDefault();

        let $form      = $(this);
        let $wrapper   = $form.closest('.mail-banner__inner');
        let $title     = $wrapper.find('.mail-banner__tlt');
        let action     = $form.data('action');
        let method     = $form.attr('method');
        let dataToSend = $.extend(true, $form.serializeObject(), {
            Submit: 1,
            url: window.location.href,
        });

        $.ajax({
            url     : action,
            type    : method,
            data    : dataToSend,
            success : function(response) {
                let errorCode = parseInt(response.code);
                let date   = new Date();
                let expire = new Date(date.getFullYear() + 1, date.getMonth(), date.getDate(), 23, 59, 59);

                if (errorCode === 0) {
                    $form.trigger('reset').hide();
                    $wrapper.addClass('_success');
                    $title.text('Благодарим за подписку!').next('p').text('Проверьте почту через несколько минут').after('<button data-fancybox-close class="button _orange">ОК</button></div>');

                    Cookies.set("email-banner-submit", "1", { expires: expire });
                } else {
                    console.log("Не удалось отправить форму! Попробуйте позже или обратитесть по телефону...");
                }
            }
        });
    });

    autosize($('[data-textarea]'));

    $('[data-scrollbar]').scrollbar();

    new Swiper("[data-footer-menu]", {
        slidesPerView: 'auto',
        spaceBetween: 0,
        centeredSlides: false,
    });

    // Обработка баннеров
    $(function() {
        let $banners = $('[data-banner]');
        let interval;

        countDown();

        function countDown() {
            let i = 0;

            interval = setInterval(function() {
                i = ++i;

                if (i === 15) bannersInit();
            }, 1000);

            $(document).on('mousemove', function(e) {
                if (e.clientY == 0) bannersInit();
            });
        }

        function bannersInit() {
            clearInterval(interval);

            $(document).off('mousemove');

            function disableBanner(order) {
                let now            = new Date();
                let midnight       = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 23, 59, 59);
                let daysToMidnight = (midnight - now) / 1000 / 60 / 60 / 24;

                Cookies.set('banner-' + order, '1', { expires: daysToMidnight });
            }

            let orders = new Array;

            $banners.each(function(i, e) {
                let $this = $(this);
                let order = $this.data('banner');

                if (!Cookies.get('banner-' + order)) {
                    orders.push(order);
                }
            });

            let minOrder   = Math.min.apply(null, orders)
            let $minBanner = $('[data-banner="' + minOrder + '"]');

            if ($minBanner.find('a').length) {
                $minBanner.find('a').on('click', function() {
                    disableBanner(minOrder);
                });
            }

            $.fancybox.open($minBanner, {
                beforeClose: function() {
                    disableBanner(minOrder);
                },
                hideScrollbar: false
            });
        }
    });

    $('.close').click(function() {
        $.fancybox.close();
    });
});


function ScrollerTopNav() {
	let navInner = $('.TopNav_inner');
	let checkNav = $('.TopNav_inner').length;

	if (checkNav) {
		let activeLink = $('.TopNav_leaf-active');
		let activeLinkWidth = activeLink.width();
		let activeLinkPosition = activeLink.position().left;
		let newPosition = activeLinkWidth + activeLinkPosition;

		let checkLastEl = $('.TopNav_leaf-active').is('.TopNav_leaf:last');

		if (checkLastEl) {
			navInner.animate({'scrollLeft': newPosition}, 300);
		} else {
			navInner.animate({'scrollLeft': activeLinkPosition}, 300);
		}
	}

}

ScrollerTopNav();


let favListFlats = new FavoritesList("favorites_flats");
let favListPlans = new FavoritesList("favorites_plans");

let favPlans;
let favFlats;

let displayHeaderFavIcon = () =>  {
	let amountFavorites = favListFlats.favorites.length;

	let headerFavIcon = $('.FavoriteIcon-header');

	if (amountFavorites) {
		headerFavIcon.addClass('FavoriteIcon-header--visible');
		headerFavIcon.html(amountFavorites)
	} else {
		headerFavIcon.removeClass('FavoriteIcon-header--visible');
	}
}


displayHeaderFavIcon();

let toolTip = () => {
	$(".PickupFlatLayout_resultsCont, .Flat-type_infoHeader, .FlatsTips_icons, .Compare-list").tooltip({
		items: ".toolTip",
		classes: {
			"ui-tooltip": "favTooltip",
		},
		position: { my: "left top+15", at: "left bottom", collision: "fit" },
		content: function () {

			let isRemove = $(this).hasClass('Compare-slider-slide__remove');
			let isComparison = $(this).hasClass('ComparisonIcon');
			let isActiveComparison = $(this).hasClass('ComparisonIcon-active');

			let isFavorites = $(this).hasClass('FavoriteIcon');
			let isActiveFavorites = $(this).hasClass('FavoriteIcon-active');

			if (isRemove) {
				return 'Удалить'
			}

			if (isComparison) {
				if (isActiveComparison) {
					return 'Убрать из сравнения'
				} else {
					return 'Добавить к сравнению'
				}
			}

			if (isFavorites) {
				if (isActiveFavorites) {
					return 'Убрать из избранного'
				} else {
					return 'Добавить в избранное'
				}
			}

		},
		position: { my: "top top-52", at: "center center", collision: "flipfit" }
	});
}

toolTip();


// compare
let flatsToCompare = new FavoritesList("compare_flats");
let plansToCompare = new FavoritesList("compare_plans");

let plansCompare;
let flatsCompare;

let setActiveFavorite = ()=> {
    let favIconPlans = $('.FavoriteIcon[data-plan-id]');
    let favIconFlats = $('.FavoriteIcon[data-flat-type-id]');
    if ( favIconPlans.length ) {
        favIconPlans.each(function(index, el){
            let $el = $(this);
            let planId = $(this).data('plan-id');
            favListPlans.isFavorite(planId);
            if ( favListPlans.isFavorite(planId) ) {
                $el.addClass('FavoriteIcon-active');
            }
        });
    }
    if ( favIconFlats.length ) {
        // console.log('favIconFlats', favIconFlats)
        favIconFlats.each(function(index, el){
            let $el = $(this);
            let flatId = $(this).data('flat-type-id');
            favListFlats.isFavorite(flatId);
            if ( favListFlats.isFavorite(flatId) ) {
                $el.addClass('FavoriteIcon-active');
            }
        });
    }
};

{
    let timer;

    let dataTimer = {
        init: function() {
            let $timerBlock = $(".timer-date");

            if (!$timerBlock.length) return;

            let $inputTime = $timerBlock.find(".timer-date__hours");
            let $inputTimeType = $timerBlock.find(".timer-date__date-type");
            let dateAction = $timerBlock.data("timer").split(",");

            let dateFinish = new Date(dateAction[0], dateAction[1], dateAction[2], dateAction[3], dateAction[4]);
            let dateStart  = new Date();

            if (dateFinish < dateStart) {
                $timerBlock.remove();
                clearInterval(timer);
                return false;
            }

            let hours   = (dateFinish - dateStart) / 3600000;
            let minutes = (dateFinish - dateStart) / 60000;
            let days    = Math.ceil(hours / 24);

            function declOfNum(number, titles) {
                let cases = [2, 0, 1, 1, 1, 2];

                return titles[number % 100 > 4 && number % 100 < 20 ? 2 : cases[number % 10 < 5 ? number % 10 : 5]];
            }

            if (minutes == 0) {
                $timerBlock.hide();

                return;
            }

            if (days > 1) {
                $inputTime.text(days);
                $inputTimeType.text(declOfNum(days, ["день", "дня", "дней"]));
            } else {
                if ((hours / 24) < 1) {
                    if (minutes < 60) {
                        $inputTime.text(Math.ceil(minutes));
                        $inputTimeType.text(declOfNum(Math.ceil(minutes), ["минута", "минут", "минут"]));
                    } else {
                        $inputTime.text(Math.ceil(hours));
                        $inputTimeType.text(declOfNum(Math.ceil(hours), ["час", "часа", "часов"]));
                    }
                }
            }
        },
    };

    dataTimer.init();

    timer = setInterval(() => {
        dataTimer.init();
    }, 60000);
}

$(function () {
    if ($('.showroom').length) {
        $('.showroom__main--modern').on('click', '.showroom__overlay', function () {
            let fullFrame = $('.showroom-fullFrame--modern');
            let srcFrame = fullFrame.data('frame');
            if (fullFrame.find('iframe').length === 0) {
                fullFrame.append(`<iframe class="showroom-fullFrame__iframe" src="${srcFrame}"></iframe>`)
            }
            fullFrame.fadeIn(400);

        });

        $('.showroom__main--classic').on('click', '.showroom__overlay', function () {
            let fullFrame = $('.showroom-fullFrame--classic');
            let srcFrame = fullFrame.data('frame');
            if (fullFrame.find('iframe').length === 0) {
                fullFrame.append(`<iframe class="showroom-fullFrame__iframe" src="${srcFrame}"></iframe>`)
            }
            fullFrame.fadeIn(400);
        });

        $('.showroom-fullFrame__close').on('click', function () {
            $('.showroom-fullFrame').fadeOut(400);
        });

        $('[data-scrollTo="showroom"]').on('click', function (event) {
            event.preventDefault();
            let top = $('[data-scroll="showroom"]').offset().top;
            $('body,html').animate({scrollTop: top - 90}, 1800);

        });
    }
});

$('.footer-subscribe__input').on('focus',function(){
    $('.footer').animate({
        scrollTop: $('.footer').offset().top
    }, 1000);
});

function sendComagicRequest(req, type, callback, isWorkingTime) {
    // isWorkingTime = (isWorkingTime === undefined || !isWorkingTime) ? false : true;
    if(isWorkingTime) {
        Comagic.sitePhoneCall({phone: req.phone}, callback);
    } else {
        let requestType = ["callback", "flat", "commerce", "parking", "tradein", "storeroom", "mortgage"];
        if(type === undefined || requestType.indexOf(type) < 0) {
            type = requestType[0];
        }
        if(req.message === undefined) {
            req.message = "";
        }
        req.message = "[" + type + "] " + req.message;
        Comagic.addOfflineRequest(req, callback);
    }
}

$('[data-cookiesagree]').each(function() {
    let $wrapper = $(this);
    $wrapper.addClass('_hide');

    //Проверяем соглашались ли ранее.
    if (!Cookies.get('policy')) {
        $wrapper.removeClass('_hide');

        $('[data-cookiesagree-btn]').on('click', function() {
            $wrapper.addClass('_hide');
            //Устанавливаем куки на срок - 30 дней.
            Cookies.set('policy', '1', { expires : 30 });

        });

        return;
    }
});

function getQueryParams(qs) {
    var queryString = qs ? qs.split('?')[1] : window.location.search.slice(1);
    return queryString;
}
window.UTM_URL = getQueryParams(document.location.search);
$('form[data-utm-save]').append(`<input type="hidden" name="utm_tags" value="${UTM_URL}" />`);