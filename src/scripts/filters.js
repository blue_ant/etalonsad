let favListPlans = new FavoritesList("favorites_plans");
const FLATS_PER_PAGE = 24;

new ViewSwitch(".ViewSwitch", {
    /*onChange: ($activeBtn) => {
        console.log($activeBtn.attr('href'));
    }*/
});

let cardTplStr = `
/*=require ./includes/chunks/FlatCard.tpl*/
`;

let flatsListBuilder = new FlatsList({
    tpl: cardTplStr,
    $mountEl: $("#filterResult"),
});

let $pager = $(".PickupFlatLayout_footerPaginationWrap").pagination({
    itemsOnPage: FLATS_PER_PAGE,
    prevText: "&nbsp;",
    nextText: "&nbsp;",
    displayedPages: 3,
    ellipsePageSet: false,
    edges: 0,
    onPageClick: (num) => {
        filter.offset = (num - 1) * FLATS_PER_PAGE;
        console.log("pager click!!!");
        filter.$filterForm.trigger("submit");
        $("html,body").animate({ scrollTop: sorter.$el.parent().offset().top }, 700, "easeInOutExpo");
        return false;
    },
});

let $progress = $(".PickupFlatLayout_progress");
let $resultsCont = $(".PickupFlatLayout_resultsCont");

let filter = new FilterForm("#filterForm", {
    submitHandler: ($filterForm) => {
        // console.log(" --- submitHandler fired---", "filter.offset: ", filter.offset);

        $resultsCont.addClass("PickupFlatLayout_resultsCont-loading");
        $progress.show().animate({ width: "33%", opacity: 1 }, 600);
        $.ajax({
            url: $filterForm.attr("target"),
            dataType: "json",
            method: $filterForm.attr("method"),
            async: true,
            data: $.extend(true, $filterForm.serializeObject(), {
                action: "get_flats",
                limit: FLATS_PER_PAGE,
                offset: filter.offset,
            }),
            xhr: () => {
                let xhr = new window.XMLHttpRequest();
                xhr.addEventListener("progress", (event) => {
                    if (event.lengthComputable) {
                        let percent = Math.ceil((100 * event.loaded) / event.total);
                        $progress.stop(true, false).animate({ width: percent + "%" }, 400);
                        console.log('percent',percent)
                        if (percent === 100) {
                            $progress.animate({ opacity: 0 }, 350, () => {
                                $progress.removeAttr("style");
                            });
                        }
                    }

                    else {
                        $progress.stop(true, false).animate({ width: '100' + "%" }, 400);
                        $progress.animate({ opacity: 0 }, 350, () => {
                            $progress.removeAttr("style");
                        });
                    }
                });

                return xhr;
            },
        })
            .done((jsonResponse) => {
                console.log('jsonResponse', jsonResponse)
                // fix pics urls in development enviroment
                if (window.location.href.indexOf("/filters.html") != -1) {
                    console.warn("rewrite json response for dev enviroment usage...");
                    jsonResponse.flats.forEach((flt) => {
                        flt.pic = "https://etalonsad.ru/" + flt.pic;
                        flt.href = "/flat.html";
                    });
                }
                // разкоментить когда будут добавлены типовые планировки
                // {
                //     let promos = jsonResponse.promos;
                //     if (promos && promos.length) {
                //         promos.forEach((promo)=>{
                //             jsonResponse.plans.splice(promo.order-1, 0, promo);
                //         });
                //     }
                // }
                //
                // // set favorites
                // jsonResponse.plans.forEach((pln) => {
                //     pln.favorite = _.includes(favListPlans.favorites, pln.id) ? 1 : 0;
                // });
                //
                // // set comparison
                // jsonResponse.plans.forEach((pln) => {
                //     pln.compare = _.includes(plansToCompare.favorites, pln.id) ? 1 : 0;
                // });
                //
                //
                // // render plans
                // $("#filteredFlatsCounter").html(jsonResponse.total);
                //
                // flatsListBuilder.render({
                //     data: jsonResponse,
                // });
                //
                //
                //
                // console.log('jsonResponse.plans', jsonResponse.plans)
                //
                // $pager.pagination("updateItems", jsonResponse.total);
                //
                // {
                //     let currentPage = filter.offset / FLATS_PER_PAGE + 1;
                //     $pager.pagination("drawPage", currentPage);
                // }
                //
                // $resultsCont.removeClass("PickupFlatLayout_resultsCont-loading");


                // стереть код ниже когда будут добавлены типовые планировки
                {
                    let promos = jsonResponse.promos;
                    if (promos && promos.length) {
                        promos.forEach((promo)=>{
                            jsonResponse.flats.splice(promo.order-1, 0, promo);
                        });
                    }
                }

                // render plans
                $("#filteredFlatsCounter").html(jsonResponse.total);

                flatsListBuilder.render({
                    data: jsonResponse,
                });

                setActiveFavorite();

                $pager.pagination("updateItems", jsonResponse.total);

                {
                    let currentPage = filter.offset / FLATS_PER_PAGE + 1;
                    $pager.pagination("drawPage", currentPage);
                }

                $resultsCont.removeClass("PickupFlatLayout_resultsCont-loading");
                //////////////////////
            })
            .fail(() => {
                alert("Не удалось получить данные с сервера!\nПопробуйте позже.");
            });
    },
});

let sorter = new Sorter(".Sorter");
let sorterTiles = new Sorter(".Sorter-tiles");

// START: favorites toggling behaviour

toolTip();

// добавление в избранное для типовых планировок в фильтре
// $("#filterResult").on('click', ".FavoriteIcon", (event)=> {
//     event.preventDefault();
//     let $favLink = $(event.currentTarget);
//
//     if ($favLink.hasClass('FavoriteIcon-active')) {
//         favListPlans.remove($favLink.data('plan-id'));
//     }else{
//         favListPlans.add($favLink.data('plan-id'));
//     }
//
//     let planId = $favLink.data('plan-id');
//     let activeIcons = $('[data-plan-id="'+ planId +'"]').filter('.FavoriteIcon');
//     activeIcons.each(function (index, el) {
//         $(this).toggleClass('FavoriteIcon-active');
//     })
//
//
//     // $favLink.toggleClass('FavoriteIcon-active');
//     displayHeaderFavIcon();
//     toolTip();
// });
// END: favorites toggling behaviour

// добавление в избранное для квартир
$("#filterResult").on('click', ".FavoriteIcon", (event)=> {
    event.preventDefault();
    let $favLink = $(event.currentTarget);
    console.log('$favLink', $favLink)

    if ($favLink.hasClass('FavoriteIcon-active')) {
        favListFlats.remove($favLink.data('flat-type-id'));
    }else{
        favListFlats.add($favLink.data('flat-type-id'));
    }

    let flatId = $favLink.data('flat-type-id');
    console.log('flatId', flatId)
    let activeIcons = $('[data-flat-type-id="'+ flatId +'"]').filter('.FavoriteIcon');
    console.log('activeIcons', activeIcons)
    activeIcons.each(function (index, el) {
        $(this).toggleClass('FavoriteIcon-active');
    })


    // $favLink.toggleClass('FavoriteIcon-active');
    displayHeaderFavIcon();
    toolTip();
});


// START: comparison toggling behaviour
$("#filterResult").on('click', ".ComparisonIcon", (event)=> {
    event.preventDefault();
    let $compareLink = $(event.currentTarget);

    if ($compareLink.hasClass('ComparisonIcon-active')) {
        plansToCompare.remove($compareLink.data('plan-id'));
    }else{
        plansToCompare.add($compareLink.data('plan-id'));
    }

    let planId = $compareLink.data('plan-id');
    let activeIcons = $('[data-plan-id="'+ planId +'"]').filter('.ComparisonIcon');
    activeIcons.each(function (index, el) {
        $(this).toggleClass('ComparisonIcon-active');
    })
    displayHeaderCompareIcon();
    toolTip();

});
// END: comparison toggling behaviour



$('.PromoActionString').each(function () {
    let title = $(this).attr('title');
    $(this).tooltip({
        classes: {
            "ui-tooltip": "PromoActionString_toolp"
        },
        content: function () {
            return title;
        }
    })
});

$('.PickupFlatLayout_results').tooltip({
    items: '[title]',
    classes: {
        "ui-tooltip": "PromoActionString_toolp"
    },
    content: function () {
        console.log(this)
        return $(this).attr('title');
    }
}).on('click', '.Tbl_picSale', (event) => {
    event.preventDefault();
});

