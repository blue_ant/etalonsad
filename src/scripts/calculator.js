var $banks = $('[data-banks-body]');
var $flats = $('.calculator-flats');
var allBanks       = banks;
var availableBanks = [];
var creditSumm     = 0;

let calcInit = new Calculator({
    $el: $("[data-calculator]"),
    banks: allBanks
});

const $modal  = $('.mortgage-modal');
const minRate = _.sortBy(allBanks, function(o) {return o.filterRate;})[0].filterRate;

$(function() {
    var allBanks       = banks;
    var availableBanks = [];
    var $rows          = $('[data-bank-id]');

    $document.on('click', '[data-bank-id]', function() {
        let $row   = $(this);
        let bankID = $row.data('bank-id');
        var bankContent;
        var bankImage;

        if ($row.hasClass(active)) {
            $('.mortgage-banks__row-more').each(function() {
                $(this).stop()
                    .animate({
                        height: 0
                    }, function() {
                        $row.removeClass(active);
                        $(this).remove();
                    });
            });

            return;
        }

        $('.mortgage-banks__row-more').each(function() {
            $(this).stop()
                .animate({
                    height: 0
                }, function() {
                    $(this).remove();
                });
        });

        $rows.removeClass(active);

        $.each(banks, function(i, el) {
            if (bankID === el.id) {
                bankContent = el.descr;
                bankImage   = el.image;
            }
        });

        if ($document.width() < 575) {
            $row.after('<div class="mortgage-modal mortgage-banks__row-more"><div class="mortgage-modal__inner">' + bankContent + '</div></div>');

            let $infoRow = $row.next();

            $infoRow.stop().animate({
                height: $infoRow.get(0).scrollHeight
            });

            $row.addClass(active);


            setTimeout(function() {
                let offset = $row.offset().top - 70;

                $('html, body').stop().animate({
                    scrollTop: offset
                });
            }, 400);
        } else {
            $modal.html('<p><img src="' + bankImage + '" alt=""></p>' + bankContent);

            $.fancybox.open($modal, {
                scrolling: 'visible',
                helpers: {
                    overlay: {
                        locked: false
                    }
                }
            });
        }
    });

    (function($switcher) {
        if (!$switcher.length) return;

        let $inputs = $switcher.find('.switch-type__inp');
        let $point  = $switcher.find('.switch-type-slider__point');
        let $switch = $switcher.find('.switch-type-slider');
        let $types  = $('[data-type]');

        function changeBankList(type) {
            let $header = $('[data-switch-header]');
            let $bodyswitch = $('[data-banks-body]');

            if (type === 'payment') {
                $header.html('Стоимость квартиры');
                $bodyswitch.addClass('_prices');
            } else if (type === 'price') {
                $header.html('Ежемесячный платеж');
                $bodyswitch.removeClass('_prices');
            }

            Calculator.banksUpdate();
        }

        $inputs.on('change', function() {
            let $this   = $(this);
            let $parent = $this.parent();
            let index   = $parent.index();
            let type    = $parent.data('switch-type');

            $('.switch-type__item').removeClass(active);
            $parent.addClass(active);

            $types.hide();

            if (index === 0) {
                $types.filter('[data-type="payment"]').show();
            } else {
                $types.filter('[data-type="price"]').show();
            }

            changeBankList(type);
        });

        $switch.on('click touch', function(e) {
            let index  = $('.switch-type__item._active').index();
            let $items = $('.switch-type__item');

            $items.find('input').attr('checked', false);
            $types.hide();

            if (index === 0) {
                $items
                    .removeClass(active)
                    .last()
                    .addClass(active)
                    .find('input')
                    .prop('checked', true);

                $types.filter('[data-type="price"]').show();
            } else {
                $items
                    .removeClass(active)
                    .first()
                    .addClass(active)
                    .find('input')
                    .prop('checked', true);

                $types.filter('[data-type="payment"]').show();
            }

            let type = $('.switch-type__item._active').data('switch-type');

            changeBankList(type);
        });
    })($('[data-switch-type]'));

    $("[data-mortgage-form]").on('submit', function(e) {
        e.preventDefault();

        let $form = $(this);

        let dataToSend = $.extend(true, $form.serializeObject(), {
            Submit : 1,
            url    : window.location.href,
        });

        $.ajax({
            url  : $form.data("action"),
            type : $form.attr("method"),
            data : dataToSend,
        }).done((response) => {
            let errorCode = parseInt(response.code);

            if (errorCode === 0) {
                let successText = `
                    <div class="page-sidebar__inner call-form-inner_sendMessage">
                        <div class="call-form-sidebar__tlt call-form-sidebar__tlt_ok">Заявка отправлена!</div>
                        <div class="call-form-sidebar__text">
                            ${response.success}
                        </div>
                        <button class="call-form-sidebar__btn call-form-sidebar__btn_ok">ok</button>
                    </div>`;

                $form.trigger('reset').parent().hide();

                window.requestAnimationFrame(() => {
                    $form.parent().hide().after(successText);
                });

            } else {
                alert("Не удалось отправить форму! Попробуйте позже или обратитесть по телефону...");
            }
        }).always(( /*response*/ ) => {

        });

        return false;
    });

    $('[data-slider]').each(function() {
        let slider = new RangeSliderMortgage($(this));
    });

    $('#form-mortgage').find('input._slide').attr('disabled', true);
});
