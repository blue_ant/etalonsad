var loadImage = function(src, callback) {
    var img = new Image();
    img.onload = img.onerror = function(evt) {
        callback(evt.type == 'error', img);
        img.onload = img.onerror = img = null;
    };
    img.src = src;
};

if ($(".house").length > 0) {
    house();
}

function house() {
    $(".house__render-item").hover(function(e) {
        let floorIsDisable = $(this).hasClass('house__render-item--disable');
        if  (!floorIsDisable ) {
            var floorText = $(this).data("floor-text");
            var flatText = $(this).data("flat-text");

            $('.house__swim').addClass('house__swim--active');
            $('.house__swim .house__label-text').text(floorText);
            $('.house__swim .house__label-title').text(flatText);
        }

        $(this).parent().addClass("house__render--active");
        $(this).addClass("house__render-item--active");

    }, function() {

        $('.house__swim').removeClass('house__swim--active');
        $(this).parent().removeClass("house__render--active");
        $(this).removeClass("house__render-item--active");
    });

    $(".house__render-item").click(function() {
        var link = $(this).data("floor-url");
        let isDisable = $(this).hasClass('house__render-item--disable');
        if ( !isDisable ) {
            window.location = link;
        }

    });

    loadImage($(".house__img-common").attr('src'), function(err, img) {
        if (err) {
            alert('image not load');
        } else {
            var ratio = img.width / img.height;
            var windowWidth = $(window).width();

            updatehouse(ratio, windowWidth);

            $(window).resize(() => {
                windowWidth = $(window).width();
                updatehouse(ratio, windowWidth);
            });
        }
    });

    swimLabel();
}

function updateSwimLabel(e) {
    var pos = $(".house__content").offset();
    var elem_left = pos.left;
    var elem_top = pos.top;
    var Xinner = e.pageX - elem_left;
    var Yinner = e.pageY - elem_top;

    $('.house__swim').css({ top: Yinner + 12, left: Xinner + 4 });
}

function swimLabel() {
    $(".house").mousemove(function(e){
        updateSwimLabel(e);
    });
}

function updatehouse(ratio, windowWidth) {
    resizeImg(ratio);
    if (windowWidth >= 1280) {
        housePosition();
        $(".house").css({ 'overflow-x': '' });
    } else {
        $(".house__content").css({ top: '', left: '' });
        $(".house").css({ 'overflow-x': 'auto' });
        $(".house__labels").css({ width: '', height: '', top: '', left: '' });
    }
}

function resizeImg(ratio) {
    var genWidth = $(".house").outerWidth();
    var genHeight = $(".house").outerHeight();
    var getRatio = genWidth / genHeight;

    if (getRatio < ratio) {
        $(".house__content").css({ width: `${genHeight*ratio}px`, height: '100%' });
        $(".house__img-common").css({ width: `${genHeight*ratio}px`, height: '100%' });
    } else {
        $(".house__content").css({ width: '', height: '' });
        $(".house__img-common").css({ width: '', height: '' });
    }
}

function housePosition() {
    var genWidth = $(".house").outerWidth();
    var genHeight = $(".house").outerHeight();
    var genContentWidth = $(".house__content").outerWidth();
    var genContentHeight = $(".house__content").outerHeight();

    $(".house__content").css({
        top: `${-(genContentHeight - genHeight)/2.25}px`,
        left: `${-(genContentWidth - genWidth)/2}px`,
    });

    $(".house__labels").css({
        width: `${genWidth}px`,
        height: `${genHeight}px`,
        top: `${(genContentHeight - genHeight)/2.25}px`,
        left: `${(genContentWidth - genWidth)/2}px`,
    });
}
