const gulp = require('gulp');
const del = require('del');
const nunjucksRender = require('gulp-nunjucks-render');
const browserSync = require('browser-sync').create();
const concat = require('gulp-concat');
const postcss = require('gulp-postcss');
const postcssNested = require("postcss-nested");
const cssnano = require('cssnano');
const include = require("gulp-include");
const notify = require("gulp-notify");
const cssnext = require("postcss-cssnext");
const gulpif = require('gulp-if');
const minifyJS = require('gulp-uglify');
const babel = require('gulp-babel');
const sourcemaps = require('gulp-sourcemaps');
const prefixer = require('gulp-autoprefixer');
const plumber = require('gulp-plumber');
const base64 = require('gulp-base64');
//const debug = require('gulp-debug');

const isProduction = process.env.NODE_ENV === "production";

gulp.task('templates:all', function() {

    return gulp.src(['src/templates/**/*.njk', '!src/templates/*layout.njk', '!src/templates/includes/**/*.*'])
        .pipe(nunjucksRender({
            path: 'src/templates'
        }))
        .on('error', function(err) {
            notify({ title: 'templates task error!' }).write(err.message);
            this.emit('end');
        })
        .pipe(gulp.dest("build"));
});

gulp.task('templates:single', function() {
    return gulp.src(['src/templates/**/*.njk', '!src/templates/*layout.njk'], { since: gulp.lastRun('templates:single') })
        .pipe(nunjucksRender({
            path: 'src/templates'
        }))
        .on('error', function(err) {
            notify({ title: 'template task error!' }).write(err.message);
            this.emit('end');
        })
        .pipe(gulp.dest("build"));
});

gulp.task('styles', function() {
    var plugins = [
        postcssNested(),
        cssnext({
            browsers: ">1%, last 2 versions",
        }),
        cssnano({
            discardUnused: { fontFace: false },
            autoprefixer: false,
            zindex: false,
            discardComments: { removeAll: true }
        }),

    ];

    return gulp.src('src/styles/*.css')
        //.pipe(debug())
        .pipe(include())
        .pipe(postcss(plugins))
        .on('error', function(err) {
            notify({ title: 'CSS task error!' }).write(err.message);
            this.emit('end');
        })
        .pipe(base64({
            baseDir: 'src/assets/img/',
            extensions: ['svg', 'png'],
            exclude: [/^((?!datauri).)*$/],
            maxImageSize: 8 * 1024, // bytes 
            debug: false
        }))
        .pipe(gulp.dest("build/css"));
});


gulp.task('scripts:single', function() {
    return gulp.src('src/scripts/*.*', { since: gulp.lastRun('scripts:single') })
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(include()).on('error', console.error)
        .pipe(babel({
            presets: ['es2015']
        }))
        .on('error', function(err) {
            notify({ title: 'scripts task error!' }).write(err.message);
            this.emit('end');
        })
        .pipe(minifyJS())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest("build/js"));
});

gulp.task('scripts:all', function() {
    return gulp.src(['src/scripts/*.*'])
        //.pipe(debug())
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(include()).on('error', console.error)
        .pipe(babel({
            presets: ['es2015']
        }))
        .on('error', function(err) {
            notify({ title: 'scripts task error!' }).write(err.message);
            this.emit('end');
        })
        .pipe(minifyJS())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest("build/js"));
});

gulp.task('assets', function() {
    return gulp.src(['src/assets/**'], { since: gulp.lastRun('assets') })
        .pipe(gulp.dest("build"));
});

gulp.task('vendors:js', function() {
    return gulp.src(['src/vendor/js/*.js'])
        .pipe(concat('vendor.js'))
        .pipe(minifyJS({
            mangle: false,
        }))
        .pipe(gulp.dest("build/js"));
});

gulp.task('vendors:css', function() {
    var plugins = [
        cssnano({
            discardUnused: { fontFace: false },
            zindex: false,
            autoprefixer: false,
            discardComments: { removeAll: true },
        }),
    ];
    return gulp.src('src/vendor/css/*.css')
        .pipe(concat('vendor.css'))
        .pipe(postcss(plugins))
        .pipe(gulp.dest("build/css"));
});

gulp.task('clean', function() {
    return del('build');
});

gulp.task('build', gulp.series('clean', gulp.parallel(
    'templates:all',
    'styles',
    'vendors:js',
    'vendors:css',
    'assets',
    'scripts:all'
)));

gulp.task('reload', function(done) {
    browserSync.reload();
    done();
});

gulp.task('watch', function() {
    gulp.watch([
        'src/templates/layout.njk',
        'src/templates/includes/**/*.*'
    ], gulp.series('templates:all', 'reload'));
    gulp.watch(['src/templates/*.njk', '!src/templates/layout.njk'], gulp.series('templates:single', 'reload'));
    gulp.watch('src/styles/**/*.*', gulp.series('styles'));
    gulp.watch('src/scripts/*.*', gulp.series('scripts:single', 'reload'));
    gulp.watch('src/scripts/includes/**/*.*', gulp.series('scripts:all', 'reload'));
    gulp.watch('src/assets/**/*.*', gulp.series('assets', 'reload'));
    gulp.watch('src/vendor/css/*.css', gulp.series('vendors:css'));
    gulp.watch('src/vendor/js/*.js', gulp.series('vendors:js', 'reload'));
});

gulp.task('serve', function() {
    browserSync.init({
        server: 'build',
        open: false,
        ghostMode: false,
        reloadDebounce: 80,
    });

    browserSync.watch(['build/css/*.css', '!build/*.html']).on('change', browserSync.reload);
});

gulp.task('validate', function() {
    return gulp.src('build/*.html')
        .pipe(require('gulp-html-validator')({ format: 'html' }))
        .pipe(gulp.dest('./validator-out'));
});

gulp.task('dev', gulp.series('build', gulp.parallel('watch', 'serve')));